package com.prismkt

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import java.io.BufferedReader
import java.io.InputStreamReader

class PrintConnectActivity constructor() : AppCompatActivity() {
    private val c: Common = Common.Companion.instance
    private val p: Printer = Printer.Companion.instance
    private var _etHost: EditText? = null
    private var _etPort: EditText? = null
    private var _tvMsg: TextView? = null
    private var _sHost: String? = null
    private var _sPort: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_print_connect)
        _etHost = findViewById<View>(R.id.IDC_PRINT_PRINTCONNECT_EDIT_HOST) as EditText?
        _etPort = findViewById<View>(R.id.IDC_PRINT_PRINTCONNECT_EDIT_PORT) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_PRINT_PRINTCONNECT_TEXT_MSG) as TextView?
        try {
            openFileInput(Printer.Companion.PRINTERNAME).use({ fis ->
                InputStreamReader(fis, "UTF-8").use({ isr ->
                    BufferedReader(isr).use({ br ->
                        _sHost = br.readLine()
                        if (_sHost == null) {
                            _sHost = ""
                        }
                        _sPort = br.readLine()
                        if (_sPort != null) {
                            val iPort: Int = _sPort!!.toInt()
                            if ((iPort <= 0) || (iPort > 9999)) {
                                _sPort = Printer.Companion.PRINTERPORT
                            }
                        } else {
                            _sPort = Printer.Companion.PRINTERPORT
                        }
                    })
                })
            })
        } catch (e: Exception) {
            _sHost = ""
            _sPort = Printer.Companion.PRINTERPORT
        }
        _etHost!!.setText(_sHost)
        _etPort!!.setText(_sPort)
        openSoftKeyboard(_etHost)
        _etPort!!.setOnKeyListener(object : View.OnKeyListener {
            public override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
                if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
                    val btnOK: Button = findViewById<View>(R.id.IDOK) as Button
                    btnOK.performClick()
                    return true
                }
                return false
            }
        })
    }

    fun btnIDOK(view: View?) {
        _sHost = _etHost!!.getText().toString().trim({ it <= ' ' })
        if (_sHost!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_host_must_be_entered)
            openSoftKeyboard(_etHost)
        } else {
            _sPort = _etPort!!.getText().toString().trim({ it <= ' ' })
            val iPort: Int
            try {
                iPort = _sPort!!.toInt()
                if ((iPort <= 0) || (iPort > 9999)) {
                    _tvMsg!!.setText(R.string.msg_port_must_be_between_0_and_9999)
                    openSoftKeyboard(_etPort)
                    return
                }
            } catch (e: Exception) {
                _tvMsg!!.setText(e.message)
                openSoftKeyboard(_etHost)
                return
            }
            _tvMsg!!.setText(R.string.msg_connecting)
            p.Connect(_sHost, iPort, this)
        }
    }

    fun Connected() {
        _tvMsg!!.setText(R.string.msg_connected)
        try {
            openFileOutput(Printer.Companion.PRINTERNAME, MODE_PRIVATE).use({ fos ->
                fos.write(_sHost!!.toByteArray(charset("UTF-8")))
                fos.write(0x0D)
                fos.write(0x0A)
                fos.write(_sPort!!.toByteArray(charset("UTF-8")))
            })
        } catch (ignored: Exception) {
        }
        onBackPressed()
    }

    fun CannotConnect(msg: String?) {
        _tvMsg!!.setText(msg)
        openSoftKeyboard(_etHost)
    }

    fun btnIDCANCEL(view: View?) {
        p.Disconnect()
        onBackPressed()
    }

    public override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed()
            return true
        }
        return (super.onOptionsItemSelected(item))
    }

    private fun openSoftKeyboard(et: EditText?) {
        val imm: InputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        val hObj: Handler = Handler(Looper.getMainLooper())
        c.PrismReqFocus(hObj, et, imm)
    }
}