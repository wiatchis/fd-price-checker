package com.prismkt

import android.content.*
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class PrintItemActivity : AppCompatActivity() {
    private val c: Common = Common.instance
    private val s: Server = Server.instance
    private val p: Printer = Printer.Companion.instance
    private var _aObj: PrintItemActivity? = null
    private var _hObj: Handler? = null
    private var _etItem: EditText? = null
    private var _tvSKU: TextView? = null
    private var _tvDesc: TextView? = null
    private var _etPrice: EditText? = null
    private var _etQty: EditText? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnOK: Button? = null
    private var _sItem: String? = null
    private var _sPrice: String? = null
    private var _sQty: String? = null
    private var _sPrintItemSKU: String? = null
    private var _sPrintItemQty: String? = null
    private var _iPrinterItemShelfLabels: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_print_item)
        _aObj = this
        if (Printer.Companion._iPrinterConnected == 0) {
            val alertDialog: AlertDialog = AlertDialog.Builder(this).create()
            alertDialog.setTitle(R.string.dlg_title_prism)
            alertDialog.setMessage(getResources().getString(R.string.dlg_question_connect_printer))
            alertDialog.setButton(
                AlertDialog.BUTTON_NEGATIVE,
                getResources().getString(R.string.dlg_answer_yes),
                object : DialogInterface.OnClickListener {
                    public override fun onClick(arg0: DialogInterface, arg1: Int) {
                        val intent: Intent = Intent(_aObj, PrintConnectActivity::class.java)
                        startActivity(intent)
                        alertDialog.cancel()
                    }
                })
            alertDialog.setButton(
                AlertDialog.BUTTON_POSITIVE,
                getResources().getString(R.string.dlg_answer_no),
                object : DialogInterface.OnClickListener {
                    public override fun onClick(arg0: DialogInterface, arg1: Int) {
                        finish()
                    }
                })
            alertDialog.show()
        }
        _iPrinterItemShelfLabels = getIntent().getIntExtra("iPrinterItemShelfLabels", 0)
        _etItem = findViewById<View>(R.id.IDC_PRINT_PRINTITEM_EDIT_ITEM) as EditText?
        _etQty = findViewById<View>(R.id.IDC_PRINT_PRINTITEM_EDIT_QTY) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_PRINT_PRINTITEM_TEXT_MSG) as TextView?
        _btnOK = findViewById<View>(R.id.IDOK) as Button?
        if (g.REC_Security.get(g.iRec_Security_PrnItem) != 0x30.toByte()) {
            _hObj = Handler(Looper.getMainLooper())
            _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
            _tvSKU = findViewById<View>(R.id.IDC_PRINT_PRINTITEM_EDIT_SKU) as TextView?
            _tvDesc = findViewById<View>(R.id.IDC_PRINT_PRINTITEM_EDIT_DESC) as TextView?
            _etPrice = findViewById<View>(R.id.IDC_PRINT_PRINTITEM_EDIT_PRICE) as EditText?
            _sPrintItemSKU = ""
            _sPrintItemQty = ""
            if (_iPrinterItemShelfLabels == 1) this.setTitle(R.string.act_print_itemlabels_1)
            if (_iPrinterItemShelfLabels == 2) {
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) setTitle(
                    R.string.act_print_itemlabels_2
                ) else setTitle(R.string.act_print_itemlabels_2_land)
            }
            if (_iPrinterItemShelfLabels == 3) {
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) setTitle(
                    R.string.act_print_itemlabels_3
                ) else setTitle(R.string.act_print_itemlabels_3_land)
                _etItem!!.setEnabled(false)
                _etItem!!.setFocusable(false)
                _etQty!!.setText("1")
                _etPrice!!.setTextIsSelectable(true)
                _etPrice!!.setEnabled(true)
                _etPrice!!.setFocusable(true)
                _etPrice!!.requestFocus()
            } else {
                _etItem!!.requestFocus()
            }
            if (g.REC_BPrtList.get(g.iRec_BPrtList_Name) == 0.toByte()) {
                _tvMsg!!.setText(R.string.msg_no_printer)
            } else {
                val btn: Button =
                    findViewById<View>(R.id.IDC_PRINT_PRINTITEM_PUSHBUTTON_CONNECT) as Button
                btn.setEnabled(true)
            }
        } else {
            _tvMsg!!.setText(R.string.msg_not_authorized)
            _etItem!!.setEnabled(false)
            _etQty!!.setEnabled(false)
            _btnOK = findViewById<View>(R.id.IDOK) as Button?
            _btnOK!!.setEnabled(false)
            val btn: Button = findViewById<View>(R.id.IDCANCEL) as Button
            btn.requestFocus()
        }
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(
            this,
            R.id.clPrintItem,
            !_etItem!!.getText().toString().trim({ it <= ' ' }).isEmpty()
        )
        val filter: IntentFilter = IntentFilter()
        filter.addCategory(g.sFilterCategory)
        filter.addAction(g.sFilterAction)
        registerReceiver(scanBroadcastReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clPrintItem)
        unregisterReceiver(scanBroadcastReceiver)
    }

    fun btnIDOK(view: View?) {
        if (_iPrinterItemShelfLabels != 3) {
            _sItem = _etItem!!.getText().toString().trim({ it <= ' ' })
            if (_sItem!!.isEmpty()) {
                _tvMsg!!.setText(R.string.msg_item_must_be_entered)
                _etItem!!.requestFocus()
                return
            }
        } else {
            _sPrice = _etPrice!!.getText().toString().trim({ it <= ' ' })
            if (_sPrice!!.isEmpty()) {
                _tvMsg!!.setText(R.string.msg_price_must_be_entered)
                _etPrice!!.requestFocus()
                return
            }
        }
        _sQty = _etQty!!.getText().toString().trim({ it <= ' ' })
        executeAction()
    }

    fun btnConnect(view: View?) {
        val intent: Intent = Intent(this, PrintConnectActivity::class.java)
        startActivity(intent)
    }

    fun btnIDCANCEL(view: View?) {
        onBackPressed()
    }

    private val scanBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        public override fun onReceive(context: Context, intent: Intent) {
            if ((intent.getAction() == g.sFilterAction)) {
                try {
                    displayScanResult(intent)
                } catch (ignored: Exception) {
                }
            }
        }
    }

    private fun displayScanResult(i: Intent) {
        if (_iPrinterItemShelfLabels != 3) {
            _etItem!!.setText(i.getStringExtra(g.sScanData))
        } else {
            _etPrice!!.setText(i.getStringExtra(g.sScanData))
        }
        _sPrintItemSKU = ""
        if (g.REC_PDConfig.get(g.iRec_PDConfig_ImmediateScan) != 0x31.toByte()) {
            val imm: InputMethodManager =
                getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            c.PrismReqFocus(_hObj, _etQty, imm)
        }
        _btnOK!!.performClick()
    }

    private fun executeAction() {
        Thread(ExecuteThread()).start()
    }

    internal inner class ExecuteThread constructor() : Runnable {
        public override fun run() {
            var sMsg: String?
            if ((_iPrinterItemShelfLabels != 3) && (!(_sItem == _sPrintItemSKU))) {
                Arrays.fill(g.REC_InvMst, 0.toByte())
                c.PrismCopyString(
                    g.REC_InvMst,
                    g.iRecordId,
                    g.sRecordId,
                    "InvMst210"
                ) //DbFunction 4; Format 79; GetItemDetail; PmWinDbm.app (11).
                if (_iPrinterItemShelfLabels == 2) g.REC_InvMst[g.iRec_InvMst_FunctionID] = 0x45 //'E'
                c.PrismCopyString(g.REC_InvMst, g.iRec_InvMst_SKU, g.sRec_InvMst_SKU, _sItem)
                if (g.REC_PDConfig.get(g.iRec_PDConfig_ImmediateScan) == 0x31.toByte()) {
                    Arrays.fill(g.REC_Tickets, 0.toByte())
                    c.PrismCopyString(
                        g.REC_Tickets,
                        g.iRecordId,
                        g.sRecordId,
                        "Tickets210"
                    ) //DbFunction 11; Format 84; Tickets; PmWinDbm.app (8).
                    System.arraycopy(
                        g.REC_InvMst,
                        g.iRec_InvMst_SKU,
                        g.REC_Tickets,
                        g.iRec_Tickets_SKU,
                        g.sRec_Tickets_SKU
                    )
                    g.REC_Tickets[g.iRec_Tickets_Quantity] = 0x31 //Goes to ! 0 200 200 200 &GLO:NumberOfLabels[@S9L] of the FMT file
                    System.arraycopy(
                        g.REC_BPrtList,
                        g.iRec_BPrtList_Name,
                        g.REC_Tickets,
                        g.iRec_Tickets_PrinterId,
                        g.sRec_Tickets_PrinterId
                    )
                    System.arraycopy(
                        g.REC_InvMst,
                        g.iRecordId,
                        g.REC_Tickets,
                        g.iRec_Tickets_InvMst,
                        g.sRec_InvMst - 1
                    )
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_getting_label)
                    if (s.ServerSend(g.REC_Tickets, _hObj, _tvMsg, _pbAct)) {
                        if (g.REC_Tickets.get(g.iRec_Tickets_ReadStatus) == 0x31.toByte()) {
                            System.arraycopy(
                                g.REC_Tickets,
                                g.iRec_Tickets_InvMst,
                                g.REC_InvMst,
                                0,
                                g.sRec_InvMst
                            )
                            g.REC_InvMst[g.iRec_InvMst_ReadStatus] = 0x31
                            val sbSku: StringBuilder = StringBuilder()
                            _sPrintItemSKU = c.PrismGetItemSku(
                                g.REC_InvMst,
                                g.iRec_InvMst_SKU,
                                g.sRec_InvMst_SKU,
                                sbSku
                            )
                            c.PrismMsg(_hObj, _etItem, _sPrintItemSKU)
                            sMsg = sbSku.toString()
                            c.PrismMsg(_hObj, _tvSKU, sMsg)
                            sMsg = c.PrismGetDescString()
                            c.PrismMsg(_hObj, _tvDesc, sMsg)
                            if (g.REC_InvMst.get(g.iRec_InvMst_DOANewReceived) != 0.toByte()) {
                                System.arraycopy(
                                    g.REC_InvMst,
                                    g.iRec_InvMst_DOANewReceived,
                                    g.REC_InvMst,
                                    g.iRec_InvMst_Price,
                                    g.sRec_InvMst_Price
                                )
                            }
                            _sPrice = c.PrismGetString(
                                g.REC_InvMst,
                                g.iRec_InvMst_Price,
                                g.sRec_InvMst_Price
                            )
                            c.PrismMsg(_hObj, _etPrice, _sPrice)
                            _sQty = c.PrismGetString(
                                g.REC_Tickets,
                                g.iRec_Tickets_Quantity,
                                g.sRec_Tickets_Quantity
                            )
                            c.PrismMsg(_hObj, _etQty, _sQty)
                            _sPrintItemQty = _sQty
                            if (g.REC_Tickets.get(g.iRec_Tickets_PrinterId) != 0.toByte()) {
                                PrintLabels()
                            } else {
                                c.PrismMsg(_hObj, _tvMsg, R.string.msg_got_item)
                            }
                            c.PrismReqFocus(_hObj, _etPrice)
                        } else {
                            c.PrismMsg(_hObj, _tvSKU, "")
                            c.PrismMsg(_hObj, _tvDesc, "")
                            c.PrismMsg(_hObj, _etPrice, "")
                            sMsg = c.PrismGetString(
                                g.REC_Tickets,
                                g.iRec_Tickets_ErrorString,
                                g.sRec_Tickets_ErrorString
                            )
                            c.PrismMsg(_hObj, _tvMsg, sMsg)
                            c.PrismReqFocus(_hObj, _etQty)
                        }
                    } else {
                        sMsg = c.PrismGetString(
                            g.REC_Tickets,
                            g.iRec_Tickets_ErrorString,
                            g.sRec_Tickets_ErrorString
                        )
                        c.PrismMsg(_hObj, _tvMsg, sMsg)
                        c.PrismReqFocus(_hObj, _etQty)
                    }
                } else {
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_getting_item)
                    if (s.ServerSend(g.REC_InvMst, _hObj, _tvMsg, _pbAct)) {
                        if (g.REC_InvMst.get(g.iRec_InvMst_ReadStatus) == 0x31.toByte()) {
                            val sbSku: StringBuilder = StringBuilder()
                            _sPrintItemSKU = c.PrismGetItemSku(
                                g.REC_InvMst,
                                g.iRec_InvMst_SKU,
                                g.sRec_InvMst_SKU,
                                sbSku
                            )
                            c.PrismMsg(_hObj, _etItem, _sPrintItemSKU)
                            sMsg = sbSku.toString()
                            c.PrismMsg(_hObj, _tvSKU, sMsg)
                            sMsg = c.PrismGetDescString()
                            c.PrismMsg(_hObj, _tvDesc, sMsg)
                            if (g.REC_InvMst.get(g.iRec_InvMst_DOANewReceived) == 0x21.toByte()) { //'!'
                                _sPrice = c.PrismGetString(
                                    g.REC_InvMst,
                                    g.iRec_InvMst_Disposition,
                                    g.sRec_InvMst_Disposition
                                )
                            } else {
                                if (g.REC_InvMst.get(g.iRec_InvMst_DOANewReceived) != 0.toByte()) {
                                    System.arraycopy(
                                        g.REC_InvMst,
                                        g.iRec_InvMst_DOANewReceived,
                                        g.REC_InvMst,
                                        g.iRec_InvMst_Price,
                                        g.sRec_InvMst_Price
                                    )
                                }
                                _sPrice = c.PrismGetString(
                                    g.REC_InvMst,
                                    g.iRec_InvMst_Price,
                                    g.sRec_InvMst_Price
                                )
                            }
                            c.PrismMsg(_hObj, _etPrice, _sPrice)
                            _sPrintItemQty = ""
                            _sQty = ""
                            c.PrismMsg(_hObj, _etQty, "")
                            c.PrismMsg(_hObj, _tvMsg, R.string.msg_got_item)
                            c.PrismReqFocus(_hObj, _etQty)
                        } else {
                            c.PrismMsg(_hObj, _tvSKU, "")
                            c.PrismMsg(_hObj, _tvDesc, "")
                            c.PrismMsg(_hObj, _etPrice, "")
                            c.PrismMsg(_hObj, _etQty, "")
                            sMsg = c.PrismGetString(
                                g.REC_InvMst,
                                g.iRec_InvMst_ErrorString,
                                g.sRec_InvMst_ErrorString
                            )
                            c.PrismMsg(_hObj, _tvMsg, sMsg)
                            c.PrismReqFocus(_hObj, _etItem)
                        }
                    }
                }
                return
            }
            if (_iPrinterItemShelfLabels == 3) {
                Arrays.fill(g.REC_Tickets, 0.toByte())
                c.PrismCopyString(
                    g.REC_Tickets,
                    g.iRecordId,
                    g.sRecordId,
                    "Tickets210"
                ) //DbFunction 11; Format 84; Tickets; PmWinDbm.app (8).
                c.PrismCopyString(
                    g.REC_Tickets,
                    g.iRec_Tickets_Price,
                    g.sRec_Tickets_Price,
                    _sPrice
                )
                c.PrismCopyString(
                    g.REC_Tickets,
                    g.iRec_Tickets_Quantity,
                    g.sRec_Tickets_Quantity,
                    _sQty
                )
                _sPrintItemQty = _sQty
                System.arraycopy(
                    g.REC_BPrtList,
                    g.iRec_BPrtList_Name,
                    g.REC_Tickets,
                    g.iRec_Tickets_PrinterId,
                    g.sRec_Tickets_PrinterId
                )
                g.REC_Tickets[g.iRec_Tickets_PrintType] = 0x33
                c.PrismMsg(_hObj, _tvMsg, R.string.msg_getting_price)
                if (s.ServerSend(g.REC_Tickets, _hObj, _tvMsg, _pbAct)) {
                    if (g.REC_Tickets.get(g.iRec_Tickets_ReadStatus) == 0x31.toByte()) {
                        _sQty = c.PrismGetString(
                            g.REC_Tickets,
                            g.iRec_Tickets_Quantity,
                            g.sRec_Tickets_Quantity
                        )
                        c.PrismMsg(_hObj, _etQty, _sQty)
                        _sPrintItemQty = _sQty
                        if (g.REC_Tickets.get(g.iRec_Tickets_PrinterId) != 0.toByte()) {
                            PrintLabels()
                        } else {
                            c.PrismMsg(_hObj, _tvMsg, R.string.msg_got_item)
                        }
                        c.PrismReqFocus(_hObj, _etPrice)
                    } else {
                        sMsg = c.PrismGetString(
                            g.REC_Tickets,
                            g.iRec_Tickets_ErrorString,
                            g.sRec_Tickets_ErrorString
                        )
                        c.PrismMsg(_hObj, _tvMsg, sMsg)
                        c.PrismReqFocus(_hObj, _etQty)
                    }
                }
            }
            if ((!(_sQty == _sPrintItemQty)) && (g.REC_InvMst.get(g.iRec_InvMst_ReadStatus) == 0x31.toByte())) {
                Arrays.fill(g.REC_Tickets, 0.toByte())
                c.PrismCopyString(
                    g.REC_Tickets,
                    g.iRecordId,
                    g.sRecordId,
                    "Tickets210"
                ) //DbFunction 11; Format 84; Tickets; PmWinDbm.app (8).
                System.arraycopy(
                    g.REC_InvMst,
                    g.iRec_InvMst_SKU,
                    g.REC_Tickets,
                    g.iRec_Tickets_SKU,
                    g.sRec_Tickets_SKU
                )
                c.PrismCopyString(
                    g.REC_Tickets,
                    g.iRec_Tickets_Quantity,
                    g.sRec_Tickets_Quantity,
                    _sQty
                )
                System.arraycopy(
                    g.REC_BPrtList,
                    g.iRec_BPrtList_Name,
                    g.REC_Tickets,
                    g.iRec_Tickets_PrinterId,
                    g.sRec_Tickets_PrinterId
                )
                System.arraycopy(
                    g.REC_InvMst,
                    g.iRecordId,
                    g.REC_Tickets,
                    g.iRec_Tickets_InvMst,
                    g.sRec_InvMst - 1
                )
                c.PrismMsg(_hObj, _tvMsg, R.string.msg_requesting_new_labels)
                if (s.ServerSend(g.REC_Tickets, _hObj, _tvMsg, _pbAct)) {
                    if (g.REC_Tickets.get(g.iRec_Tickets_ReadStatus) == 0x31.toByte()) {
                        _sQty = c.PrismGetString(
                            g.REC_Tickets,
                            g.iRec_Tickets_Quantity,
                            g.sRec_Tickets_Quantity
                        )
                        c.PrismMsg(_hObj, _etQty, _sQty)
                        _sPrintItemQty = _sQty
                        if (g.REC_Tickets.get(g.iRec_Tickets_PrinterId) != 0.toByte()) {
                            PrintLabels()
                        } else {
                            c.PrismMsg(_hObj, _tvMsg, R.string.msg_got_item)
                        }
                        c.PrismReqFocus(_hObj, _etPrice)
                    } else {
                        sMsg = c.PrismGetString(
                            g.REC_Tickets,
                            g.iRec_Tickets_ErrorString,
                            g.sRec_Tickets_ErrorString
                        )
                        c.PrismMsg(_hObj, _tvMsg, sMsg)
                        c.PrismReqFocus(_hObj, _etQty)
                    }
                }
            }
        }

        private fun PrintLabels() {
            var iLineStatus: Int = 1
            var stDataSize: Int = 0
            val bRecv: ByteArray = ByteArray(80)
            var bSend: ByteArray
            while (true) {
                c.PrismMsg(_hObj, _tvMsg, R.string.msg_receiving_from_server)
                stDataSize = s.ServerReceive(bRecv, _hObj, _tvMsg, _pbAct)
                if (stDataSize <= 0) break
                if ((bRecv.get(0) == 0x45.toByte()) && (bRecv.get(1) == 0x4F.toByte()) && (bRecv.get(2) == 0x46.toByte())) { //EOF
                    if ((Printer.Companion._iPrinterConnected != 0) && (iLineStatus != 0)) c.PrismMsg(
                        _hObj,
                        _tvMsg,
                        R.string.msg_printing_completed
                    ) else if (Printer.Companion._iPrinterConnected == 0) c.PrismMsg(
                        _hObj,
                        _tvMsg,
                        R.string.msg_printer_not_connected
                    )
                    break
                }
                if ((Printer.Companion._iPrinterConnected != 0) && (iLineStatus != 0)) {
                    bSend = ByteArray(stDataSize)
                    System.arraycopy(bRecv, 0, bSend, 0, bSend.size)
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_sending_to_printer)
                    if (!p.PrinterSend(bSend, _hObj, _tvMsg)) iLineStatus = 0
                }
            }
        }
    }
}