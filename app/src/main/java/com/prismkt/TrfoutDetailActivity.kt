package com.prismkt

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class TrfoutDetailActivity constructor() : AppCompatActivity(), View.OnKeyListener {
    private val c: Common = Common.instance
    private val s: Server = Server.instance
    private var _hObj: Handler? = null
    private var _etItem: EditText? = null
    private var _tvSKU: TextView? = null
    private var _tvDesc: TextView? = null
    private var _tvTotal: TextView? = null
    private var _etQty: EditText? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnOK: Button? = null
    private var _sItem: String? = null
    private var _sQty: String? = null
    private var _sTrfoutDetailNew: String? = null
    private var _sTrfoutDetailQty: String? = null
    private var _iTrfoutDetailScanFlag: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trfout_detail)
        _hObj = Handler(Looper.getMainLooper())
        _etItem = findViewById<View>(R.id.IDC_TRFOUT_TRFOUTDETAIL_EDIT_ITEM) as EditText?
        _tvSKU = findViewById<View>(R.id.IDC_TRFOUT_TRFOUTDETAIL_EDIT_SKU) as TextView?
        _tvDesc = findViewById<View>(R.id.IDC_TRFOUT_TRFOUTDETAIL_EDIT_DESC) as TextView?
        _tvTotal = findViewById<View>(R.id.IDC_TRFOUT_TRFOUTDETAIL_EDIT_TOTAL) as TextView?
        _etQty = findViewById<View>(R.id.IDC_TRFOUT_TRFOUTDETAIL_EDIT_QTY) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_TRFOUT_TRFOUTDETAIL_TEXT_MSG) as TextView?
        _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
        _btnOK = findViewById<View>(R.id.IDOK) as Button?
        _sTrfoutDetailNew = ""
        _iTrfoutDetailScanFlag = 0
        setTitle(
            getTitle().toString() + " " + c.PrismGetString(
                g.REC_TrfOut,
                g.iRec_TrfOut_TranNo,
                g.sRec_TrfOut_TranNo
            )
        )
        _etItem!!.setOnKeyListener(this)
        _etQty!!.setOnKeyListener(this)
        _etItem!!.requestFocus()
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(
            this,
            R.id.clTrfoutDetail,
            !_etItem!!.getText().toString().trim({ it <= ' ' }).isEmpty()
        )
        val filter: IntentFilter = IntentFilter()
        filter.addCategory(g.sFilterCategory)
        filter.addAction(g.sFilterAction)
        registerReceiver(scanBroadcastReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clTrfoutDetail)
        unregisterReceiver(scanBroadcastReceiver)
    }

    override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
        if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
            _btnOK!!.performClick()
            return true
        }
        return false
    }

    fun btnIDOK(view: View?) {
        _sItem = _etItem!!.getText().toString().trim({ it <= ' ' })
        if (_sItem!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_item_must_be_entered)
        } else {
            _sQty = _etQty!!.getText().toString().trim({ it <= ' ' })
            executeAction()
        }
    }

    fun btnIDCANCEL(view: View?) {
        onBackPressed()
    }

    public override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed()
            return true
        }
        return (super.onOptionsItemSelected(item))
    }

    private fun executeAction() {
        Thread(ExecuteThread()).start()
    }

    private val scanBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        public override fun onReceive(context: Context, intent: Intent) {
            if ((intent.getAction() == g.sFilterAction)) {
                try {
                    displayScanResult(intent)
                } catch (ignored: Exception) {
                }
            }
        }
    }

    private fun displayScanResult(i: Intent) {
        _etItem!!.setText(i.getStringExtra(g.sScanData))
        _sTrfoutDetailNew = ""
        _iTrfoutDetailScanFlag = 1
        if (g.REC_PDConfig.get(g.iRec_PDConfig_ImmediateScan) != 0x31.toByte()) {
            val imm: InputMethodManager =
                getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            c.PrismReqFocus(_hObj, _etQty, imm)
        }
        _btnOK!!.performClick()
    }

    internal inner class ExecuteThread constructor() : Runnable {
        var _sMsg: String? = ""
        var _iMsg: Int = 0
        override fun run() {
            if (!(_sItem == _sTrfoutDetailNew)) {
                if ((_iTrfoutDetailScanFlag != 0) && (g.REC_PDConfig.get(g.iRec_PDConfig_ImmediateScan) == 0x31.toByte())) {
                    c.PrismCopyString(
                        g.REC_TrfOut,
                        g.iRecordId,
                        g.sRecordId,
                        "TrfDtl4307"
                    ) //DbFunction 24; Format 98; TrfOutDetail; PmWinDbm.app (14); ShpDtl.tps, ShpHdr.tps
                    c.PrismCopyString(g.REC_TrfOut, g.iRec_TrfOut_SKU, g.sRec_TrfOut_SKU, _sItem)
                    Arrays.fill(
                        g.REC_TrfOut,
                        g.iRec_TrfOut_Quantity,
                        g.iRec_TrfOut_Quantity + g.sRec_TrfOut_Quantity - 1,
                        0.toByte()
                    )
                    g.REC_TrfOut[g.iRec_TrfOut_Quantity] = 0x31 //'1'
                    c.PrismMsg(_hObj, _etQty, "1")
                    _sQty = "1"
                    _iMsg = R.string.msg_sending_item
                    c.PrismMsg(_hObj, _tvMsg, _iMsg)
                    if (s.ServerSend(g.REC_TrfOut, _hObj, _tvMsg, _pbAct)) {
                        if (g.REC_TrfOut.get(g.iRec_TrfOut_ReadStatus) == 0x31.toByte()) {
                            val sbSku: StringBuilder = StringBuilder()
                            _sTrfoutDetailNew = c.PrismGetItemSku(
                                g.REC_TrfOut,
                                g.iRec_TrfOut_SKU,
                                g.sRec_TrfOut_SKU,
                                sbSku
                            )
                            c.PrismMsg(_hObj, _etItem, _sTrfoutDetailNew)
                            _sMsg = sbSku.toString()
                            c.PrismMsg(_hObj, _tvSKU, _sMsg)
                            System.arraycopy(
                                g.REC_TrfOut,
                                g.iRec_TrfOut_InvMst,
                                g.REC_InvMst,
                                0,
                                g.sRec_InvMst
                            )
                            _sMsg = c.PrismGetDescString()
                            c.PrismMsg(_hObj, _tvDesc, _sMsg)
                            _sMsg = c.PrismGetString(
                                g.REC_TrfOut,
                                g.iRec_TrfOut_Quantity,
                                g.sRec_TrfOut_Quantity
                            )
                            c.PrismMsg(_hObj, _tvTotal, _sMsg)
                            _sTrfoutDetailQty = _sMsg
                            if (g.REC_TrfOut.get(g.iRec_TrfOut_ErrorString) != 0x00.toByte()) {
                                _sMsg = (getResources().getString(R.string.msg_sent_item) + " " +
                                        getResources().getString(R.string.msg_with_error) + " " +
                                        c.PrismGetString(
                                            g.REC_TrfOut,
                                            g.iRec_TrfOut_ErrorString,
                                            g.sRec_TrfOut_ErrorString
                                        ))
                                c.PrismMsg(_hObj, _tvMsg, _sMsg)
                                c.PrismReqFocus(_hObj, _etItem)
                            } else {
                                _iMsg = R.string.msg_sent_item
                                c.PrismMsg(_hObj, _tvMsg, _iMsg)
                                c.PrismReqFocus(_hObj, _etQty)
                            }
                        } else {
                            c.PrismMsg(_hObj, _tvSKU, "")
                            c.PrismMsg(_hObj, _tvDesc, "")
                            c.PrismMsg(_hObj, _tvTotal, "")
                            c.PrismMsg(_hObj, _etQty, "")
                            _sQty = ""
                            _sMsg = c.PrismGetString(
                                g.REC_TrfOut,
                                g.iRec_TrfOut_ErrorString,
                                g.sRec_TrfOut_ErrorString
                            )
                            c.PrismMsg(_hObj, _tvMsg, _sMsg)
                            c.PrismReqFocus(_hObj, _etItem)
                        }
                    } //if (s.ServerSend(g.REC_TrfOut, _hObj, _tvMsg, _pbAct)) {
                } else { //if ((_iTrfoutDetailScanFlag != 0) && (g.REC_PDConfig[g.iRec_PDConfig_ImmediateScan] == 0x31.toByte())) {
                    Arrays.fill(g.REC_InvMst, 0.toByte())
                    c.PrismCopyString(
                        g.REC_InvMst,
                        g.iRecordId,
                        g.sRecordId,
                        "InvMst210"
                    ) //DbFunction 4; Format 79; GetItemDetail; PmWinDbm.app (11).
                    g.REC_InvMst[g.iRec_InvMst_FunctionID] = 0x43 //'C' KIA 04/01/17 BSH 13. Added New Function. See GetItemDetail.
                    c.PrismCopyString(g.REC_InvMst, g.iRec_InvMst_SKU, g.sRec_InvMst_SKU, _sItem)
                    c.PrismMsg(_hObj, _tvTotal, "")
                    c.PrismMsg(_hObj, _etQty, "")
                    _sTrfoutDetailQty = ""
                    _sQty = ""
                    _iMsg = R.string.msg_getting_item
                    c.PrismMsg(_hObj, _tvMsg, _iMsg)
                    if (s.ServerSend(g.REC_InvMst, _hObj, _tvMsg, _pbAct)) {
                        if (g.REC_InvMst.get(g.iRec_InvMst_ReadStatus) == 0x31.toByte()) {
                            val sbSku: StringBuilder = StringBuilder()
                            _sTrfoutDetailNew = c.PrismGetItemSku(
                                g.REC_InvMst,
                                g.iRec_InvMst_SKU,
                                g.sRec_InvMst_SKU,
                                sbSku
                            )
                            c.PrismMsg(_hObj, _etItem, _sTrfoutDetailNew)
                            _sMsg = sbSku.toString()
                            c.PrismMsg(_hObj, _tvSKU, _sMsg)
                            _sMsg = c.PrismGetDescString()
                            c.PrismMsg(_hObj, _tvDesc, _sMsg)
                            _iMsg = R.string.msg_got_item_enter_qty
                            c.PrismMsg(_hObj, _tvMsg, _iMsg)
                            c.PrismReqFocus(_hObj, _etQty)
                        } else {
                            c.PrismMsg(_hObj, _tvSKU, "")
                            c.PrismMsg(_hObj, _tvDesc, "")
                            c.PrismMsg(_hObj, _tvTotal, "")
                            _sMsg = c.PrismGetString(
                                g.REC_InvMst,
                                g.iRec_InvMst_ErrorString,
                                g.sRec_InvMst_ErrorString
                            )
                            c.PrismMsg(_hObj, _tvMsg, _sMsg)
                            c.PrismReqFocus(_hObj, _etItem)
                        }
                    }
                }
                exit()
                return
            } //if (_sItem.equals(_sTrfoutDetailNew) == false) {
            if (!(_sQty == _sTrfoutDetailQty)) {
                c.PrismCopyString(
                    g.REC_TrfOut,
                    g.iRecordId,
                    g.sRecordId,
                    "TrfDtl4307"
                ) //DbFunction 24; Format 98; TrfOutDetail; PmWinDbm.app (14); ShpDtl.tps, ShpHdr.tps
                c.PrismCopyString(
                    g.REC_TrfOut,
                    g.iRec_TrfOut_SKU,
                    g.sRec_TrfOut_SKU,
                    _sTrfoutDetailNew
                )
                c.PrismCopyString(
                    g.REC_TrfOut,
                    g.iRec_TrfOut_Quantity,
                    g.sRec_TrfOut_Quantity,
                    _sQty
                )
                _sTrfoutDetailQty = _sQty
                _iMsg = R.string.msg_sending_qty
                c.PrismMsg(_hObj, _tvMsg, _iMsg)
                if (s.ServerSend(g.REC_TrfOut, _hObj, _tvMsg, _pbAct)) {
                    if (g.REC_TrfOut.get(g.iRec_TrfOut_ReadStatus) == 0x31.toByte()) {
                        val sbSku: StringBuilder = StringBuilder()
                        _sTrfoutDetailNew = c.PrismGetItemSku(
                            g.REC_TrfOut,
                            g.iRec_TrfOut_SKU,
                            g.sRec_TrfOut_SKU,
                            sbSku
                        )
                        c.PrismMsg(_hObj, _etItem, _sTrfoutDetailNew)
                        _sMsg = sbSku.toString()
                        c.PrismMsg(_hObj, _tvSKU, _sMsg)
                        System.arraycopy(
                            g.REC_TrfOut,
                            g.iRec_TrfOut_InvMst,
                            g.REC_InvMst,
                            0,
                            g.sRec_InvMst
                        )
                        _sMsg = c.PrismGetDescString()
                        c.PrismMsg(_hObj, _tvDesc, _sMsg)
                        _sMsg = c.PrismGetString(
                            g.REC_TrfOut,
                            g.iRec_TrfOut_Quantity,
                            g.sRec_TrfOut_Quantity
                        )
                        c.PrismMsg(_hObj, _tvTotal, _sMsg)
                        if (g.REC_TrfOut.get(g.iRec_TrfOut_ErrorString) != 0x00.toByte()) {
                            _sMsg = (getResources().getString(R.string.msg_sent_qty) + " " +
                                    getResources().getString(R.string.msg_with_error) + " " +
                                    c.PrismGetString(
                                        g.REC_TrfOut,
                                        g.iRec_TrfOut_ErrorString,
                                        g.sRec_TrfOut_ErrorString
                                    ))
                            c.PrismMsg(_hObj, _tvTotal, _sMsg)
                        } else {
                            _iMsg = R.string.msg_sent_qty
                            c.PrismMsg(_hObj, _tvMsg, _iMsg)
                        }
                        c.PrismReqFocus(_hObj, _etItem)
                    } else {
                        c.PrismMsg(_hObj, _tvSKU, "")
                        c.PrismMsg(_hObj, _tvDesc, "")
                        c.PrismMsg(_hObj, _tvTotal, "")
                        c.PrismMsg(_hObj, _etQty, "")
                        _sQty = ""
                        _sMsg = c.PrismGetString(
                            g.REC_TrfOut,
                            g.iRec_TrfOut_ErrorString,
                            g.sRec_TrfOut_ErrorString
                        )
                        c.PrismMsg(_hObj, _tvTotal, _sMsg)
                        c.PrismReqFocus(_hObj, _etQty)
                    }
                } //if (s.ServerSend(g.REC_TrfOut, _hObj, _tvMsg, _pbAct)) {
            } //if (_sQty.equals(_sTrfoutDetailQty) == false) {
            exit()
        }

        private fun exit() {
            _iTrfoutDetailScanFlag = 0
        }
    }
}