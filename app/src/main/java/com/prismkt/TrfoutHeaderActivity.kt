package com.prismkt

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class TrfoutHeaderActivity constructor() : AppCompatActivity() {
    internal enum class troAction {
        CREATE, FIRST, NEXT
    }

    private val c: Common = Common.Companion.instance
    private val s: Server = Server.Companion.instance
    private var _hObj: Handler? = null
    private var _etToStore: EditText? = null
    private var _tvUserId: TextView? = null
    private var _tvTransaction: TextView? = null
    private var _tvDate: TextView? = null
    private var _tvReference: TextView? = null
    private var _tvEntries: TextView? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnOK: Button? = null
    private var _btnIDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_DETAILS: Button? = null
    private var _btnIDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_RELEASE: Button? = null
    private var _sToStore: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trfout_header)
        _etToStore = findViewById<View>(R.id.IDC_TRFOUT_TRFOUTHEADER_EDIT_TOSTORE) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_TRFOUT_TRFOUTHEADER_TEXT_MSG) as TextView?
        _btnOK = findViewById<View>(R.id.IDOK) as Button?
        _btnIDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_DETAILS =
            findViewById<View>(R.id.IDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_DETAILS) as Button?
        _btnIDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_RELEASE =
            findViewById<View>(R.id.IDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_RELEASE) as Button?
        if (g.REC_Security.get(g.iRec_Security_ShipLookup) != 0x30.toByte()) {
            Arrays.fill(g.REC_TrfOut, 0.toByte())
            _hObj = Handler(Looper.getMainLooper())
            _tvUserId = findViewById<View>(R.id.IDC_TRFOUT_TRFOUTHEADER_EDIT_USERID) as TextView?
            _tvUserId!!.setText(
                c.PrismGetString(
                    g.REC_Logon,
                    g.iRec_Logon_UserId,
                    g.sRec_Logon_UserId
                )
            )
            _tvTransaction =
                findViewById<View>(R.id.IDC_TRFOUT_TRFOUTHEADER_EDIT_TRANNO) as TextView?
            _tvDate = findViewById<View>(R.id.IDC_TRFOUT_TRFOUTHEADER_EDIT_DATE) as TextView?
            _tvReference =
                findViewById<View>(R.id.IDC_TRFOUT_TRFOUTHEADER_EDIT_REFERENCE) as TextView?
            _tvEntries = findViewById<View>(R.id.IDC_TRFOUT_TRFOUTHEADER_EDIT_ENTRIES) as TextView?
            _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
            if ((g.REC_PDConfig.get(g.iRec_PDConfig_CreateTrfOut) != 0x31.toByte()) &&
                (g.REC_PDConfig.get(g.iRec_PDConfig_CreateTrfOut) != 0x32.toByte())) {
                var btn: Button =
                    findViewById<View>(R.id.IDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_FIRST) as Button
                btn.setEnabled(true)
                btn = findViewById<View>(R.id.IDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_NEXT) as Button
                btn.setEnabled(true)
            }
            if (g.REC_Security.get(g.iRec_Security_ShipLookup) == 0x30.toByte()) {
                val btn: Button =
                    findViewById<View>(R.id.IDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_STORES) as Button
                btn.setEnabled(false)
            }
            _etToStore!!.requestFocus()
            _etToStore!!.setOnKeyListener(object : View.OnKeyListener {
                public override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
                    if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
                        _btnOK!!.performClick()
                        return true
                    }
                    return false
                }
            })
        } else {
            _tvMsg!!.setText(R.string.msg_not_authorized)
            _etToStore!!.setEnabled(false)
            _btnOK!!.setEnabled(false)
            var btn: Button =
                findViewById<View>(R.id.IDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_STORES) as Button
            btn.setEnabled(false)
            btn = findViewById<View>(R.id.IDCANCEL) as Button
            btn.requestFocus()
        }
    }

    override fun onResume() {
        super.onResume()
        if (g.REC_Receiver[g.iRec_Receiver_OrigLocation] != 0x00.toByte() || _etToStore!!.text.toString().trim { it <= ' ' }.isNotEmpty()) { //At orientation change, TextView loses text, EditText doesn't.
            c.PrismLayoutRestore(this, R.id.clTrfoutHeader, true)
            if (g.REC_Receiver[g.iRec_Receiver_OrigLocation] != 0x00.toByte()) { //LookupActivity
                _sToStore = c.PrismGetString(g.REC_Receiver, g.iRec_Receiver_OrigLocation, g.sRec_Receiver_OrigLocation)
                _etToStore!!.setText(_sToStore)
            }
        } else {
            c.PrismLayoutRestore(this, R.id.clTrfoutHeader, false)
        }
        val filter: IntentFilter = IntentFilter()
        filter.addCategory(g.sFilterCategory)
        filter.addAction(g.sFilterAction)
        registerReceiver(scanBroadcastReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clTrfoutHeader)
        unregisterReceiver(scanBroadcastReceiver)
    }

    fun btnIDOK(view: View?) {
        _sToStore = _etToStore!!.getText().toString().trim({ it <= ' ' })
        if (_sToStore!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_store_must_be_entered)
        } else {
            executeAction(troAction.CREATE)
        }
    }

    fun btnIDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_FIRST(view: View?) {
        _sToStore = _etToStore!!.getText().toString().trim({ it <= ' ' })
        if (_sToStore!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_store_must_be_entered)
        } else {
            executeAction(troAction.FIRST)
        }
    }

    fun btnIDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_NEXT(view: View?) {
        _sToStore = _etToStore!!.getText().toString().trim({ it <= ' ' })
        if (_sToStore!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_store_must_be_entered)
        } else {
            executeAction(troAction.NEXT)
        }
    }

    fun btnIDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_RELEASE(view: View?) {
        executeRelease()
    }

    fun btnIDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_STORES(view: View?) {
        val intent: Intent = Intent(this, LookupActivity::class.java)
        intent.putExtra("iLookupWhat", 0) //LU_STRMST
        startActivity(intent)
    }

    fun btnIDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_DETAILS(view: View?) {
        val intent: Intent = Intent(this, TrfoutDetailActivity::class.java)
        startActivity(intent)
    }

    fun btnIDCANCEL(view: View?) {
        onBackPressed()
    }

    public override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed()
            return true
        }
        return (super.onOptionsItemSelected(item))
    }

    private val scanBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        public override fun onReceive(context: Context, intent: Intent) {
            if ((intent.getAction() == g.sFilterAction)) {
                try {
                    displayScanResult(intent)
                } catch (ignored: Exception) {
                }
            }
        }
    }

    private fun displayScanResult(i: Intent) {
        _etToStore!!.setText(i.getStringExtra(g.sScanData))
        _btnOK!!.performClick()
    }

    private fun executeAction(eCall: troAction) {
        Thread(ExecuteThread(eCall)).start()
    }

    private fun executeRelease() {
        Thread(object : Runnable {
            public override fun run() {
                c.PrismCopyString(
                    g.REC_TrfOut,
                    g.iRecordId,
                    g.sRecordId,
                    "RelTrf4307"
                ) //DbFunction 25; Format 98; ReleaseTrfOut; PmWinDbm.app (14); Calls ShpRelease, PrnShipTx(SHH:TrnNo,TRUE)
                c.PrismMsg(_hObj, _tvMsg, R.string.msg_tro_releasing)
                if (s.ServerSend(g.REC_TrfOut, _hObj, _tvMsg, _pbAct)) {
                    if (g.REC_TrfOut.get(g.iRec_TrfOut_ReadStatus) == 0x31.toByte()) {
                        c.PrismMsg(_hObj, _tvMsg, R.string.msg_tro_released)
                    } else {
                        c.PrismMsg(
                            _hObj,
                            _tvMsg,
                            c.PrismGetString(
                                g.REC_TrfOut,
                                g.iRec_TrfOut_ErrorString,
                                g.sRec_TrfOut_ErrorString
                            )
                        )
                    }
                }
            }
        }).start()
    }

    internal inner class ExecuteThread constructor(private val _eCall: troAction) : Runnable {
        public override fun run() {
            val bCtl: Boolean
            var sMsg: String?
            var iMsg: Int
            Arrays.fill(g.REC_TrfOut, 0.toByte())
            c.PrismCopyString(
                g.REC_TrfOut,
                g.iRecordId,
                g.sRecordId,
                "TrfOut4307"
            ) //DbFunction 23; Format 98; ProcessTransferOut; PmWinDbm.app (14); StrMst.tps, ShpHdr.tps
            c.PrismCopyString(g.REC_TrfOut, g.iRec_TrfOut_ToStore, g.sRec_TrfOut_ToStore, _sToStore)
            System.arraycopy(
                g.REC_Logon,
                g.iRec_Logon_UserId,
                g.REC_TrfOut,
                g.iRec_TrfOut_EmpID,
                g.sRec_TrfOut_EmpID
            )
            when (_eCall) {
                troAction.CREATE -> {
                    g.REC_TrfOut[g.iRec_TrfOut_TrfType] = 0x41 //'A'
                    iMsg = R.string.msg_creating_new_to
                }
                troAction.FIRST -> {
                    g.REC_TrfOut[g.iRec_TrfOut_TrfType] = 0x49 //'I'
                    iMsg = R.string.msg_getting_first_tro
                }
                troAction.NEXT -> {
                    g.REC_TrfOut[g.iRec_TrfOut_TrfType] = 0x4E //'N'
                    iMsg = R.string.msg_getting_next_tro
                }
                else -> return
            }
            c.PrismMsg(_hObj, _tvMsg, iMsg)
            if (s.ServerSend(g.REC_TrfOut, _hObj, _tvMsg, _pbAct)) {
                sMsg = c.PrismGetString(g.REC_TrfOut, g.iRec_TrfOut_EmpID, g.sRec_TrfOut_EmpID)
                c.PrismMsg(_hObj, _tvUserId, sMsg)
                sMsg = c.PrismGetString(g.REC_TrfOut, g.iRec_TrfOut_TranNo, g.sRec_TrfOut_TranNo)
                c.PrismMsg(_hObj, _tvTransaction, sMsg)
                sMsg = c.PrismGetString(g.REC_TrfOut, g.iRec_TrfOut_Date, g.sRec_TrfOut_Date)
                c.PrismMsg(_hObj, _tvDate, sMsg)
                sMsg =
                    c.PrismGetString(g.REC_TrfOut, g.iRec_TrfOut_Reference, g.sRec_TrfOut_Reference)
                c.PrismMsg(_hObj, _tvReference, sMsg)
                sMsg = c.PrismGetString(g.REC_TrfOut, g.iRec_TrfOut_Entries, g.sRec_TrfOut_Entries)
                c.PrismMsg(_hObj, _tvEntries, sMsg)
                if (g.REC_TrfOut.get(g.iRec_TrfOut_ReadStatus) == 0x31.toByte()) {
                    when (_eCall) {
                        troAction.CREATE -> iMsg = R.string.msg_created_new_to
                        troAction.FIRST -> iMsg = R.string.msg_got_first_tro
                        troAction.NEXT -> iMsg = R.string.msg_got_next_tro
                    }
                    c.PrismMsg(_hObj, _tvMsg, iMsg)
                } else {
                    sMsg = c.PrismGetString(
                        g.REC_TrfOut,
                        g.iRec_TrfOut_ErrorString,
                        g.sRec_TrfOut_ErrorString
                    )
                    c.PrismMsg(_hObj, _tvMsg, sMsg)
                }
            }
            bCtl = g.REC_TrfOut.get(g.iRec_TrfOut_TranNo) != 0.toByte()
            c.PrismBtnEnabled(_hObj, _btnIDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_DETAILS, bCtl)
            if (g.REC_PDConfig.get(g.iRec_PDConfig_ReleaseTrfOut) != 0x30.toByte()) {
                c.PrismBtnEnabled(_hObj, _btnIDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_RELEASE, bCtl)
            }
            if (bCtl) {
                c.PrismReqFocus(_hObj, _btnIDC_TRFOUT_TRFOUTHEADER_PUSHBUTTON_DETAILS)
            } else {
                c.PrismReqFocus(_hObj, _etToStore)
            }
        }
    }
}