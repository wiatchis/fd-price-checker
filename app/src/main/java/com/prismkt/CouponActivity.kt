package com.prismkt

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
//import com.prism.CouponRecyclerViewAdapter.ItemClickListener
import java.util.*
import kotlin.concurrent.withLock

class CouponActivity : AppCompatActivity(), CouponRecyclerViewAdapter.ItemClickListener {
    private val c: Common = Common.instance
    private val s: Server = Server.instance
    private var _hObj: Handler? = null
    private var _aObj: CouponActivity? = null
    private var _rvAdapter: CouponRecyclerViewAdapter? = null
    private var _rvCoupons: RecyclerView? = null
    private var _rvData: ArrayList<ByteArray>? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _sCouponSKU: String? = null
    private var _iCouponWhat: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coupon)
        _aObj = this
        _hObj = Handler(Looper.getMainLooper())
        _tvMsg = findViewById<View>(R.id.tvMsg) as TextView?
        _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
        _rvCoupons = findViewById<View>(R.id.rvCoupons) as RecyclerView?
        val llm = LinearLayoutManager(this)
        _rvCoupons!!.setLayoutManager(llm)
        val did = DividerItemDecoration(_rvCoupons!!.getContext(), llm.getOrientation())
        _rvCoupons!!.addItemDecoration(did)
        _rvData = ArrayList()
        val lll: LinearLayout = findViewById<View>(R.id.llHeaderLanscape) as LinearLayout
        val llp: LinearLayout = findViewById<View>(R.id.llHeaderPortrait) as LinearLayout
        val cl: ConstraintLayout = findViewById<View>(R.id.clCoupons) as ConstraintLayout
        val cs = ConstraintSet()
        cs.clone(cl)
        var sMsg = ""
        _iCouponWhat = getIntent().getIntExtra("iCouponWhat", 0)
        if (_iCouponWhat != 0) {
            val sbSku: StringBuilder = StringBuilder()
            _sCouponSKU =
                c.PrismGetItemSku(g.REC_InvMst, g.iRec_InvMst_SKU, g.sRec_InvMst_SKU, sbSku)
            sMsg = sbSku.toString()
            if (!(sMsg == "")) _sCouponSKU = sMsg
            sMsg = _sCouponSKU + " " + c.PrismGetDescString()
        }
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            llp.setVisibility(View.GONE) //Not working?
            lll.setVisibility(View.VISIBLE)
            cs.connect(lll.getId(), ConstraintSet.BOTTOM, _rvCoupons!!.getId(), ConstraintSet.TOP)
            run({
                var i: Int = 0
                val l: Int = llp.getChildCount()
                while (i < l) {
                    llp.getChildAt(i).setVisibility(View.GONE)
                    i++
                }
            })
            var i: Int = 0
            val l: Int = llp.getChildCount()
            while (i < l) {
                lll.getChildAt(i).setVisibility(View.VISIBLE)
                i++
            }
            if (_iCouponWhat == 0) {
                setTitle(R.string.act_coupon)
            } else {
                setTitle(R.string.act_coupon_sku)
                setTitle(getTitle().toString() + " " + sMsg)
            }
        } else {
            lll.setVisibility(View.GONE)
            llp.setVisibility(View.VISIBLE)
            cs.connect(llp.getId(), ConstraintSet.BOTTOM, _rvCoupons!!.getId(), ConstraintSet.TOP)
            run({
                var i: Int = 0
                val l: Int = lll.getChildCount()
                while (i < l) {
                    lll.getChildAt(i).setVisibility(View.GONE)
                    i++
                }
            })
            var i: Int = 0
            val l: Int = llp.getChildCount()
            while (i < l) {
                llp.getChildAt(i).setVisibility(View.VISIBLE)
                i++
            }
            if (_iCouponWhat == 0) {
                setTitle(R.string.act_coupon_port)
            } else {
                setTitle(sMsg)
            }
        }
        cs.applyTo(cl)
        getList()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed()
            return true
        }
        return (super.onOptionsItemSelected(item))
    }

    override fun onItemClick(view: View?, position: Int) {
        val item: ByteArray = _rvData!!.get(position)
        System.arraycopy(item, 0, g.REC_CpnBrw, 0, g.sRec_CpnBrw)
        val intent: Intent = Intent(this, CouponDetailActivity::class.java)
        intent.putExtra("iCouponDetailWhat", 0)
        startActivity(intent)
    }

    private fun getList() { Thread(ExecuteThread()).start() }

    internal inner class ExecuteThread constructor() : Runnable {
        public override fun run() {
            Arrays.fill(g.REC_CpnBrw, 0.toByte())
            c.PrismCopyString(
                g.REC_CpnBrw,
                g.iRecordId,
                g.sRecordId,
                "CpnBrw"
            ) //DbFunction 35; Format 110; CouponsLookup; PmWinDbm.app (42).
            g.REC_CpnBrw[g.iRec_CpnBrw_Action] = 0x49 //'I'
            if (_iCouponWhat == 1) c.PrismCopyString(
                g.REC_CpnBrw,
                g.iRec_CpnBrw_SKU,
                g.sRec_CpnBrw_SKU,
                _sCouponSKU
            )
            c.PrismMsg(_hObj, _tvMsg, R.string.msg_getting_coupon)
            while (s.ServerSend(g.REC_CpnBrw, _hObj, _tvMsg, _pbAct)) {
                if (g.REC_CpnBrw.get(g.iRec_CpnBrw_ReadStatus) == 0x31.toByte()) {
                    g.REC_CpnBrw[g.iRec_CpnBrw_Action] = 0x4E //'N'
                    val ba: ByteArray = ByteArray(g.sRec_CpnBrw)
                    System.arraycopy(g.REC_CpnBrw, 0, ba, 0, g.sRec_CpnBrw)
                    _rvData!!.add(ba)
                } else {
                    val sMsg: String? = c.PrismGetString(
                        g.REC_CpnBrw,
                        g.iRec_CpnBrw_ErrorString,
                        g.sRec_CpnBrw_ErrorString
                    )
                    c.PrismMsg(_hObj, _tvMsg, sMsg)
                    _hObj!!.post(object : Runnable {
                        override fun run() {
                            Server._syncToken.withLock {
                                _rvAdapter = CouponRecyclerViewAdapter(_aObj!!, _rvData!!)
                                _rvAdapter!!.setClickListener(_aObj)
                                _rvCoupons!!.setAdapter(_rvAdapter)
                            }
                            /*
                            synchronized(Server.Companion._syncToken, {
                                _rvAdapter = CouponRecyclerViewAdapter(_aObj!!, _rvData!!)
                                _rvAdapter!!.setClickListener(_aObj)
                                _rvCoupons!!.setAdapter(_rvAdapter)
                            })
                             */
                        }
                    })
                    break
                }
            }
        }
    }
}