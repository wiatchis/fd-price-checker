package com.prismkt

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity

class MainMenuActivity constructor() : AppCompatActivity() {
    private val s: Server = Server.Companion.instance
    private val p: Printer = Printer.Companion.instance
    private lateinit var _rbArray: Array<RadioButton?>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)
        _rbArray = arrayOfNulls(5)
        _rbArray[0] = findViewById<View>(R.id.IDC_RADIO_MAINMENU_MAINMENU_INVENTOR) as RadioButton?
        _rbArray[1] = findViewById<View>(R.id.IDC_RADIO_MAINMENU_MAINMENU_PRICING) as RadioButton?
        _rbArray[2] = findViewById<View>(R.id.IDC_RADIO_MAINMENU_MAINMENU_SERVERCONNECT) as RadioButton?
        _rbArray[3] = findViewById<View>(R.id.IDC_RADIO_MAINMENU_MAINMENU_PRINTERCONNECT) as RadioButton?
        _rbArray[4] = findViewById<View>(R.id.IDC_RADIO_MAINMENU_MAINMENU_UPDATECONFIG) as RadioButton?
    }

    override fun onResume() {
        super.onResume()
        val bEnable: Boolean
        if (_rbArrayIndex >= 0 && _rbArrayIndex < _rbArray.size) {
            _rbArray.get(_rbArrayIndex)!!.setChecked(true)
            bEnable = true
        } else {
            bEnable = false
        }
        val btnOK: Button = findViewById<View>(R.id.IDOK) as Button
        btnOK.setEnabled(bEnable)
    }

    fun rbClicked(view: View?) {
        executeAction()
    }

    fun btnIDOK(view: View?) {
        executeAction()
    }

    fun btnIDCANCEL(view: View?) {
        s.Disconnect()
        p.Disconnect()
        finish()
    }

    public override fun onKeyDown(keyCode: Int,event: KeyEvent): Boolean { //onBackPressed() api lvl 5+
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            s.Disconnect()
            p.Disconnect()
            finish()
            return true
        }
        if (keyCode >= KeyEvent.KEYCODE_1 && keyCode <= KeyEvent.KEYCODE_5) {
            _rbArrayIndex = keyCode - KeyEvent.KEYCODE_1
            _rbArray.get(_rbArrayIndex)!!.setChecked(true)
            val btnOK: Button = findViewById<View>(R.id.IDOK) as Button
            btnOK.performClick()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun executeAction() {
        _rbArrayIndex = Common.Companion.instance.PrismGetMenuItem(_rbArray)
        when (_rbArrayIndex) {
            0 -> {
                val intent: Intent = Intent(this, InventoryMenuActivity::class.java)
                startActivity(intent)
            }
            1 -> {
                intent = Intent(this, PricingMenuActivity::class.java)
                startActivity(intent)
            }
            2 -> {
                intent = Intent(this, MainActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK) //FLAG_ACTIVITY_CLEAR_TOP
                intent.putExtra("iServerChange", 2)
                startActivity(intent)
                finish()
            }
            3 -> {
                intent = Intent(this, PrintConnectActivity::class.java)
                startActivity(intent)
            }
            4 -> {
                intent = Intent(this, LogonActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK) //FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
                finish()
            }
        }
    }

    companion object {
        private var _rbArrayIndex: Int = -1
    }
}