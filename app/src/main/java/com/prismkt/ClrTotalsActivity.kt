package com.prismkt

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.RadioButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class ClrTotalsActivity constructor() : AppCompatActivity() {
    private val c: Common = Common.Companion.instance
    private val s: Server = Server.Companion.instance
    private var _hObj: Handler? = null
    private var _tvDoc: TextView? = null
    private var _tvBOL: TextView? = null
    private var _tvPalletsShipped: TextView? = null
    private var _tvPalletsReceived: TextView? = null
    private var _tvUnitsShipped: TextView? = null
    private var _tvUnitsReceived: TextView? = null
    private var _tvMsg: TextView? = null
    private lateinit var _rbArray: Array<RadioButton?>
    private var _pbAct: ProgressBar? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clr_totals)
        _tvDoc = findViewById<View>(R.id.IDC_CLR_TOTALS_EDIT_DOC) as TextView?
        _tvBOL = findViewById<View>(R.id.IDC_CLR_TOTALS_EDIT_BOL) as TextView?
        _tvPalletsShipped = findViewById<View>(R.id.IDC_CLR_TOTALS_EDIT_CARTONSSHIPPED) as TextView?
        _tvPalletsReceived =
            findViewById<View>(R.id.IDC_CLR_TOTALS_EDIT_CARTONSRECEIVED) as TextView?
        _tvUnitsShipped = findViewById<View>(R.id.IDC_CLR_TOTALS_EDIT_UNITSSHIPPED) as TextView?
        _tvUnitsReceived = findViewById<View>(R.id.IDC_CLR_TOTALS_EDIT_UNITSRECEIVED) as TextView?
        _tvMsg = findViewById<View>(R.id.IDC_CLR_TOTALS_TEXT_MSG) as TextView?
        _rbArray = arrayOfNulls(2)
        _rbArray[0] = findViewById<View>(R.id.IDC_CLR_TOTALS_RADIO_BYPALLETS) as RadioButton?
        _rbArray[1] = findViewById<View>(R.id.IDC_CLR_TOTALS_RADIO_BYSKUS) as RadioButton?
        _hObj = Handler(Looper.getMainLooper())
        _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
        if (savedInstanceState == null) {
            Arrays.fill(g.REC_ClrTot, 0.toByte())
            val btnIDOK: Button = findViewById<View>(R.id.IDOK) as Button
            btnIDOK.performClick()
        }
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(this, R.id.clClrTotals, (g.REC_ClrTot.get(g.iRecordId) != 0x00.toByte()))
        if (_iClrTotalsPrintRadio >= 0 && _iClrTotalsPrintRadio < _rbArray.size) {
            _rbArray.get(_iClrTotalsPrintRadio)!!.setChecked(true)
        }
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clClrTotals)
    }

    fun rbClicked(view: View?) {
        _iClrTotalsPrintRadio = c.PrismGetMenuItem(_rbArray)
    }

    fun btnIDOK(view: View?) {
        executeAction()
    }

    fun btnIDCANCEL(view: View?) {
        onBackPressed()
    }

    public override fun onKeyDown(
        keyCode: Int,
        event: KeyEvent
    ): Boolean { //onBackPressed() api lvl 5+
        if (keyCode >= KeyEvent.KEYCODE_1 && keyCode <= KeyEvent.KEYCODE_2) {
            _iClrTotalsPrintRadio = keyCode - KeyEvent.KEYCODE_1
            _rbArray.get(_iClrTotalsPrintRadio)!!.setChecked(true)
            val btnOK: Button = findViewById<View>(R.id.IDOK) as Button
            btnOK.performClick()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    public override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed()
            return true
        }
        return (super.onOptionsItemSelected(item))
    }

    private fun executeAction() {
        Thread(object : Runnable {
            public override fun run() {
                if (g.REC_ClrTot.get(g.iRecordId) == 0x00.toByte()) {
                    c.PrismCopyString(
                        g.REC_ClrTot,
                        g.iRecordId,
                        g.sRecordId,
                        "ClrTot210"
                    ) //DbFunction 30; Format 93; CartonTot; PmWinDbm.app (15)
                    g.REC_ClrTot[g.iRec_ClrTot_PrintReport] = 0x20
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_getting_totals)
                } else {
                    g.REC_ClrTot[g.iRec_ClrTot_PrintReport] = (0x31 + _iClrTotalsPrintRadio).toByte() //'1'
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_sending_print_request)
                }
                if (s.ServerSend(g.REC_ClrTot, _hObj, _tvMsg, _pbAct)) {
                    if (g.REC_ClrTot.get(g.iRec_ClrTot_ReadStatus) == 0x31.toByte()) {
                        if (g.REC_ClrTot.get(g.iRec_ClrTot_PrintReport) == 0x20.toByte()) {
                            c.PrismMsg(
                                _hObj, _tvDoc,
                                c.PrismGetString(
                                    g.REC_ClrTot,
                                    g.iRec_ClrTot_Source,
                                    g.sRec_ClrTot_Source
                                ) +
                                        c.PrismGetString(
                                            g.REC_ClrTot,
                                            g.iRec_ClrTot_TranNo,
                                            g.sRec_ClrTot_TranNo
                                        )
                            )
                            c.PrismMsg(
                                _hObj,
                                _tvBOL,
                                c.PrismGetString(g.REC_ClrTot, g.iRec_ClrTot_BOL, g.sRec_ClrTot_BOL)
                            )
                            c.PrismMsg(
                                _hObj,
                                _tvPalletsShipped,
                                c.PrismGetString(
                                    g.REC_ClrTot,
                                    g.iRec_ClrTot_CtnQtyShp,
                                    g.sRec_ClrTot_CtnQtyShp
                                )
                            )
                            c.PrismMsg(
                                _hObj,
                                _tvPalletsReceived,
                                c.PrismGetString(
                                    g.REC_ClrTot,
                                    g.iRec_ClrTot_CtnQtyRec,
                                    g.sRec_ClrTot_CtnQtyRec
                                )
                            )
                            c.PrismMsg(
                                _hObj,
                                _tvUnitsShipped,
                                c.PrismGetString(
                                    g.REC_ClrTot,
                                    g.iRec_ClrTot_SelQtyShp,
                                    g.sRec_ClrTot_SelQtyShp
                                )
                            )
                            c.PrismMsg(
                                _hObj,
                                _tvUnitsReceived,
                                c.PrismGetString(
                                    g.REC_ClrTot,
                                    g.iRec_ClrTot_SelQtyRec,
                                    g.sRec_ClrTot_SelQtyRec
                                )
                            )
                            if (g.REC_ClrTot.get(g.iRec_ClrTot_ErrorString) != 0x00.toByte()) {
                                c.PrismMsg(
                                    _hObj, _tvMsg,
                                    (getResources().getString(R.string.msg_got_totals) + " " +
                                            getResources().getString(R.string.msg_with_error) + " " +
                                            c.PrismGetString(
                                                g.REC_ClrTot,
                                                g.iRec_ClrTot_ErrorString,
                                                g.sRec_ClrTot_ErrorString
                                            ))
                                )
                            } else {
                                c.PrismMsg(_hObj, _tvMsg, R.string.msg_got_totals)
                                _hObj!!.post(object : Runnable {
                                    public override fun run() {
                                        synchronized(Server.Companion._syncToken, {
                                            _rbArray.get(_iClrTotalsPrintRadio)!!
                                                .requestFocus()
                                        })
                                    }
                                })
                            }
                        } else {
                            c.PrismMsg(_hObj, _tvMsg, R.string.msg_report_printed_out)
                        } //if (g.REC_ClrTot[g.iRec_ClrTot_PrintReport] == 0x20.toByte()) {
                    } else {
                        c.PrismMsg(
                            _hObj,
                            _tvMsg,
                            c.PrismGetString(
                                g.REC_ClrTot,
                                g.iRec_ClrTot_ErrorString,
                                g.sRec_ClrTot_ErrorString
                            )
                        )
                    } //if (g.REC_ClrTot[g.iRec_ClrTot_ReadStatus] == 0x31.toByte()) {
                } //if (s.ServerSend(g.REC_ClrTot, _hObj, _tvMsg, _pbAct)) {
            } //public void run() {
        }).start()
    }

    companion object {
        private var _iClrTotalsPrintRadio: Int = 0
    }
}