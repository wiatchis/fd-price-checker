package com.prismkt

import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.activity.ComponentActivity
import androidx.constraintlayout.widget.ConstraintLayout
import java.util.*
import kotlin.concurrent.withLock

class Common private constructor() {
    fun PrismCopyString(a: ByteArray, i: Int, l: Int, s: String?) {
        var l = l
        l--
        Arrays.fill(a, i, i + l, 0.toByte())
        var x = 0
        while (((x < l) && (x < s!!.length))) {
            a[i + x] = s.toByteArray()[x]
            x++
        }
    }

    fun PrismGetString(a: ByteArray?, i: Int, l: Int): String {
        val b = ByteArray(l)
        Arrays.fill(b, 0.toByte())
        System.arraycopy(a, i, b, 0, l)
        var s: String
        try {
            s = String(b, charset("UTF-8")).trim { it <= ' ' }
        } catch (e: Exception) {
            s = ""
        }
        return s
    }

    fun PrismGetItemSku(s: ByteArray, si: Int, st: Int, sbSku: StringBuilder): String {
        var sbSku = sbSku
        var i: Int
        var j = -2
        val bSku = ByteArray(80)
        Arrays.fill(bSku, 0.toByte())
        i = 0
        while (i < st) {
            if (j < -1) {
                if (s[si + i] == 0.toByte()) j = -1
            } else {
                if (j == -1) {
                    if (s[si + i] != 0.toByte()) {
                        j = 0
                        i--
                    }
                } else {
                    if (s[si + i] == 0.toByte()) break
                    if (j < bSku.size) {
                        bSku[j] = s[si + i]
                        j += 1
                    }
                }
            }
            i++
        }
        var sSku: String
        try {
            sSku = String(bSku, charset("UTF-8")).trim { it <= ' ' }
        } catch (e: Exception) {
            sSku = ""
        }
        sbSku = sbSku.append(sSku)
        val b = ByteArray(st)
        Arrays.fill(b, 0.toByte())
        i = 0
        while (i < st) {
            if (s[si + i] == 0.toByte()) break
            b[i] = s[si + i]
            i++
        }
        var sItem: String
        try {
            sItem = String(b, charset("UTF-8")).trim { it <= ' ' }
        } catch (e: Exception) {
            sItem = ""
        }
        return sItem

        /*
        size_t  i;
        int     j = -2;

        memset(szSku,0,stSku);
        for (i = 0; i < st; i++) {
            if (j < -1) {
                if (s[i] == '\0') j = -1;
            } else {
                if (j == -1) {
                    if (s[i] != '\0') {j = 0; i--;}
                } else {
                    if (s[i] == '\0') break;
                    if ((j >= 0) && ((size_t)j < stSku)) {
                        szSku[j] = s[i];
                        j += 1;
                    }
                }
            }
        }
        return s;
        */
    }

    fun PrismGetDescString(): String {
        var i: Int
        var j: Int
        val l = (g.sRec_InvMst_Description1 + g.sRec_InvMst_Description2)
        val b = ByteArray(l)
        Arrays.fill(b, 0.toByte())
        System.arraycopy(g.REC_InvMst, g.iRec_InvMst_Description1, b, 0, g.sRec_InvMst_Description1)
        i = 0
        while (i < l) {
            if (b[i] == 0.toByte()) {
                System.arraycopy(
                    g.REC_InvMst,
                    g.iRec_InvMst_Description2,
                    b,
                    i,
                    g.sRec_InvMst_Description2
                )
                break
            }
            i++
        }
        var s: String
        try {
            s = String(b, charset("UTF-8")).trim { it <= ' ' }
        } catch (e: Exception) {
            s = ""
        }
        return s
    }

    fun PrismMsg(hObj: Handler?, tvMsg: TextView?, sMsg: String?) {
        hObj!!.post(Runnable {
            Server._syncToken.withLock { tvMsg!!.setText(sMsg) }
            //synchronized(Server.Companion._syncToken) { tvMsg!!.setText(sMsg) }
        })
    }

    fun PrismMsg(hObj: Handler?, tvMsg: TextView?, iMsg: Int) {
        hObj!!.post(object : Runnable {
            override fun run() {
                Server._syncToken.withLock { tvMsg!!.setText(iMsg) }
                //synchronized(Server.Companion._syncToken) { tvMsg!!.setText(iMsg) }
            }
        })
    }

    fun PrismReqFocus(hObj: Handler?, etControl: EditText?) {
        hObj!!.post(object : Runnable {
            override fun run() {
                Server._syncToken.withLock { etControl!!.requestFocus() }
                //synchronized(Server.Companion._syncToken) { etControl!!.requestFocus() }
            }
        })
    }

    fun PrismReqFocus(hObj: Handler?, btnControl: Button?) {
        hObj!!.post(object : Runnable {
            override fun run() {
                Server._syncToken.withLock { btnControl!!.requestFocus() }
                //synchronized(Server.Companion._syncToken) { btnControl!!.requestFocus() }
            }
        })
    }

    fun PrismProgressBar(hObj: Handler?, pbAct: ProgressBar?, iVisibility: Int) {
        hObj!!.post(object : Runnable {
            override fun run() {
                Server._syncToken.withLock { pbAct!!.setVisibility(iVisibility) }
                //synchronized(Server.Companion._syncToken) { pbAct!!.setVisibility(iVisibility) }
            }
        })
    }

    fun PrismGetMenuItem(a: Array<RadioButton?>): Int {
        var iRet = -1
        var j: Int
        j = 0
        while (j < a.size) {
            if (a[j]!!.isChecked) {
                iRet = j
                break
            }
            j++
        }
        return iRet
    }

    fun PrismBtnEnabled(hObj: Handler?, btnControl: Button?, bEnable: Boolean) {
        hObj!!.post(object : Runnable {
            override fun run() {
                Server._syncToken.withLock { btnControl!!.setEnabled(bEnable)}
                //synchronized(Server.Companion._syncToken) { btnControl!!.setEnabled(bEnable) }
            }
        })
    }

    fun PrismLayoutSave(act: ComponentActivity, id: Int) {
        g.mapSaveRestoreViews.remove(id)
        val cl = act.findViewById<View>(id) as ConstraintLayout
        val mapIdStrings: MutableMap<Int, String> = HashMap()
        var vId: Int
        var sSave: String
        var i = 0
        val l = cl.childCount
        while (i < l) {
            val v = cl.getChildAt(i)
            vId = v.id
            if (v is Button) {
                if (v.isEnabled) sSave = "1" else sSave = "0"
                mapIdStrings[vId] = sSave
            } else {
                if (v is TextView) {
                    sSave = v.text.toString()
                    mapIdStrings[vId] = sSave
                } else {
                    if (v is Spinner) {
                        sSave = v.selectedItemPosition.toString()
                        mapIdStrings[vId] = sSave
                    }
                }
            }
            i++
        }
        g.mapSaveRestoreViews[id] = mapIdStrings
    }

    fun PrismLayoutRestore(act: ComponentActivity, id: Int, fRestore: Boolean) {
        if (!g.mapSaveRestoreViews.containsKey(id)) return
        if (fRestore) {
            val mapIdStrings = g.mapSaveRestoreViews.get(id) ?: return
            val cl = act.findViewById<View>(id) as ConstraintLayout
            var vId: Int
            var sRest: String?
            var i = 0
            val l = cl.childCount
            while (i < l) {
                val v = cl.getChildAt(i)
                vId = v.id
                if (v is Button) {
                    if (mapIdStrings.containsKey(vId)) {
                        sRest = mapIdStrings[vId]
                        if (sRest == null) {
                            i++
                            continue
                        }
                        val bEnabled = sRest != "0"
                        v.isEnabled = bEnabled
                    }
                } else {
                    if (v is TextView) {
                        if (mapIdStrings.containsKey(vId)) {
                            sRest = mapIdStrings[vId]
                            v.text = sRest
                        }
                    } else {
                        if (v is Spinner) {
                            if (mapIdStrings.containsKey(vId)) {
                                sRest = mapIdStrings[vId]
                                if (sRest == null) {
                                    i++
                                    continue
                                }
                                v.setSelection(sRest.toInt())
                            }
                        }
                    }
                }
                i++
            }
        }
        g.mapSaveRestoreViews.remove(id)
    }

    fun PrismReqFocus(hObj: Handler?, etControl: EditText?, imm: InputMethodManager) {
        hObj!!.postDelayed(object : Runnable {
            override fun run() {
                Server._syncToken.withLock {
                    etControl!!.setSelection(etControl.getText().length)
                    etControl.requestFocus()
                    val result = IMMResult()
                    imm.showSoftInput(etControl, 0, result)
                    val res: Int = result.result
                    //if ((res == InputMethodManager.RESULT_HIDDEN) || (res == -1) || (res == InputMethodManager.RESULT_UNCHANGED_HIDDEN)) {
                    if ((res == InputMethodManager.RESULT_HIDDEN) || (res == InputMethodManager.RESULT_UNCHANGED_HIDDEN)) {
                        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
                    }
                }
            }
        }, 200)
    }

    private class IMMResult() : ResultReceiver(null) {
        var _result = -1
        public override fun onReceiveResult(r: Int, data: Bundle?) {
            _result = r
            //Log.i(g.TAG,"onReceiveResult");
        }

        //It looks like InteliJ issue. See https://stackoverflow.com/questions/66764515/how-to-deal-with-call-to-thread-sleep-in-a-loop-probably-busy-waiting
        val result: Int
            get() {
                try {
                    var sleep = 0
                    while (_result == -1 && sleep < 500) {
                        Thread.sleep(100) //It looks like Intelij issue. See https://stackoverflow.com/questions/66764515/how-to-deal-with-call-to-thread-sleep-in-a-loop-probably-busy-waiting
                        sleep += 100
                    }
                } catch (ignored: InterruptedException) { }
                return _result
            }
    }

    companion object {
        val instance = Common()
    }
}