//
//WEA 04/08/24 - 04/16/24 Added Serial Number to the title bar
//
package com.prismkt

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.TypedValue
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import java.util.*

/*
com.prism.g
import androidx.constraintlayout.widget.ConstraintLayout
import com.prism.Common.IMMResult
import android.os.Bundle
import com.prism.Server.RecvThread
import com.prism.Server.Tick
import androidx.appcompat.app.AppCompatActivity
import android.os.Looper
import com.prism.CouponRecyclerViewAdapter.ItemClickListener
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.constraintlayout.widget.ConstraintSet
import com.prism.RcvHeaderActivity.rcvAction
import com.prism.RtvHeaderActivity.rtvAction
import com.prism.ClrScanBolActivity.csbAction
import com.prism.StockCountActivity.scaAction
import com.prism.TrfoutHeaderActivity.troAction
import com.prism.DamagesReportActivity.draAction
import android.view.LayoutInflater
import android.view.ViewGroup
import android.app.Activity
*/
class PriceCheckerActivity constructor() : AppCompatActivity(), View.OnKeyListener {
    private val c: Common = Common.instance
    private val s: Server = Server.instance
    private var _aObj: PriceCheckerActivity? = null
    private var _hObj: Handler? = null
    private var _clWidget: ConstraintLayout? = null
    private var _clChildren: Int? = null
    private var _etItem: EditText? = null
    private var _tvDesc: TextView? = null
    private var _tvPrice: TextView? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnOK: Button? = null
    private var _ivPicture: ImageView? = null
    private var _ivPictureError: ImageView? = null
    private var _sItem: String? = null
    private val _timer = object: CountDownTimer(30000, 30000) {
        override fun onTick(p0: Long) {}
        override fun onFinish() {
            setControls(View.GONE)
            _ivPicture!!.visibility = View.VISIBLE
        }
    }

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setCustomView(R.layout.custom_action_bar_layout)

        val tvTitleName = findViewById<View>(R.id.titleName) as TextView
        tvTitleName.text = getString(R.string.act_price_checker)

        val tvSerialNumber = findViewById<View>(R.id.serialNumber) as TextView
        tvSerialNumber.text = getString(R.string.act_price_check_serial_number, MainActivity.serialNumber)

        setContentView(R.layout.activity_price_checker)
        _aObj = this
        _clWidget = findViewById<View>(R.id.clPriceChecker) as ConstraintLayout
        _clChildren = _clWidget!!.childCount
        _etItem = findViewById<View>(R.id.IDC_PRICING_PRICECHK_EDIT_ITEM) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_PRICING_PRICECHK_TEXT_MSG) as TextView?
        _btnOK = findViewById<View>(R.id.IDOK) as Button?
        _ivPicture = findViewById<View>(R.id.imageView) as ImageView
        _ivPictureError = findViewById<View>(R.id.imageViewError) as ImageView
        if (g.REC_Security.get(g.iRec_Security_PriceVerify) != 0x30.toByte()) {
            _hObj = Handler(Looper.getMainLooper())
            _tvMsg = findViewById<View>(R.id.IDC_PRICING_PRICECHK_TEXT_MSG) as TextView?
            _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
            _tvDesc = findViewById<View>(R.id.IDC_PRICING_PRICECHK_TEXT_DESC) as TextView?
            _tvPrice = findViewById<View>(R.id.IDC_PRICING_PRICECHK_TEXT_PRICE) as TextView?
            _etItem!!.setFocusable(false)
            //_etItem!!.setOnKeyListener(this)
            //_etItem!!.requestFocus()
            setControls(View.GONE)
            _ivPicture!!.visibility = View.VISIBLE
        } else {
            _tvMsg!!.setText(R.string.msg_not_authorized)
            _etItem!!.setEnabled(false)
            _btnOK!!.setEnabled(false)
            val btn: Button = findViewById<View>(R.id.IDCANCEL) as Button
            btn.requestFocus()
        }
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(this,R.id.clPriceChecker,_etItem!!.getText().toString().trim { it <= ' ' }.isNotEmpty())
        val filter: IntentFilter = IntentFilter()
        filter.addCategory(g.sFilterCategory)
        filter.addAction(g.sFilterAction)
        registerReceiver(scanBroadcastReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clPriceChecker)
        unregisterReceiver(scanBroadcastReceiver)
    }

    override fun onBackPressed() {
        return;
    }

    public override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
        if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
            _btnOK!!.performClick()
            return true
        }
        return false
    }

    fun btnIDOK(view: View?) {
        _sItem = _etItem!!.getText().toString().trim({ it <= ' ' })
        if (_sItem!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_item_must_be_entered)
        } else {
            if (_sItem!!.length == 30) {
                _sItem = parseInCommBarCode(_sItem!!)
            }
            executeAction()
        }
        _tvDesc!!.setText("");
        _tvPrice!!.setText("");
        setControls(View.VISIBLE)
        _ivPicture!!.visibility = View.GONE
        _ivPictureError!!.visibility = View.GONE
        _timer.start()
    }

    fun btnIDCANCEL(view: View?) {
        onBackPressed()
    }

    private fun parseInCommBarCode(sBarCode: String) : String {
        var sUPC: String = sBarCode.substring(0,11)
        var iOddSum: Int = 0
        var iEventSum: Int = 0
        var iValue: Int
        for(i in 1..11) {
            iValue = (sUPC[i-1].code - '0'.code)
            if ((i % 2) == 1) {
                iOddSum += iValue
            } else {
                iEventSum += iValue
            }
        }
        iValue = ((iOddSum * 3) + iEventSum) % 10
        if (iValue  != 0) {
            iValue = 10 - iValue
        }
        sUPC += (iValue + '0'.code).toChar()
        return sUPC
    }

    private fun executeAction() {
        Thread(object : Runnable {
            public override fun run() {
                var sMsg: String?
                Arrays.fill(g.REC_InvMst, 0.toByte())
                c.PrismCopyString(g.REC_InvMst,g.iRecordId,g.sRecordId,"InvMst210") //DbFunction 4; Format 79; GetItemDetail; PmWinDbm.app (11).
                c.PrismCopyString(g.REC_InvMst, g.iRec_InvMst_SKU, g.sRec_InvMst_SKU, _sItem)
                c.PrismMsg(_hObj, _tvMsg, R.string.msg_getting_product)
                if (s.ServerSend(g.REC_InvMst, _hObj, _tvMsg, _pbAct)) {
                    if (g.REC_InvMst.get(g.iRec_InvMst_ReadStatus) == 0x31.toByte()) {
                        val sbSku: StringBuilder = StringBuilder()
                        sMsg = c.PrismGetItemSku(g.REC_InvMst,g.iRec_InvMst_SKU,g.sRec_InvMst_SKU,sbSku)
                        c.PrismMsg(_hObj, _etItem, sMsg)
                        sMsg = c.PrismGetDescString()
                        c.PrismMsg(_hObj, _tvDesc, sMsg)
                        val fFontSize: Float = if (sMsg.length < 25) 12.0F else 11.0F
                        sMsg = c.PrismGetString(g.REC_InvMst,g.iRec_InvMst_Price,g.sRec_InvMst_Price)
                        //sMsg = getResources().getString(R.string.msg_regular_price) + sMsg;
                        sMsg = "$$sMsg";
                        c.PrismMsg(_hObj, _tvPrice, sMsg)
                        c.PrismMsg(_hObj, _tvMsg, "")
                        _hObj!!.post(object : Runnable {
                            public override fun run() {
                                synchronized(Server.Companion._syncToken, {
                                    _etItem!!.visibility = View.GONE
                                    _tvDesc!!.setTextSize(TypedValue.COMPLEX_UNIT_MM, fFontSize)
                                    _tvPrice!!.setTextSize(TypedValue.COMPLEX_UNIT_MM, fFontSize)
                                })
                            }
                        })
                    } else {
                        c.PrismMsg(_hObj, _tvDesc, "")
                        c.PrismMsg(_hObj, _tvPrice, "")
                        sMsg = c.PrismGetString(g.REC_InvMst,g.iRec_InvMst_ErrorString,g.sRec_InvMst_ErrorString)
                        if (sMsg.isNotEmpty()) {
                            if (sMsg == getResources().getString(R.string.msg_got_item_not_found)) {
                                //c.PrismMsg(_hObj, _tvMsg, R.string.msg_got_product_not_found)
                                _hObj!!.post(object : Runnable {
                                    public override fun run() {
                                        synchronized(Server.Companion._syncToken, {
                                            setControls(View.GONE)
                                            _ivPictureError!!.visibility = View.VISIBLE })
                                    }
                                })
                            } else {
                                c.PrismMsg(_hObj, _tvMsg, sMsg)
                            }
                        } else {
                            _hObj!!.post(object : Runnable {
                                override fun run() {
                                    val intent = Intent(_aObj, MainActivity::class.java)
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK) //FLAG_ACTIVITY_CLEAR_TOP
                                    startActivity(intent)
                                    finish()
                                }
                            })
                        }
                    }
                } else {
                    sMsg = resources.getString(R.string.msg_server_send_error)
                    sMsg = "$_tvMsg$sMsg"
                    c.PrismMsg(_hObj, _tvMsg, sMsg)
                }
            }
        }).start()
    }

    private val scanBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        public override fun onReceive(context: Context, intent: Intent) {
            if ((intent.action == g.sFilterAction)) {
                try {
                    displayScanResult(intent)
                } catch (ignored: Exception) {}
            }
        }
    }

    private fun displayScanResult(i: Intent) {
        _etItem!!.setText(i.getStringExtra(g.sScanData))
        _btnOK!!.performClick()
    }

    private fun setControls(iView: Int) {
        var i = 0
        while (i < _clChildren!!) {
            if (_clWidget!!.getChildAt(i).visibility != View.INVISIBLE) _clWidget!!.getChildAt(i).visibility = iView
            i++
        }
    }
}