package com.prismkt

import android.content.*
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.InputType
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class ClrScanItemActivity constructor() : AppCompatActivity(), View.OnKeyListener {
    private val c: Common = Common.instance
    private val s: Server = Server.instance
    private var _hObj: Handler? = null
    private var _etItem: EditText? = null
    private var _tvSKU: TextView? = null
    private var _tvDesc: TextView? = null
    private var _tvTotal: TextView? = null
    private var _etQty: EditText? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnOK: Button? = null
    private var _sItem: String? = null
    private var _sQty: String? = null
    private var _sClrScanItemNew: String? = null
    private var _sClrScanItemQty: String? = null
    private var _iClrScanItemFlag: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clr_scan_item)
        _hObj = Handler(Looper.getMainLooper())
        _etItem = findViewById<View>(R.id.IDC_CLR_SCANITEM_EDIT_ITEM) as EditText?
        _tvSKU = findViewById<View>(R.id.IDC_CLR_SCANITEM_EDIT_SKU) as TextView?
        _tvDesc = findViewById<View>(R.id.IDC_CLR_SCANITEM_EDIT_DESC) as TextView?
        _tvTotal = findViewById<View>(R.id.IDC_CLR_SCANITEM_EDIT_TOTAL) as TextView?
        _etQty = findViewById<View>(R.id.IDC_CLR_SCANITEM_EDIT_QTY) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_CLR_SCANITEM_TEXT_MSG) as TextView?
        _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
        _btnOK = findViewById<View>(R.id.IDOK) as Button?
        _sClrScanItemNew = ""
        _iClrScanItemFlag = 0
        if (g.REC_ClrHdr.get(g.iRec_ClrHdr_TotalType) == 0x34.toByte()) { //'4'
            var tv: TextView = findViewById<View>(R.id.IDC_CLR_SCANITEM_TEXT_TOTAL) as TextView
            tv = findViewById<View>(R.id.IDC_CLR_SCANITEM_TEXT_TOTAL) as TextView
            tv.setText(R.string.pmt_received)
            tv = findViewById<View>(R.id.IDC_CLR_SCANITEM_TEXT_QTY) as TextView
            tv.setText(R.string.pmt_shipment)
            _etQty!!.setInputType(InputType.TYPE_NULL)
            _etQty!!.setFocusable(false)
        }
        var sMsg: String? =
            c.PrismGetString(g.REC_ClrHdr, g.iRec_ClrHdr_NewCartonId, g.sRec_ClrHdr_NewCartonId)
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            sMsg = getResources().getString(R.string.act_clr_carton_items) + " " + sMsg
        } else {
            sMsg =
                getResources().getString(R.string.act_clr_carton_items_port) + " " + sMsg + " " + getResources().getString(
                    R.string.act_clr_carton_items_port2
                )
        }
        setTitle(sMsg)
        _etItem!!.setOnKeyListener(this)
        _etQty!!.setOnKeyListener(this)
        _etItem!!.requestFocus()
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(
            this,
            R.id.clClrScanItem,
            !_etItem!!.getText().toString().trim({ it <= ' ' }).isEmpty()
        )
        val filter: IntentFilter = IntentFilter()
        filter.addCategory(g.sFilterCategory)
        filter.addAction(g.sFilterAction)
        registerReceiver(scanBroadcastReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clClrScanItem)
        unregisterReceiver(scanBroadcastReceiver)
    }

    public override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
        if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
            _btnOK!!.performClick()
            return true
        }
        return false
    }

    fun btnIDOK(view: View?) {
        _sItem = _etItem!!.getText().toString().trim({ it <= ' ' })
        if (_sItem!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_item_must_be_entered)
        } else {
            _sQty = _etQty!!.getText().toString().trim({ it <= ' ' })
            executeAction()
        }
    }

    fun btnIDCANCEL(view: View?) {
        onBackPressed()
    }

    public override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed()
            return true
        }
        return (super.onOptionsItemSelected(item))
    }

    private fun executeAction() {
        Thread(ExecuteThread()).start()
    }

    private val scanBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        public override fun onReceive(context: Context, intent: Intent) {
            if ((intent.getAction() == g.sFilterAction)) {
                try {
                    displayScanResult(intent)
                } catch (ignored: Exception) {
                }
            }
        }
    }

    private fun displayScanResult(i: Intent) {
        _etItem!!.setText(i.getStringExtra(g.sScanData))
        _sClrScanItemNew = ""
        _iClrScanItemFlag = 1
        if (g.REC_PDConfig.get(g.iRec_PDConfig_ImmediateScan) != 0x31.toByte()) {
            val imm: InputMethodManager =
                getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            c.PrismReqFocus(_hObj, _etQty, imm)
        }
        _btnOK!!.performClick()
    }

    internal inner class ExecuteThread constructor() : Runnable {
        public override fun run() {
            var iMsg: Int
            val iSentGot: Int
            var sMsg: String?
            if (!(_sItem == _sClrScanItemNew)) {
                Arrays.fill(g.REC_ClrDtl, 0.toByte())
                c.PrismCopyString(
                    g.REC_ClrDtl,
                    g.iRecordId,
                    g.sRecordId,
                    "ClrDtl210"
                ) //DbFunction 29; Format 92; CartonDtl; PmWinDbm.app (15); Calls ReceiveDetail or UnReceiveDetail in CLR.app (14/39)
                c.PrismCopyString(
                    g.REC_ClrDtl,
                    g.iRec_ClrDtl_NewItem,
                    g.sRec_ClrDtl_NewItem,
                    _sItem
                )
                if ((_iClrScanItemFlag != 0) && (g.REC_ClrHdr.get(g.iRec_ClrHdr_TotalType) != 0x34.toByte()) && (g.REC_PDConfig.get(
                        g.iRec_PDConfig_ImmediateScan
                    ) == 0x31.toByte())
                ) {
                    g.REC_ClrDtl[g.iRec_ClrDtl_Quantity] = 0x31 //'1'
                    System.arraycopy(
                        g.REC_ClrDtl,
                        g.iRec_ClrDtl_NewItem,
                        g.REC_ClrDtl,
                        g.iRec_ClrDtl_SKU,
                        g.sRec_ClrDtl_SKU
                    )
                    iMsg = R.string.msg_sending_item
                    iSentGot = 1
                } else {
                    iMsg = R.string.msg_getting_item
                    iSentGot = 0
                }
                c.PrismMsg(_hObj, _tvMsg, iMsg)
                if (s.ServerSend(g.REC_ClrDtl, _hObj, _tvMsg, _pbAct)) {
                    if (g.REC_ClrDtl.get(g.iRec_ClrDtl_ReadStatus) == 0x31.toByte()) {
                        _sClrScanItemNew = c.PrismGetString(
                            g.REC_ClrDtl,
                            g.iRec_ClrDtl_NewItem,
                            g.sRec_ClrDtl_NewItem
                        )
                        c.PrismMsg(_hObj, _etQty, _sClrScanItemNew)
                        sMsg = c.PrismGetString(g.REC_ClrDtl, g.iRec_ClrDtl_SKU, g.sRec_ClrDtl_SKU)
                        c.PrismMsg(_hObj, _tvSKU, sMsg)
                        System.arraycopy(
                            g.REC_ClrDtl,
                            g.iRec_ClrDtl_Description1,
                            g.REC_InvMst,
                            g.iRec_InvMst_Description1,
                            g.sRec_InvMst_Description1
                        )
                        System.arraycopy(
                            g.REC_ClrDtl,
                            g.iRec_ClrDtl_Description2,
                            g.REC_InvMst,
                            g.iRec_InvMst_Description2,
                            g.sRec_InvMst_Description2
                        )
                        sMsg = c.PrismGetDescString()
                        c.PrismMsg(_hObj, _tvDesc, sMsg)
                        sMsg = c.PrismGetString(
                            g.REC_ClrDtl,
                            g.iRec_ClrDtl_SelQtyRec,
                            g.sRec_ClrDtl_SelQtyRec
                        )
                        c.PrismMsg(_hObj, _tvTotal, sMsg)
                        sMsg = c.PrismGetString(
                            g.REC_ClrDtl,
                            g.iRec_ClrDtl_DefaultQty,
                            g.sRec_ClrDtl_DefaultQty
                        )
                        c.PrismMsg(_hObj, _etQty, sMsg)
                        if (g.REC_ClrDtl.get(g.iRec_ClrDtl_ErrorString) != 0x00.toByte()) {
                            if (iSentGot == 1) {
                                sMsg = getResources().getString(R.string.msg_sent_item)
                            } else {
                                sMsg = getResources().getString(R.string.msg_got_item)
                            }
                            sMsg += " " + getResources().getString(R.string.msg_with_error) + " " + c.PrismGetString(
                                g.REC_ClrDtl,
                                g.iRec_ClrDtl_ErrorString,
                                g.sRec_ClrDtl_ErrorString
                            )
                            c.PrismMsg(_hObj, _tvMsg, sMsg)
                            c.PrismReqFocus(_hObj, _etItem)
                        } else {
                            if (iSentGot == 1) {
                                _sClrScanItemQty = sMsg
                                iMsg = R.string.msg_sent_item
                            } else {
                                _sClrScanItemQty = ""
                                iMsg = R.string.msg_got_item
                            }
                            c.PrismMsg(_hObj, _tvMsg, iMsg)
                            if (g.REC_ClrHdr.get(g.iRec_ClrHdr_TotalType) != 0x34.toByte()) c.PrismReqFocus(
                                _hObj,
                                _etQty
                            )
                        }
                    } else {
                        c.PrismMsg(_hObj, _tvSKU, "")
                        c.PrismMsg(_hObj, _tvDesc, "")
                        c.PrismMsg(_hObj, _tvTotal, "")
                        c.PrismMsg(_hObj, _etQty, "")
                        _sQty = ""
                        sMsg = c.PrismGetString(
                            g.REC_ClrDtl,
                            g.iRec_ClrDtl_ErrorString,
                            g.sRec_ClrDtl_ErrorString
                        )
                        c.PrismMsg(_hObj, _tvMsg, sMsg)
                        c.PrismReqFocus(_hObj, _etItem)
                    }
                } //if (s.ServerSend(g.REC_ClrDtl, _hObj, _tvMsg, _pbAct)) {
                exit()
                return
            } //if (_sItem.equals(_sClrScanItemNew) == false) {
            if (g.REC_ClrHdr.get(g.iRec_ClrHdr_TotalType) == 0x34.toByte()) { //'4'
                c.PrismMsg(_hObj, _tvMsg, "")
                exit()
                return
            }
            if (!(_sClrScanItemQty == _sQty) && (
                        g.REC_ClrDtl.get(g.iRec_ClrDtl_ReadStatus) == 0x31.toByte()) && (
                        g.REC_ClrDtl.get(g.iRec_ClrDtl_SKU) != 0x00.toByte())
            ) {
                c.PrismCopyString(
                    g.REC_ClrDtl,
                    g.iRec_ClrDtl_Quantity,
                    g.sRec_ClrDtl_Quantity,
                    _sQty
                )
                c.PrismMsg(_hObj, _tvMsg, R.string.msg_sending_qty)
                if (s.ServerSend(g.REC_ClrDtl, _hObj, _tvMsg, _pbAct)) {
                    if (g.REC_ClrDtl.get(g.iRec_ClrDtl_ReadStatus) == 0x31.toByte()) {
                        _sClrScanItemNew = c.PrismGetString(
                            g.REC_ClrDtl,
                            g.iRec_ClrDtl_NewItem,
                            g.sRec_ClrDtl_NewItem
                        )
                        c.PrismMsg(_hObj, _etItem, _sClrScanItemNew)
                        System.arraycopy(
                            g.REC_ClrDtl,
                            g.iRec_ClrDtl_Description1,
                            g.REC_InvMst,
                            g.iRec_InvMst_Description1,
                            g.sRec_InvMst_Description1
                        )
                        System.arraycopy(
                            g.REC_ClrDtl,
                            g.iRec_ClrDtl_Description2,
                            g.REC_InvMst,
                            g.iRec_InvMst_Description2,
                            g.sRec_InvMst_Description2
                        )
                        sMsg = c.PrismGetDescString()
                        c.PrismMsg(_hObj, _tvDesc, sMsg)
                        sMsg = c.PrismGetString(
                            g.REC_ClrDtl,
                            g.iRec_ClrDtl_SelQtyRec,
                            g.sRec_ClrDtl_SelQtyRec
                        )
                        c.PrismMsg(_hObj, _tvTotal, sMsg)
                        sMsg = c.PrismGetString(
                            g.REC_ClrDtl,
                            g.iRec_ClrDtl_DefaultQty,
                            g.sRec_ClrDtl_DefaultQty
                        )
                        c.PrismMsg(_hObj, _etQty, sMsg)
                        _sClrScanItemQty = sMsg
                        if (g.REC_ClrDtl.get(g.iRec_ClrDtl_ErrorString) != 0x00.toByte()) {
                            sMsg = (getResources().getString(R.string.msg_sent_qty) + " " +
                                    getResources().getString(R.string.msg_with_error) + " " +
                                    c.PrismGetString(
                                        g.REC_ClrDtl,
                                        g.iRec_ClrDtl_ErrorString,
                                        g.sRec_ClrDtl_ErrorString
                                    ))
                            c.PrismMsg(_hObj, _tvMsg, sMsg)
                        } else {
                            c.PrismMsg(_hObj, _tvMsg, R.string.msg_sent_qty)
                        }
                        c.PrismReqFocus(_hObj, _etItem)
                    } else {
                        sMsg = c.PrismGetString(
                            g.REC_ClrDtl,
                            g.iRec_ClrDtl_ErrorString,
                            g.sRec_ClrDtl_ErrorString
                        )
                        c.PrismMsg(_hObj, _tvMsg, sMsg)
                        c.PrismReqFocus(_hObj, _etQty)
                    }
                } //if (s.ServerSend(g.REC_ClrDtl, _hObj, _tvMsg, _pbAct)) {
            } else { //if (_sQty.equals(_sClrScanItemQty) == false) {
                c.PrismMsg(_hObj, _tvMsg, "")
                _hObj!!.post(object : Runnable {
                    public override fun run() {
                        synchronized(
                            Server.Companion._syncToken,
                            { if (_etItem!!.isFocused()) _etQty!!.requestFocus() else _etItem!!.requestFocus() })
                    }
                })
            }
            exit()
        } //public void run() {

        private fun exit() {
            _iClrScanItemFlag = 0
        }
    }
}