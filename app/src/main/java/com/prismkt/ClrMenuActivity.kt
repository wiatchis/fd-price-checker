package com.prismkt

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class ClrMenuActivity constructor() : AppCompatActivity() {
    private lateinit var _rbArray: Array<RadioButton?>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clr_menu)
        _rbArray = arrayOfNulls(4)
        _rbArray[0] = findViewById<View>(R.id.IDC_RADIO_CLR_CLRMENU_BYBILLOFLANDING) as RadioButton
        _rbArray[1] = findViewById<View>(R.id.IDC_RADIO_CLR_CLRMENU_BYPOTRANSFER) as RadioButton
        _rbArray[2] = findViewById<View>(R.id.IDC_RADIO_CLR_CLRMENU_BYCARTONID) as RadioButton
        _rbArray[3] = findViewById<View>(R.id.IDC_RADIO_CLR_CLRMENU_CARTONINQUIRY) as RadioButton
    }

    override fun onResume() {
        super.onResume()
        val bEnable: Boolean
        if (_rbArrayIndex >= 0 && _rbArrayIndex < _rbArray.size) {
            _rbArray.get(_rbArrayIndex)!!.setChecked(true)
            bEnable = true
        } else {
            bEnable = false
        }
        val btnOK: Button = findViewById<View>(R.id.IDOK) as Button
        btnOK.setEnabled(bEnable)
    }

    fun rbClicked(view: View?) {
        executeAction()
    }

    fun btnIDOK(view: View?) {
        executeAction()
    }

    fun btnIDCANCEL(view: View?) {
        onBackPressed()
    }

    public override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode >= KeyEvent.KEYCODE_1 && keyCode <= KeyEvent.KEYCODE_7) {
            _rbArrayIndex = keyCode - KeyEvent.KEYCODE_1
            _rbArray.get(_rbArrayIndex)!!.setChecked(true)
            val btnOK: Button = findViewById<View>(R.id.IDOK) as Button
            btnOK.performClick()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun executeAction() {
        val intent: Intent
        _rbArrayIndex = Common.Companion.instance.PrismGetMenuItem(_rbArray)
        if (_rbArrayIndex == 0) {
            Arrays.fill(g.REC_ClrTot, 0.toByte())
            intent = Intent(this, ClrScanBolActivity::class.java)
        } else {
            if (_rbArrayIndex < _rbArray.size) {
                Arrays.fill(g.REC_ClrHdr, 0.toByte())
                intent = Intent(this, ClrScanCartonActivity::class.java)
            } else return
        }
        intent.putExtra("iWhat", _rbArrayIndex)
        startActivity(intent)
    }

    companion object {
        private var _rbArrayIndex: Int = -1
    }
}