package com.prismkt
import android.content.*
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import java.util.*
class EventVerificationActivity constructor() : AppCompatActivity() {
    private val c: Common = Common.Companion.instance
    private val s: Server = Server.Companion.instance
    private var _hObj: Handler? = null
    private var _etItem: EditText? = null
    private var _tvSKU: TextView? = null
    private var _tvDesc: TextView? = null
    private var _tvPrice: TextView? = null
    private var _tvNewAdPrice: TextView? = null
    private var _tvActiveDate: TextView? = null
    private var _tvCouponPrice: TextView? = null
    private var _tvCouponType: TextView? = null
    private var _tvCouponNumb: TextView? = null
    private var _tvCouponDesc: TextView? = null
    private var _tvCouponAdat: TextView? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnOK: Button? = null
    private var _btnIDC_PRICING_EVENTVER_PUSHBUTTON_DETAIL: Button? = null
    private var _btnIDC_PRICING_EVENTVER_PUSHBUTTON_MORECOUPONS: Button? = null
    private var _sItem: String? = null
    private var _sEventVerificationSKU: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_verification)
        _etItem = findViewById<View>(R.id.IDC_PRICING_EVENTVER_EDIT_ITEM) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_PRICING_EVENTVER_TEXT_MSG) as TextView?
        _btnOK = findViewById<View>(R.id.IDOK) as Button?
        _btnIDC_PRICING_EVENTVER_PUSHBUTTON_DETAIL =
            findViewById<View>(R.id.IDC_PRICING_EVENTVER_PUSHBUTTON_DETAIL) as Button?
        _btnIDC_PRICING_EVENTVER_PUSHBUTTON_MORECOUPONS =
            findViewById<View>(R.id.IDC_PRICING_EVENTVER_PUSHBUTTON_MORECOUPONS) as Button?
        if (g.REC_Security[g.iRec_Security_PriceVerify] != 0x30.toByte()) {
            _hObj = Handler(Looper.getMainLooper())
            _tvSKU = findViewById<View>(R.id.IDC_PRICING_EVENTVER_EDIT_SKU) as TextView?
            _tvDesc = findViewById<View>(R.id.IDC_PRICING_EVENTVER_EDIT_DESC) as TextView?
            _tvPrice = findViewById<View>(R.id.IDC_PRICING_EVENTVER_EDIT_PRICE) as TextView?
            _tvNewAdPrice =
                findViewById<View>(R.id.IDC_PRICING_EVENTVER_EDIT_NEWADPRICE) as TextView?
            _tvActiveDate =
                findViewById<View>(R.id.IDC_PRICING_EVENTVER_EDIT_ACTIVEDATE) as TextView?
            _tvCouponPrice =
                findViewById<View>(R.id.IDC_PRICING_EVENTVER_EDIT_COUPONPRICE) as TextView?
            _tvCouponType =
                findViewById<View>(R.id.IDC_PRICING_EVENTVER_EDIT_COUPONTYPE) as TextView?
            _tvCouponNumb =
                findViewById<View>(R.id.IDC_PRICING_EVENTVER_EDIT_COUPONNUMB) as TextView?
            _tvCouponDesc =
                findViewById<View>(R.id.IDC_PRICING_EVENTVER_EDIT_COUPONDESC) as TextView?
            _tvCouponAdat =
                findViewById<View>(R.id.IDC_PRICING_EVENTVER_EDIT_COUPONADAT) as TextView?
            _tvMsg = findViewById<View>(R.id.IDC_PRICING_EVENTVER_TEXT_MSG) as TextView?
            _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
            _sEventVerificationSKU = ""
            _etItem!!.requestFocus()
            _etItem!!.setOnKeyListener(object : View.OnKeyListener {
                public override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
                    if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
                        _btnOK!!.performClick()
                        return true
                    }
                    return false
                }
            })
        } else {
            _tvMsg!!.setText(R.string.msg_not_authorized)
            _etItem!!.setEnabled(false)
            _btnOK!!.setEnabled(false)
            val btn: Button = findViewById<View>(R.id.IDCANCEL) as Button
            btn.requestFocus()
        }
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(
            this,
            R.id.clEventVerification,
            !_etItem!!.getText().toString().trim({ it <= ' ' }).isEmpty()
        )
        val filter: IntentFilter = IntentFilter()
        filter.addCategory(g.sFilterCategory)
        filter.addAction(g.sFilterAction)
        registerReceiver(scanBroadcastReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clEventVerification)
        unregisterReceiver(scanBroadcastReceiver)
    }

    fun btnIDOK(view: View?) {
        _sItem = _etItem!!.getText().toString().trim({ it <= ' ' })
        if (_sItem!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_item_must_be_entered)
            _btnIDC_PRICING_EVENTVER_PUSHBUTTON_DETAIL!!.setEnabled(false)
            _btnIDC_PRICING_EVENTVER_PUSHBUTTON_MORECOUPONS!!.setEnabled(false)
        } else {
            executeAction()
        }
    }

    fun btnIDC_PRICING_EVENTVER_PUSHBUTTON_DETAIL(view: View?) {
        val intent: Intent = Intent(this, CouponDetailActivity::class.java)
        intent.putExtra("iCouponDetailWhat", 1)
        startActivity(intent)
    }

    fun btnIDC_PRICING_EVENTVER_PUSHBUTTON_MORECOUPONS(view: View?) {
        val intent: Intent = Intent(this, CouponActivity::class.java)
        intent.putExtra("iCouponWhat", 1)
        startActivity(intent)
    }

    fun btnIDCANCEL(view: View?) {
        onBackPressed()
    }

    private fun executeAction() {
        Thread(object : Runnable {
            public override fun run() {
                var bCtl: Boolean = false
                if (!(_sItem == _sEventVerificationSKU)) {
                    var sMsg: String?
                    Arrays.fill(g.REC_InvMst, 0.toByte())
                    c.PrismCopyString(
                        g.REC_InvMst,
                        g.iRecordId,
                        g.sRecordId,
                        "InvMst210"
                    ) //DbFunction 4; Format 79; GetItemDetail; PmWinDbm.app (11).
                    c.PrismCopyString(g.REC_InvMst, g.iRec_InvMst_SKU, g.sRec_InvMst_SKU, _sItem)
                    g.REC_InvMst[g.iRec_InvMst_FunctionID] = 0x45.toByte() //'E'
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_getting_item)
                    if (s.ServerSend(g.REC_InvMst, _hObj, _tvMsg, _pbAct)) {
                        if (g.REC_InvMst[g.iRec_InvMst_ReadStatus] == 0x31.toByte()) {
                            val sbSku: StringBuilder = StringBuilder()
                            _sEventVerificationSKU = c.PrismGetItemSku(
                                g.REC_InvMst,
                                g.iRec_InvMst_SKU,
                                g.sRec_InvMst_SKU,
                                sbSku
                            )
                            c.PrismMsg(_hObj, _etItem, _sEventVerificationSKU)
                            sMsg = sbSku.toString()
                            c.PrismMsg(_hObj, _tvSKU, sMsg)
                            Arrays.fill(g.REC_CpnSku, 0.toByte())
                            c.PrismCopyString(
                                g.REC_CpnSku,
                                g.iRecordId,
                                g.sRecordId,
                                "CpnSku"
                            ) //DbFunction 36; Format 111; CouponSkuPref; PmWinDbm.app (43); populates the new coupon fields on “PRISM Event Verification” screen.
                            if (sMsg != "") c.PrismCopyString(
                                g.REC_CpnSku,
                                g.iRec_CpnSku_SKU,
                                g.sRec_CpnSku_SKU,
                                sMsg
                            ) else System.arraycopy(
                                g.REC_InvMst,
                                g.iRec_InvMst_SKU,
                                g.REC_CpnSku,
                                g.iRec_CpnSku_SKU,
                                g.sRec_CpnSku_SKU
                            )
                            sMsg = c.PrismGetDescString()
                            c.PrismMsg(_hObj, _tvDesc, sMsg)
                            sMsg = c.PrismGetString(
                                g.REC_InvMst,
                                g.iRec_InvMst_Price,
                                g.sRec_InvMst_Price
                            )
                            c.PrismMsg(_hObj, _tvPrice, sMsg)
                            if (g.REC_InvMst[g.iRec_InvMst_DOANewReceived] == 0x21.toByte()) //'!'
                                sMsg = c.PrismGetString(
                                    g.REC_InvMst,
                                    g.iRec_InvMst_Disposition,
                                    g.sRec_InvMst_Disposition
                                ) else sMsg = c.PrismGetString(
                                g.REC_InvMst,
                                g.iRec_InvMst_DOANewReceived,
                                g.sRec_InvMst_DOANewReceived
                            )
                            c.PrismMsg(_hObj, _tvNewAdPrice, sMsg)
                            sMsg = c.PrismGetString(
                                g.REC_InvMst,
                                g.iRec_InvMst_DOAReceived,
                                g.sRec_InvMst_DOAReceived
                            )
                            c.PrismMsg(_hObj, _tvActiveDate, sMsg)
                            c.PrismMsg(_hObj, _tvMsg, R.string.msg_getting_coupon)
                            if (s.ServerSend(g.REC_CpnSku, _hObj, _tvMsg, _pbAct)) {
                                if (g.REC_CpnSku[g.iRec_CpnSku_ReadStatus] == 0x31.toByte()) {
                                    sMsg = c.PrismGetString(
                                        g.REC_CpnSku,
                                        g.iRec_CpnSku_Price,
                                        g.sRec_CpnSku_Price
                                    )
                                    c.PrismMsg(_hObj, _tvCouponPrice, sMsg)
                                    sMsg = c.PrismGetString(
                                        g.REC_CpnSku,
                                        g.iRec_CpnSku_CouponType,
                                        g.sRec_CpnSku_CouponType
                                    )
                                    c.PrismMsg(_hObj, _tvCouponType, sMsg)
                                    sMsg = c.PrismGetString(
                                        g.REC_CpnSku,
                                        g.iRec_CpnSku_CouponMasterId,
                                        g.sRec_CpnSku_CouponMasterId
                                    )
                                    c.PrismMsg(_hObj, _tvCouponNumb, sMsg)
                                    sMsg = c.PrismGetString(
                                        g.REC_CpnSku,
                                        g.iRec_CpnSku_Description,
                                        g.sRec_CpnSku_Description
                                    )
                                    c.PrismMsg(_hObj, _tvCouponDesc, sMsg)
                                    sMsg = c.PrismGetString(
                                        g.REC_CpnSku,
                                        g.iRec_CpnSku_ActiveDate,
                                        g.sRec_CpnSku_ActiveDate
                                    )
                                    c.PrismMsg(_hObj, _tvCouponAdat, sMsg)
                                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_got_item)
                                    bCtl = true
                                } else {
                                    c.PrismMsg(_hObj, _tvCouponPrice, "")
                                    c.PrismMsg(_hObj, _tvCouponType, "")
                                    c.PrismMsg(_hObj, _tvCouponNumb, "")
                                    c.PrismMsg(_hObj, _tvCouponDesc, "")
                                    c.PrismMsg(_hObj, _tvCouponAdat, "")
                                    sMsg = c.PrismGetString(
                                        g.REC_CpnSku,
                                        g.iRec_CpnSku_ErrorString,
                                        g.sRec_CpnSku_ErrorString
                                    )
                                    c.PrismMsg(_hObj, _tvMsg, sMsg)
                                }
                            }
                        } else {
                            c.PrismMsg(_hObj, _tvSKU, "")
                            c.PrismMsg(_hObj, _tvDesc, "")
                            c.PrismMsg(_hObj, _tvPrice, "")
                            c.PrismMsg(_hObj, _tvNewAdPrice, "")
                            c.PrismMsg(_hObj, _tvActiveDate, "")
                            c.PrismMsg(_hObj, _tvCouponPrice, "")
                            c.PrismMsg(_hObj, _tvCouponType, "")
                            c.PrismMsg(_hObj, _tvCouponNumb, "")
                            c.PrismMsg(_hObj, _tvCouponDesc, "")
                            c.PrismMsg(_hObj, _tvCouponAdat, "")
                            sMsg = c.PrismGetString(
                                g.REC_InvMst,
                                g.iRec_InvMst_ErrorString,
                                g.sRec_InvMst_ErrorString
                            )
                            c.PrismMsg(_hObj, _tvMsg, sMsg)
                        }
                    }
                } else { //if (!_sItem.equals(_sEventVerificationSKU)) {
                    c.PrismMsg(_hObj, _tvMsg, "")
                }
                c.PrismBtnEnabled(_hObj, _btnIDC_PRICING_EVENTVER_PUSHBUTTON_DETAIL, bCtl)
                c.PrismBtnEnabled(_hObj, _btnIDC_PRICING_EVENTVER_PUSHBUTTON_MORECOUPONS, bCtl)
            }
        }).start()
    }

    private val scanBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        public override fun onReceive(context: Context, intent: Intent) {
            if ((intent.getAction() == g.sFilterAction)) {
                try {
                    displayScanResult(intent)
                } catch (ignored: Exception) {
                }
            }
        }
    }

    private fun displayScanResult(i: Intent) {
        _etItem!!.setText(i.getStringExtra(g.sScanData))
        _btnOK!!.performClick()
    }
}