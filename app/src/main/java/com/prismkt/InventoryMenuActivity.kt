package com.prismkt
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import java.util.*
class InventoryMenuActivity constructor() : AppCompatActivity() {
    private lateinit var _rbArray: Array<RadioButton?>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inventory_menu)
        _rbArray = arrayOfNulls(7)
        _rbArray[0] = findViewById<View>(R.id.IDC_RADIO_INVENTORY_INVENTORYMENU_RECEIVING) as RadioButton
        _rbArray[1] = findViewById<View>(R.id.IDC_RADIO_INVENTORY_INVENTORYMENU_CARTONRECEIVING) as RadioButton
        _rbArray[2] = findViewById<View>(R.id.IDC_RADIO_INVENTORY_INVENTORYMENU_RTV) as RadioButton
        _rbArray[3] = findViewById<View>(R.id.IDC_RADIO_INVENTORY_INVENTORYMENU_TRANSFERSOUT) as RadioButton
        _rbArray[4] = findViewById<View>(R.id.IDC_RADIO_INVENTORY_INVENTORYMENU_PHYSICALINVENTORY) as RadioButton
        _rbArray[5] = findViewById<View>(R.id.IDC_RADIO_INVENTORY_INVENTORYMENU_INVENTORYINQUIRY) as RadioButton
        _rbArray[6] = findViewById<View>(R.id.IDC_RADIO_INVENTORY_INVENTORYMENU_DAMAGESREPORT) as RadioButton
    }

    override fun onResume() {
        super.onResume()
        val bEnable: Boolean
        if (InventoryMenuActivity.Companion._rbArrayIndex >= 0 && InventoryMenuActivity.Companion._rbArrayIndex < _rbArray.size) {
            _rbArray.get(InventoryMenuActivity.Companion._rbArrayIndex)!!.setChecked(true)
            bEnable = true
        } else {
            bEnable = false
        }
        val btnOK: Button = findViewById<View>(R.id.IDOK) as Button
        btnOK.setEnabled(bEnable)
    }

    fun rbClicked(view: View?) {
        executeAction()
    }

    fun btnIDOK(view: View?) {
        executeAction()
    }

    fun btnIDCANCEL(view: View?) {
        onBackPressed()
    }

    public override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode >= KeyEvent.KEYCODE_1 && keyCode <= KeyEvent.KEYCODE_7) {
            InventoryMenuActivity.Companion._rbArrayIndex = keyCode - KeyEvent.KEYCODE_1
            _rbArray.get(InventoryMenuActivity.Companion._rbArrayIndex)!!.setChecked(true)
            val btnOK: Button = findViewById<View>(R.id.IDOK) as Button
            btnOK.performClick()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun executeAction() {
        val intent: Intent
        InventoryMenuActivity.Companion._rbArrayIndex =
            Common.Companion.instance.PrismGetMenuItem(_rbArray)
        when (InventoryMenuActivity.Companion._rbArrayIndex) {
            0 -> {
                Arrays.fill(g.REC_Receiver, 0.toByte())
                intent = Intent(this, RcvHeaderActivity::class.java)
            }
            1 -> intent = Intent(this, ClrMenuActivity::class.java)
            2 -> {
                Arrays.fill(g.REC_Receiver, 0.toByte())
                intent = Intent(this, RtvHeaderActivity::class.java)
            }
            3 -> {
                Arrays.fill(g.REC_Receiver, 0.toByte())
                intent = Intent(this, TrfoutHeaderActivity::class.java)
            }
            4 -> {
                Arrays.fill(g.REC_StockCnt, 0.toByte())
                intent = Intent(this, StockCountActivity::class.java)
            }
            5 -> intent = Intent(this, InventoryItemActivity::class.java)
            else -> {
                Arrays.fill(g.REC_StockCnt, 0.toByte())
                intent = Intent(this, DamagesReportActivity::class.java)
            }
        }
        startActivity(intent)
    }

    companion object {
        private var _rbArrayIndex: Int = -1
    }
}