package com.prismkt
import android.app.Activity
import android.content.*
import android.content.res.Configuration
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
//import com.prism.*
import java.util.*
class CouponRecyclerViewAdapter internal constructor(context: Context, data: ArrayList<ByteArray>) :
    RecyclerView.Adapter<CouponRecyclerViewAdapter.ViewHolder>() {
    private val c: Common = Common.Companion.instance
    private val s: Server = Server.Companion.instance
    private val _data: ArrayList<ByteArray>
    private val _inflater: LayoutInflater
    private val _orientation: Int
    private var _clickListener: ItemClickListener? = null

    // data is passed into the constructor
    init {
        _inflater = LayoutInflater.from(context)
        _data = data
        val activity: Activity = context as Activity
        _orientation = activity.getResources().getConfiguration().orientation
    }

    // inflates the row layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CouponRecyclerViewAdapter.ViewHolder {
        val view: View
        view =
            if (_orientation == Configuration.ORIENTATION_LANDSCAPE) _inflater.inflate(
                R.layout.activity_coupon_recyclerview_item,
                parent,
                false
            ) else _inflater.inflate(
                R.layout.activity_coupon_recyclerview_item_portrait,
                parent,
                false
            )
        return ViewHolder(view)
    }

    // binds the data to the TextView in each row
    override fun onBindViewHolder(
        holder: CouponRecyclerViewAdapter.ViewHolder,
        position: Int
    ) {
        val item: ByteArray = _data.get(position)
        val sCouponMasterId: String =
            c.PrismGetString(item, g.iRec_CpnBrw_CouponMasterId, g.sRec_CpnBrw_CouponMasterId)
        val sDescription: String =
            c.PrismGetString(item, g.iRec_CpnBrw_Description, g.sRec_CpnBrw_Description)
        val sCouponType: String =
            c.PrismGetString(item, g.iRec_CpnBrw_CouponType, g.sRec_CpnBrw_CouponType)
        val sAutoApply: String =
            c.PrismGetString(item, g.iRec_CpnBrw_AutoApply, g.sRec_CpnBrw_AutoApply)
        val sStartDate: String =
            c.PrismGetString(item, g.iRec_CpnBrw_StartDate, g.sRec_CpnBrw_StartDate)
        val sEndDate: String = c.PrismGetString(item, g.iRec_CpnBrw_EndDate, g.sRec_CpnBrw_EndDate)
        holder._tvCouponMasterId.setText(sCouponMasterId)
        holder._tvDescription.setText(sDescription)
        holder._tvCouponType.setText(sCouponType)
        holder._tvAutoApply.setText(sAutoApply)
        holder._tvStartDate.setText(sStartDate)
        holder._tvEndDate.setText(sEndDate)
    }

    // total number of rows
    public override fun getItemCount(): Int {
        return _data.size
    }

    // stores and recycles views as they are scrolled off screen
    inner class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView),View.OnClickListener {
        var _tvCouponMasterId: TextView
        var _tvDescription: TextView
        var _tvCouponType: TextView
        var _tvAutoApply: TextView
        var _tvStartDate: TextView
        var _tvEndDate: TextView

        override fun onClick(view: View) {
            if (_clickListener != null) _clickListener!!.onItemClick(view, getAdapterPosition())
        }

        init {
            _tvCouponMasterId = itemView.findViewById(R.id.tvCouponMasterId)
            _tvDescription = itemView.findViewById(R.id.tvDescription)
            _tvCouponType = itemView.findViewById(R.id.tvCouponType)
            _tvAutoApply = itemView.findViewById(R.id.tvAutoApply)
            _tvStartDate = itemView.findViewById(R.id.tvStartDate)
            _tvEndDate = itemView.findViewById(R.id.tvEndDate)
            itemView.setOnClickListener(this)
        }
    }

    // convenience method for getting data at click position
    fun getItem(id: Int): ByteArray { return _data.get(id) }

    // allows clicks events to be caught
    fun setClickListener(itemClickListener: ItemClickListener?) {
        _clickListener = itemClickListener
    }

    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }
}