package com.prismkt

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class DamagesReportActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    internal enum class draAction { GET_REPORT, ADD_SHEET, FIRST, NEXT, SUBMIT_NO_PRINT, SUBMIT_PRINT }
    private val c: Common = Common.instance
    private val s: Server = Server.instance
    private var _hObj: Handler? = null
    private var _etSheet: EditText? = null
    private var _tvCreated: TextView? = null
    private var _tvCreatedBy: TextView? = null
    private var _spnReason: Spinner? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnIDOK: Button? = null
    private var _btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_SUBMIT: Button? = null
    private var _btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_COUNT: Button? = null
    private var _btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_NEXT: Button? = null
    private var _sSheet: String? = null
    private var _iReason: Int = 0
    private var _bCtl: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_damages_report)
        _etSheet = findViewById<View>(R.id.IDC_DAMAGES_DAMAGESREPORT_EDIT_SHEET) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_DAMAGES_DAMAGESREPORT_TEXT_MSG) as TextView?
        _btnIDOK = findViewById<View>(R.id.IDOK) as Button?
        _btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_SUBMIT =
            findViewById<View>(R.id.IDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_SUBMIT) as Button?
        _btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_COUNT =
            findViewById<View>(R.id.IDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_COUNT) as Button?
        _btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_NEXT =
            findViewById<View>(R.id.IDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_NEXT) as Button?
        if (g.REC_Security.get(g.iRec_Security_PhysicalInv) == 0x30.toByte()) {
            _tvMsg!!.setText(R.string.msg_not_authorized)
            _etSheet!!.setEnabled(false)
            _btnIDOK!!.setEnabled(false)
            _btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_NEXT!!.setEnabled(false)
            var btn: Button =
                findViewById<View>(R.id.IDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_ADDSHEET) as Button
            btn.setEnabled(false)
            btn = findViewById<View>(R.id.IDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_FIRST) as Button
            btn.setEnabled(false)
            btn = findViewById<View>(R.id.IDCANCEL) as Button
            btn.requestFocus()
            return
        }
        _hObj = Handler(Looper.getMainLooper())
        _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
        _etSheet!!.setText(_sDamagesReportSheet)
        _tvCreated = findViewById<View>(R.id.IDC_DAMAGES_DAMAGESREPORT_EDIT_DATETIME) as TextView?
        _tvCreatedBy =
            findViewById<View>(R.id.IDC_DAMAGES_DAMAGESREPORT_EDIT_CREATEDBY) as TextView?
        _spnReason = findViewById<View>(R.id.IDC_DAMAGES_DAMAGESREPORT_COMBOBOX_REASON) as Spinner?
        val adapter: ArrayAdapter<CharSequence> = ArrayAdapter.createFromResource(
            this,
            R.array.damages,
            android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        _spnReason!!.setAdapter(adapter)
        _spnReason!!.setOnItemSelectedListener(this)
        _etSheet!!.requestFocus()
        if ((_sDamagesReportSheet == "")) {
            val imm: InputMethodManager =
                getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            c.PrismReqFocus(_hObj, _etSheet, imm)
        }
        _etSheet!!.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
                if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
                    if (_bCtl) _btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_COUNT!!.performClick() else _btnIDOK!!.performClick()
                    return true
                }
                return false
            }
        })
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(
            this,
            R.id.clDamagesReport,
            (g.REC_StockCnt.get(g.iRec_StockCnt_CountId) != 0x00.toByte())
        )
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clDamagesReport)
    }

    fun btnIDOK(view: View?) {
        _sSheet = _etSheet!!.getText().toString().trim({ it <= ' ' })
        executeAction(draAction.GET_REPORT)
    }

    fun btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_ADDSHEET(view: View?) {
        _sSheet = _etSheet!!.getText().toString().trim({ it <= ' ' })
        executeAction(draAction.ADD_SHEET)
    }

    fun btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_FIRST(view: View?) {
        _sSheet = _etSheet!!.getText().toString().trim({ it <= ' ' })
        executeAction(draAction.FIRST)
    }

    fun btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_NEXT(view: View?) {
        _sSheet = _etSheet!!.getText().toString().trim({ it <= ' ' })
        executeAction(draAction.NEXT)
    }

    fun btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_SUBMIT(view: View?) {
        _sSheet = _etSheet!!.getText().toString().trim({ it <= ' ' })
        val alertDialog: AlertDialog = AlertDialog.Builder(this).create()
        alertDialog.setTitle(R.string.dlg_title_prism)
        alertDialog.setMessage(getResources().getString(R.string.dlg_question_print_report))
        alertDialog.setButton(
            AlertDialog.BUTTON_NEGATIVE,
            getResources().getString(R.string.dlg_answer_yes),
            object : DialogInterface.OnClickListener {
                override fun onClick(arg0: DialogInterface, arg1: Int) {
                    executeAction(draAction.SUBMIT_PRINT)
                }
            })
        alertDialog.setButton(
            AlertDialog.BUTTON_POSITIVE,
            getResources().getString(R.string.dlg_answer_no),
            object : DialogInterface.OnClickListener {
                override fun onClick(arg0: DialogInterface, arg1: Int) {
                    executeAction(draAction.SUBMIT_NO_PRINT)
                }
            })
        alertDialog.show()
    }

    fun btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_COUNT(view: View?) {
        val intent: Intent = Intent(this, DamagesItemActivity::class.java)
        startActivity(intent)
    }

    fun btnIDCANCEL(view: View?) {
        checkExit()
    }

    private fun executeAction(eCall: draAction) {
        Thread(ExecuteThread(eCall)).start()
        //Thread(DamagesReportActivity.ExecuteThread(eCall)).start()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            checkExit()
            return true
        }
        return (super.onOptionsItemSelected(item))
    }

    override fun onKeyDown(
        keyCode: Int,
        event: KeyEvent
    ): Boolean { //onBackPressed() api lvl 5+
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            checkExit()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun checkExit() {
        if (!_btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_SUBMIT!!.isEnabled()) {
            onBackPressed()
        } else {
            val alertDialog: AlertDialog = AlertDialog.Builder(this).create()
            alertDialog.setTitle(R.string.dlg_title_damages_exit)
            alertDialog.setMessage(getResources().getString(R.string.dlg_question_submit_report))
            alertDialog.setButton(
                AlertDialog.BUTTON_NEGATIVE,
                getResources().getString(R.string.dlg_answer_yes),
                object : DialogInterface.OnClickListener {
                    public override fun onClick(arg0: DialogInterface, arg1: Int) {
                        onBackPressed()
                    }
                })
            alertDialog.setButton(
                AlertDialog.BUTTON_POSITIVE,
                getResources().getString(R.string.dlg_answer_no),
                object : DialogInterface.OnClickListener {
                    public override fun onClick(arg0: DialogInterface, arg1: Int) {}
                })
            alertDialog.show()
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View, pos: Int, id: Long) {
        _iReason = pos
        _btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_SUBMIT!!.setEnabled((pos > 0));
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}

    fun DamagesReportReason(iWhat: Int) {
        val sMsg: String?
        if (iWhat == 0) {
            when (_iReason) {
                1 -> sMsg = "DM"
                2 -> sMsg = "SU"
                3 -> sMsg = "EX"
                4 -> sMsg = "PK"
                5 -> sMsg = "MP"
                6 -> sMsg = "RS"
                7 -> sMsg = "KS"
                8 -> sMsg = "BD"
                else -> sMsg = ""
            }
            c.PrismCopyString(g.REC_StockCnt,g.iRec_StockCnt_CountStatus,g.sRec_StockCnt_CountStatus,sMsg)
        } else {
            _iReason = 0
            sMsg = c.PrismGetString(g.REC_StockCnt,g.iRec_StockCnt_CountStatus,g.sRec_StockCnt_CountStatus)
            val aCountStatus: CharArray = CharArray(2)
            try {
                sMsg.toCharArray(aCountStatus, 0, 0, 2)
                when (aCountStatus.get(0)) {
                    'D' -> if (aCountStatus.get(1) == 'M') _iReason = 1
                    'S' -> if (aCountStatus.get(1) == 'U') _iReason = 2
                    'E' -> if (aCountStatus.get(1) == 'X') _iReason = 3
                    'P' -> if (aCountStatus.get(1) == 'K') _iReason = 4
                    'M' -> if (aCountStatus.get(1) == 'P') _iReason = 5
                    'R' -> if (aCountStatus.get(1) == 'S') _iReason = 6
                    'K' -> if (aCountStatus.get(1) == 'S') _iReason = 7
                    'B' -> if (aCountStatus.get(1) == 'D') _iReason = 8
                }
            } catch (e: Exception) {}
            _hObj!!.post(object : Runnable {
                public override fun run() {
                    synchronized(
                        Server.Companion._syncToken,
                        { _spnReason!!.setSelection(_iReason) })
                }
            })
        }
    }

    internal inner class ExecuteThread constructor(private val _eCall: draAction) : Runnable {
        public override fun run() {
            var iMsg: Int
            val sMsg: String?
            _bCtl = false
            Arrays.fill(g.REC_StockCnt, 0.toByte())
            c.PrismCopyString(
                g.REC_StockCnt,
                g.iRecordId,
                g.sRecordId,
                "StockCnt4426"
            ) //DbFunction 6(35); Format 107; PhyInvHdr; PmWinDbm.app (3); Updates PiShtHdr.tps
            g.REC_StockCnt[g.iRec_StockCnt_Action] = 0x32.toByte() //'2'
            c.PrismCopyString(
                g.REC_StockCnt,
                g.iRec_StockCnt_CountId,
                g.sRec_StockCnt_CountId,
                _sSheet
            )
            when (_eCall) {
                draAction.GET_REPORT -> if (g.REC_StockCnt[g.iRec_StockCnt_CountId] == 0x00.toByte()) {
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_report_must_be_entered)
                    exit()
                    return
                } else {
                    g.REC_StockCnt[g.iRec_StockCnt_AreaId] = 0x47.toByte() //'G'
                    iMsg = R.string.msg_getting_report
                    //break
                }
                draAction.ADD_SHEET -> {
                    g.REC_StockCnt[g.iRec_StockCnt_CountId] = 0x00.toByte()
                    g.REC_StockCnt[g.iRec_StockCnt_AreaId] = 0x41.toByte() //'A'
                    DamagesReportReason(0)
                    System.arraycopy(
                        g.REC_Logon,
                        g.iRec_Logon_UserId,
                        g.REC_StockCnt,
                        g.iRec_StockCnt_UserId,
                        g.sRec_StockCnt_UserId
                    )
                    iMsg = R.string.msg_adding_report
                }
                draAction.FIRST -> {
                    g.REC_StockCnt[g.iRec_StockCnt_AreaId] = 0x46.toByte() //'F'
                    iMsg = R.string.msg_getting_most_recent_open_report
                }
                draAction.NEXT -> {
                    g.REC_StockCnt[g.iRec_StockCnt_AreaId] = 0x4E.toByte() //'A'
                    iMsg = R.string.msg_getting_next_older_open_report
                }
                draAction.SUBMIT_NO_PRINT -> {
                    DamagesReportReason(0)
                    g.REC_StockCnt[g.iRec_StockCnt_AreaId] = 0x53.toByte() //'S'
                    iMsg = R.string.msg_releasing_report
                }
                draAction.SUBMIT_PRINT -> {
                    DamagesReportReason(0)
                    g.REC_StockCnt[g.iRec_StockCnt_AreaId] = 0x58.toByte() //'X'
                    iMsg = R.string.msg_releasing_report_printing
                }
                else -> return
            }
            c.PrismMsg(_hObj, _tvMsg, iMsg)
            if (s.ServerSend(g.REC_StockCnt, _hObj, _tvMsg, _pbAct)) {
                if (g.REC_StockCnt[g.iRec_StockCnt_ReadStatus] == 0x31.toByte()) {
                    _sSheet = c.PrismGetString(
                        g.REC_StockCnt,
                        g.iRec_StockCnt_CountId,
                        g.sRec_StockCnt_CountId
                    )
                    _sDamagesReportSheet = _sSheet as String
                    c.PrismMsg(_hObj, _etSheet, _sSheet)
                    val sbSku: StringBuilder = StringBuilder()
                    c.PrismGetItemSku(
                        g.REC_StockCnt,
                        g.iRec_StockCnt_ErrorString,
                        g.sRec_StockCnt_ErrorString,
                        sbSku
                    )
                    c.PrismMsg(_hObj, _tvCreated, sbSku.toString())
                    sMsg = c.PrismGetString(
                        g.REC_StockCnt,
                        g.iRec_StockCnt_UserId,
                        g.sRec_StockCnt_UserId
                    )
                    c.PrismMsg(_hObj, _tvCreatedBy, sMsg)
                    when (_eCall) {
                        draAction.GET_REPORT -> {
                            DamagesReportReason(1)
                            iMsg = R.string.msg_got_report
                        }
                        draAction.ADD_SHEET -> iMsg = R.string.msg_added_report
                        draAction.FIRST -> {
                            DamagesReportReason(1)
                            if (g.REC_StockCnt.get(g.iRec_StockCnt_CountId) != 0x00.toByte()) {
                                iMsg = R.string.msg_got_most_recent_open_report
                            } else {
                                iMsg = R.string.msg_got_no_open_reports
                            }
                        }
                        draAction.NEXT -> {
                            DamagesReportReason(1)
                            if (g.REC_StockCnt.get(g.iRec_StockCnt_AreaId) == 0x4E.toByte()) { //'N'
                                iMsg = R.string.msg_got_next_older_open_report
                            } else {
                                iMsg = R.string.msg_got_last_older_open_report
                            }
                        }
                        draAction.SUBMIT_NO_PRINT -> {
                            iMsg = R.string.msg_released_report
                            c.PrismMsg(_hObj, _tvMsg, iMsg)
                            exit()
                            return
                        }
                        draAction.SUBMIT_PRINT -> {
                            iMsg = R.string.msg_released_report_printed
                            c.PrismMsg(_hObj, _tvMsg, iMsg)
                            exit()
                            return
                        }
                    }
                    _bCtl = true
                    c.PrismMsg(_hObj, _tvMsg, iMsg)
                } else {
                    c.PrismMsg(_hObj, _tvCreated, "")
                    c.PrismMsg(_hObj, _tvCreatedBy, "")
                    sMsg = c.PrismGetString(
                        g.REC_StockCnt,
                        g.iRec_StockCnt_ErrorString,
                        g.sRec_StockCnt_ErrorString
                    )
                    c.PrismMsg(_hObj, _tvMsg, sMsg)
                }
            }
            exit()
        }

        private fun exit() {
            val bCtl2: Boolean
            bCtl2 =
                !_bCtl || (_eCall != draAction.NEXT) || (g.REC_StockCnt[g.iRec_StockCnt_AreaId] == 0x4E.toByte()) //'N'
            c.PrismBtnEnabled(_hObj, _btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_COUNT, _bCtl)
            c.PrismBtnEnabled(_hObj, _btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_SUBMIT, (_bCtl && (_iReason > 0)))
            c.PrismBtnEnabled(_hObj, _btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_NEXT, bCtl2)
            if (_bCtl) {
                c.PrismReqFocus(_hObj, _btnIDC_DAMAGES_DAMAGESREPORT_PUSHBUTTON_COUNT)
            } else {
                val imm: InputMethodManager =
                    getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                c.PrismReqFocus(_hObj, _etSheet, imm)
            }
        }
    }

    companion object {
        private var _sDamagesReportSheet: String = ""
    }
}