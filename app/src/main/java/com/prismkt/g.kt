package com.prismkt

import java.util.*
object g {
    const val iPriceChecker = 1
    const val istatus = 0
    const val sstatus = 1
    const val iRecordId = sstatus
    const val sRecordId = 13
    const val TAG = "PRISM" //It's for logging. Log.i(g.TAG,"onResume"); for example.
    var mapSaveRestoreViews: HashMap<Int,Map<Int,String>> = HashMap<Int,Map<Int,String>>()
    //var mapSaveRestoreViews: Map<Int, Map<Int, String>> = HashMap<Int,Map<Int,String>>()
    var sFilterAction: String? = null
    var sFilterCategory: String? = null
    var sScanData: String? = null

    /*
    struct Rec_Logon {  //"Logon4307" in Logon.cpp; DbFunction 1; Format 95; SecurityCheck; PmWinDbm.app (7).
       unsigned char status;
       char RecordId[13];
       char UserId[12];
       char Password[9];
       char Security[31];
       char ReadStatus[2];
       char ErrorString[81];
       char ErrorCode[6];
       char EmpName[31];
       char NetStatus[2];
    };
    */
    const val iRec_Logon_UserId = iRecordId + sRecordId
    const val sRec_Logon_UserId = 12
    const val iRec_Logon_Password = iRec_Logon_UserId + sRec_Logon_UserId
    const val sRec_Logon_Password = 9
    const val iRec_Logon_Security = iRec_Logon_Password + sRec_Logon_Password
    const val sRec_Logon_Security = 31
    const val iRec_Logon_ReadStatus = iRec_Logon_Security + sRec_Logon_Security
    const val sRec_Logon_ReadStatus = 2
    const val iRec_Logon_ErrorString = iRec_Logon_ReadStatus + sRec_Logon_ReadStatus
    const val sRec_Logon_ErrorString = 81
    const val iRec_Logon_ErrorCode = iRec_Logon_ErrorString + sRec_Logon_ErrorString
    const val sRec_Logon_ErrorCode = 6
    const val iRec_Logon_EmpName = iRec_Logon_ErrorCode + sRec_Logon_ErrorCode
    const val sRec_Logon_EmpName = 31
    const val iRec_Logon_NetStatus = iRec_Logon_EmpName + sRec_Logon_EmpName
    const val sRec_Logon_NetStatus = 2
    const val sRec_Logon = iRec_Logon_NetStatus + sRec_Logon_NetStatus
    var REC_Logon = ByteArray(sRec_Logon)

    /*
    struct Rec_PDConfig { //"PDConfig4206" in Logon.cpp; DbFunction 10; Format 70; ConfigurePDT; PmWinDbm.app (9); Gets from PdConfig.tps
       unsigned char status;
       char RecordId[13];
       char CustomRcv[2];
       char RcvPrompt1[6];
       char RcvPrompt2[6];
       char CustomSO[2];
       char SOPrompt1[16];
       char SOPrompt2[16];
       char CustomRTV[2];
       char ReadStatus[2];
       char ErrorString[81];
       char ErrorCode[6];
       char NetStatus[2];
       char PIShowCount[2];
       char SOShowCost[2];
       char RCVShowCount[2];
       char RcvNOF[2];
       char RcvNOFWarn[2];
       char TolerancePct[4];
       char RcvOptions[9];
       char CreateTrfOut[2];
       char ReleaseTrfOut[2];
       char ImmediateScan[2];
       char CreateRTV[2];
       char ReleaseRTV[2];
    };
    */
    const val iRec_PDConfig_CustomRcv = iRecordId + sRecordId
    const val sRec_PDConfig_CustomRcv = 2
    const val iRec_PDConfig_RcvPrompt1 = iRec_PDConfig_CustomRcv + sRec_PDConfig_CustomRcv
    const val sRec_PDConfig_RcvPrompt1 = 6
    const val iRec_PDConfig_RcvPrompt2 = iRec_PDConfig_RcvPrompt1 + sRec_PDConfig_RcvPrompt1
    const val sRec_PDConfig_RcvPrompt2 = 6
    const val iRec_PDConfig_CustomSO = iRec_PDConfig_RcvPrompt2 + sRec_PDConfig_RcvPrompt2
    const val sRec_PDConfig_CustomSO = 2
    const val iRec_PDConfig_SOPrompt1 = iRec_PDConfig_CustomSO + sRec_PDConfig_CustomSO
    const val sRec_PDConfig_SOPrompt1 = 16
    const val iRec_PDConfig_SOPrompt2 = iRec_PDConfig_SOPrompt1 + sRec_PDConfig_SOPrompt1
    const val sRec_PDConfig_SOPrompt2 = 16
    const val iRec_PDConfig_CustomRTV = iRec_PDConfig_SOPrompt2 + sRec_PDConfig_SOPrompt2
    const val sRec_PDConfig_CustomRTV = 2
    const val iRec_PDConfig_ReadStatus = iRec_PDConfig_CustomRTV + sRec_PDConfig_CustomRTV
    const val sRec_PDConfig_ReadStatus = 2
    const val iRec_PDConfig_ErrorString = iRec_PDConfig_ReadStatus + sRec_PDConfig_ReadStatus
    const val sRec_PDConfig_ErrorString = 81
    const val iRec_PDConfig_ErrorCode = iRec_PDConfig_ErrorString + sRec_PDConfig_ErrorString
    const val sRec_PDConfig_ErrorCode = 6
    const val iRec_PDConfig_NetStatus = iRec_PDConfig_ErrorCode + sRec_PDConfig_ErrorCode
    const val sRec_PDConfig_NetStatus = 2
    const val iRec_PDConfig_PIShowCount = iRec_PDConfig_NetStatus + sRec_PDConfig_NetStatus
    const val sRec_PDConfig_PIShowCount = 2
    const val iRec_PDConfig_SOShowCost = iRec_PDConfig_PIShowCount + sRec_PDConfig_PIShowCount
    const val sRec_PDConfig_SOShowCost = 2
    const val iRec_PDConfig_RCVShowCount = iRec_PDConfig_SOShowCost + sRec_PDConfig_SOShowCost
    const val sRec_PDConfig_RCVShowCount = 2
    const val iRec_PDConfig_RcvNOF = iRec_PDConfig_RCVShowCount + sRec_PDConfig_RCVShowCount
    const val sRec_PDConfig_RcvNOF = 2
    const val iRec_PDConfig_RcvNOFWarn = iRec_PDConfig_RcvNOF + sRec_PDConfig_RcvNOF
    const val sRec_PDConfig_RcvNOFWarn = 2
    const val iRec_PDConfig_TolerancePct = iRec_PDConfig_RcvNOFWarn + sRec_PDConfig_RcvNOFWarn
    const val sRec_PDConfig_TolerancePct = 4
    const val iRec_PDConfig_RcvOptions = iRec_PDConfig_TolerancePct + sRec_PDConfig_TolerancePct
    const val sRec_PDConfig_RcvOptions = 9
    const val iRec_PDConfig_CreateTrfOut = iRec_PDConfig_RcvOptions + sRec_PDConfig_RcvOptions
    const val sRec_PDConfig_CreateTrfOut = 2
    const val iRec_PDConfig_ReleaseTrfOut = iRec_PDConfig_CreateTrfOut + sRec_PDConfig_CreateTrfOut
    const val sRec_PDConfig_ReleaseTrfOut = 2
    const val iRec_PDConfig_ImmediateScan = iRec_PDConfig_ReleaseTrfOut + sRec_PDConfig_ReleaseTrfOut
    const val sRec_PDConfig_ImmediateScan = 2
    const val iRec_PDConfig_CreateRTV = iRec_PDConfig_ImmediateScan + sRec_PDConfig_ImmediateScan
    const val sRec_PDConfig_CreateRTV = 2
    const val iRec_PDConfig_ReleaseRTV = iRec_PDConfig_CreateRTV + sRec_PDConfig_CreateRTV
    const val sRec_PDConfig_ReleaseRTV = 2
    const val sRec_PDConfig = iRec_PDConfig_ReleaseRTV + sRec_PDConfig_ReleaseRTV
    var REC_PDConfig = ByteArray(sRec_PDConfig)

    /*
    struct Rec_BPrtList { //"BPrtList4201" in Logon.cpp; DbFunction 26; Format 94; SendPrinterList; PmPrt.app (7); Gets from BPrinter.tps
        unsigned char status; //PrintItem_WM_COMMAND_IDOK gets it.
        char RecordId[13];
        char Name[7];
        char Port[2];
        char Baud[6];
        char WordLength[2];
        char Parity[2];
        char StopBits[2];
        char FlowCtrl[2];
        char PortPwrDn[2];
        char IdRequest[9];
        char IdResponse[65];
        char StatusRequest[9];
        char Busy[21];
        char PaperOut[21];
        char CoverOpen[21];
        char LowBattery[21];
        char PowerOffRequest[9];
        char ReadStatus[2];
        char ErrorString[81];
        char ErrorCode[6];
        char NetStatus[2];
    };
    */
    const val iRec_BPrtList_Name = iRecordId + sRecordId
    const val sRec_BPrtList_Name = 7
    const val iRec_BPrtList_Port = iRec_BPrtList_Name + sRec_BPrtList_Name
    const val sRec_BPrtList_Port = 2
    const val iRec_BPrtList_Baud = iRec_BPrtList_Port + sRec_BPrtList_Port
    const val sRec_BPrtList_Baud = 6
    const val iRec_BPrtList_WordLength = iRec_BPrtList_Baud + sRec_BPrtList_Baud
    const val sRec_BPrtList_WordLength = 2
    const val iRec_BPrtList_Parity = iRec_BPrtList_WordLength + sRec_BPrtList_WordLength
    const val sRec_BPrtList_Parity = 2
    const val iRec_BPrtList_StopBits = iRec_BPrtList_Parity + sRec_BPrtList_Parity
    const val sRec_BPrtList_StopBits = 2
    const val iRec_BPrtList_FlowCtrl = iRec_BPrtList_StopBits + sRec_BPrtList_StopBits
    const val sRec_BPrtList_FlowCtrl = 2
    const val iRec_BPrtList_PortPwrDn = iRec_BPrtList_FlowCtrl + sRec_BPrtList_FlowCtrl
    const val sRec_BPrtList_PortPwrDn = 2
    const val iRec_BPrtList_IdRequest = iRec_BPrtList_PortPwrDn + sRec_BPrtList_PortPwrDn
    const val sRec_BPrtList_IdRequest = 9
    const val iRec_BPrtList_IdResponse = iRec_BPrtList_IdRequest + sRec_BPrtList_IdRequest
    const val sRec_BPrtList_IdResponse = 65
    const val iRec_BPrtList_StatusRequest = iRec_BPrtList_IdResponse + sRec_BPrtList_IdResponse
    const val sRec_BPrtList_StatusRequest = 9
    const val iRec_BPrtList_Busy = iRec_BPrtList_StatusRequest + sRec_BPrtList_StatusRequest
    const val sRec_BPrtList_Busy = 21
    const val iRec_BPrtList_PaperOut = iRec_BPrtList_Busy + sRec_BPrtList_Busy
    const val sRec_BPrtList_PaperOut = 21
    const val iRec_BPrtList_CoverOpen = iRec_BPrtList_PaperOut + sRec_BPrtList_PaperOut
    const val sRec_BPrtList_CoverOpen = 21
    const val iRec_BPrtList_LowBattery = iRec_BPrtList_CoverOpen + sRec_BPrtList_CoverOpen
    const val sRec_BPrtList_LowBattery = 21
    const val iRec_BPrtList_PowerOffRequest = iRec_BPrtList_LowBattery + sRec_BPrtList_LowBattery
    const val sRec_BPrtList_PowerOffRequest = 9
    const val iRec_BPrtList_ReadStatus =
        iRec_BPrtList_PowerOffRequest + sRec_BPrtList_PowerOffRequest
    const val sRec_BPrtList_ReadStatus = 2
    const val iRec_BPrtList_ErrorString = iRec_BPrtList_ReadStatus + sRec_BPrtList_ReadStatus
    const val sRec_BPrtList_ErrorString = 81
    const val iRec_BPrtList_ErrorCode = iRec_BPrtList_ErrorString + sRec_BPrtList_ErrorString
    const val sRec_BPrtList_ErrorCode = 6
    const val iRec_BPrtList_NetStatus = iRec_BPrtList_ErrorCode + sRec_BPrtList_ErrorCode
    const val sRec_BPrtList_NetStatus = 2
    const val sRec_BPrtList = iRec_BPrtList_NetStatus + sRec_BPrtList_NetStatus
    var REC_BPrtList = ByteArray(sRec_BPrtList)

    /*
    struct Rec_Security { //LogonSetGlobals fills it in.
       unsigned char status;
       char Inventory[2];
       char Pricing[2];
       char POS[2];
       char Utilities[2];
       char Receiving[2];
       char RTV[2];
       char StoreOrders[2];
       char PhysicalInv[2];
       char PriceVerify[2];
       char PrnItem[2];
       char PrnShelf[2];
       char NewSale[2];
       char ResumeSale[2];
       char DateTime[2];
       char IPAddress[2];
       char HostName[2];
       char UpdProp[2];
       char VendorLookup[2];
       char ShipLookup[2];
       char CreateRcv[2];
       char AddItemToPO[2];
       char DOAReceiving[2];
       char ToleranceOver[2];
       char TransferOut[2];
       char ReleaseTrfOut[2];
       char Printer[2];
       char Carton[2];
       char ChangeQty[2];
       char CreateRTV[2];
       char ReleaseRTV[2];
    };
    */
    const val iRec_Security_Inventory = istatus + sstatus
    const val sRec_Security_Inventory = 2
    const val iRec_Security_Pricing = iRec_Security_Inventory + sRec_Security_Inventory
    const val sRec_Security_Pricing = 2
    const val iRec_Security_POS = iRec_Security_Pricing + sRec_Security_Pricing
    const val sRec_Security_POS = 2
    const val iRec_Security_Utilities = iRec_Security_POS + sRec_Security_POS
    const val sRec_Security_Utilities = 2
    const val iRec_Security_Receiving = iRec_Security_Utilities + sRec_Security_Utilities
    const val sRec_Security_Receiving = 2
    const val iRec_Security_RTV = iRec_Security_Receiving + sRec_Security_Receiving
    const val sRec_Security_RTV = 2
    const val iRec_Security_StoreOrders = iRec_Security_RTV + sRec_Security_RTV
    const val sRec_Security_StoreOrders = 2
    const val iRec_Security_PhysicalInv = iRec_Security_StoreOrders + sRec_Security_StoreOrders
    const val sRec_Security_PhysicalInv = 2
    const val iRec_Security_PriceVerify = iRec_Security_PhysicalInv + sRec_Security_PhysicalInv
    const val sRec_Security_PriceVerify = 2
    const val iRec_Security_PrnItem = iRec_Security_PriceVerify + sRec_Security_PriceVerify
    const val sRec_Security_PrnItem = 2
    const val iRec_Security_PrnShelf = iRec_Security_PrnItem + sRec_Security_PrnItem
    const val sRec_Security_PrnShelf = 2
    const val iRec_Security_NewSale = iRec_Security_PrnShelf + sRec_Security_PrnShelf
    const val sRec_Security_NewSale = 2
    const val iRec_Security_ResumeSale = iRec_Security_NewSale + sRec_Security_NewSale
    const val sRec_Security_ResumeSale = 2
    const val iRec_Security_DateTime = iRec_Security_ResumeSale + sRec_Security_ResumeSale
    const val sRec_Security_DateTime = 2
    const val iRec_Security_IPAddress = iRec_Security_DateTime + sRec_Security_DateTime
    const val sRec_Security_IPAddress = 2
    const val iRec_Security_HostName = iRec_Security_IPAddress + sRec_Security_IPAddress
    const val sRec_Security_HostName = 2
    const val iRec_Security_UpdProp = iRec_Security_HostName + sRec_Security_HostName
    const val sRec_Security_UpdProp = 2
    const val iRec_Security_VendorLookup = iRec_Security_UpdProp + sRec_Security_UpdProp
    const val sRec_Security_VendorLookup = 2
    const val iRec_Security_ShipLookup = iRec_Security_VendorLookup + sRec_Security_VendorLookup
    const val sRec_Security_ShipLookup = 2
    const val iRec_Security_CreateRcv = iRec_Security_ShipLookup + sRec_Security_ShipLookup
    const val sRec_Security_CreateRcv = 2
    const val iRec_Security_AddItemToPO = iRec_Security_CreateRcv + sRec_Security_CreateRcv
    const val sRec_Security_AddItemToPO = 2
    const val iRec_Security_DOAReceiving = iRec_Security_AddItemToPO + sRec_Security_AddItemToPO
    const val sRec_Security_DOAReceiving = 2
    const val iRec_Security_ToleranceOver = iRec_Security_DOAReceiving + sRec_Security_DOAReceiving
    const val sRec_Security_ToleranceOver = 2
    const val iRec_Security_TransferOut = iRec_Security_ToleranceOver + sRec_Security_ToleranceOver
    const val sRec_Security_TransferOut = 2
    const val iRec_Security_ReleaseTrfOut = iRec_Security_TransferOut + sRec_Security_TransferOut
    const val sRec_Security_ReleaseTrfOut = 2
    const val iRec_Security_Printer = iRec_Security_ReleaseTrfOut + sRec_Security_ReleaseTrfOut
    const val sRec_Security_Printer = 2
    const val iRec_Security_Carton = iRec_Security_Printer + sRec_Security_Printer
    const val sRec_Security_Carton = 2
    const val iRec_Security_ChangeQty = iRec_Security_Carton + sRec_Security_Carton
    const val sRec_Security_ChangeQty = 2
    const val iRec_Security_CreateRTV = iRec_Security_ChangeQty + sRec_Security_ChangeQty
    const val sRec_Security_CreateRTV = 2
    const val iRec_Security_ReleaseRTV = iRec_Security_CreateRTV + sRec_Security_CreateRTV
    const val sRec_Security_ReleaseRTV = 2
    const val sRec_Security = iRec_Security_ReleaseRTV + sRec_Security_ReleaseRTV
    var REC_Security = ByteArray(sRec_Security)

    /*
    struct Rec_InvMst { // RecordId = "InvMst210" in Pricing.cpp, Print.cpp, Pos.cpp; DbFunction 4; Format 79; GetItemDetail; PmWinDbm.app (11).
        unsigned char status;
        char RecordId[13];
        char SKU[29];
        char FunctionID[3];      // 'P' in Pos.cpp;
        char Description1[16];
        char Description2[21];
        char Quantity[10];
        char Price[10];
        char Vendor[16];
        char Disposition[16];
        char ReadStatus[2];
        char ErrorString[81];
        char ErrorCode[6];
        char NetStatus[2];
        char NewReceived[10];
        char Received[10];
        char DOANewReceived[10];
        char DOAReceived[10];
        char NotOnPO[2];
        char OnHand[7];
        char OrdAt[7];
        char OkToOrder[2];
    };
    */
    const val iRec_InvMst_SKU = iRecordId + sRecordId
    const val sRec_InvMst_SKU = 29
    const val iRec_InvMst_FunctionID = iRec_InvMst_SKU + sRec_InvMst_SKU
    const val sRec_InvMst_FunctionID = 3
    const val iRec_InvMst_Description1 = iRec_InvMst_FunctionID + sRec_InvMst_FunctionID
    const val sRec_InvMst_Description1 = 16
    const val iRec_InvMst_Description2 = iRec_InvMst_Description1 + sRec_InvMst_Description1
    const val sRec_InvMst_Description2 = 21
    const val iRec_InvMst_Quantity = iRec_InvMst_Description2 + sRec_InvMst_Description2
    const val sRec_InvMst_Quantity = 10
    const val iRec_InvMst_Price = iRec_InvMst_Quantity + sRec_InvMst_Quantity
    const val sRec_InvMst_Price = 10
    const val iRec_InvMst_Vendor = iRec_InvMst_Price + sRec_InvMst_Price
    const val sRec_InvMst_Vendor = 16
    const val iRec_InvMst_Disposition = iRec_InvMst_Vendor + sRec_InvMst_Vendor
    const val sRec_InvMst_Disposition = 16
    const val iRec_InvMst_ReadStatus = iRec_InvMst_Disposition + sRec_InvMst_Disposition
    const val sRec_InvMst_ReadStatus = 2
    const val iRec_InvMst_ErrorString = iRec_InvMst_ReadStatus + sRec_InvMst_ReadStatus
    const val sRec_InvMst_ErrorString = 81
    const val iRec_InvMst_ErrorCode = iRec_InvMst_ErrorString + sRec_InvMst_ErrorString
    const val sRec_InvMst_ErrorCode = 6
    const val iRec_InvMst_NetStatus = iRec_InvMst_ErrorCode + sRec_InvMst_ErrorCode
    const val sRec_InvMst_NetStatus = 2
    const val iRec_InvMst_NewReceived = iRec_InvMst_NetStatus + sRec_InvMst_NetStatus
    const val sRec_InvMst_NewReceived = 10
    const val iRec_InvMst_Received = iRec_InvMst_NewReceived + sRec_InvMst_NewReceived
    const val sRec_InvMst_Received = 10
    const val iRec_InvMst_DOANewReceived = iRec_InvMst_Received + sRec_InvMst_Received
    const val sRec_InvMst_DOANewReceived = 10
    const val iRec_InvMst_DOAReceived = iRec_InvMst_DOANewReceived + sRec_InvMst_DOANewReceived
    const val sRec_InvMst_DOAReceived = 10
    const val iRec_InvMst_NotOnPO = iRec_InvMst_DOAReceived + sRec_InvMst_DOAReceived
    const val sRec_InvMst_NotOnPO = 2
    const val iRec_InvMst_OnHand = iRec_InvMst_NotOnPO + sRec_InvMst_NotOnPO
    const val sRec_InvMst_OnHand = 7
    const val iRec_InvMst_OrdAt = iRec_InvMst_OnHand + sRec_InvMst_OnHand
    const val sRec_InvMst_OrdAt = 7
    const val iRec_InvMst_OkToOrder = iRec_InvMst_OrdAt + sRec_InvMst_OrdAt
    const val sRec_InvMst_OkToOrder = 2
    const val sRec_InvMst = iRec_InvMst_OkToOrder + sRec_InvMst_OkToOrder
    var REC_InvMst = ByteArray(sRec_InvMst)

    /*
    struct Rec_PriceVar { // RecordId = "PrcVar210" in Pricing.cpp; DbFunction 8; Format 82; PDTPriceVariance; PmWinDbm.app (10); Adds to PdPrcVar.tps
       unsigned char status;
       char RecordId[13];
       char SKU[29];
       char FilePrice[15];
       char FloorPrice[15];
       char ReadStatus[2];
       char ErrorString[81];
       char ErrorCode[6];
       char NetStatus[2];
       char InvMst[351];
    };
    */
    const val iRec_PriceVar_SKU = iRecordId + sRecordId
    const val sRec_PriceVar_SKU = 29
    const val iRec_PriceVar_FilePrice = iRec_PriceVar_SKU + sRec_PriceVar_SKU
    const val sRec_PriceVar_FilePrice = 15
    const val iRec_PriceVar_FloorPrice = iRec_PriceVar_FilePrice + sRec_PriceVar_FilePrice
    const val sRec_PriceVar_FloorPrice = 15
    const val iRec_PriceVar_ReadStatus = iRec_PriceVar_FloorPrice + sRec_PriceVar_FloorPrice
    const val sRec_PriceVar_ReadStatus = 2
    const val iRec_PriceVar_ErrorString = iRec_PriceVar_ReadStatus + sRec_PriceVar_ReadStatus
    const val sRec_PriceVar_ErrorString = 81
    const val iRec_PriceVar_ErrorCode = iRec_PriceVar_ErrorString + sRec_PriceVar_ErrorString
    const val sRec_PriceVar_ErrorCode = 6
    const val iRec_PriceVar_NetStatus = iRec_PriceVar_ErrorCode + sRec_PriceVar_ErrorCode
    const val sRec_PriceVar_NetStatus = 2
    const val iRec_PriceVar_InvMst = iRec_PriceVar_NetStatus + sRec_PriceVar_NetStatus
    const val sRec_PriceVar_InvMst = 351
    const val sRec_PriceVar = iRec_PriceVar_InvMst + sRec_PriceVar_InvMst
    var REC_PriceVar = ByteArray(sRec_PriceVar)

    /*
    struct Rec_Tickets {  // RecordId = "Tickets210" in Print.cpp; DbFunction 11; Format 84; Tickets; PmWinDbm.app (8).
        unsigned char status;
        char RecordId[13];
        char SKU[29];
        char Quantity[10];
        char Price[10];
        char PrintType[2];
        char ReadStatus[2];
        char ErrorString[81];
        char ErrorCode[6];
        char NetStatus[2];
        char PrinterId[7];
        char InvMst[351];
    };
    */
    const val iRec_Tickets_SKU = iRecordId + sRecordId
    const val sRec_Tickets_SKU = 29
    const val iRec_Tickets_Quantity = iRec_Tickets_SKU + sRec_Tickets_SKU
    const val sRec_Tickets_Quantity = 10
    const val iRec_Tickets_Price = iRec_Tickets_Quantity + sRec_Tickets_Quantity
    const val sRec_Tickets_Price = 10
    const val iRec_Tickets_PrintType = iRec_Tickets_Price + sRec_Tickets_Price
    const val sRec_Tickets_PrintType = 2
    const val iRec_Tickets_ReadStatus = iRec_Tickets_PrintType + sRec_Tickets_PrintType
    const val sRec_Tickets_ReadStatus = 2
    const val iRec_Tickets_ErrorString = iRec_Tickets_ReadStatus + sRec_Tickets_ReadStatus
    const val sRec_Tickets_ErrorString = 81
    const val iRec_Tickets_ErrorCode = iRec_Tickets_ErrorString + sRec_Tickets_ErrorString
    const val sRec_Tickets_ErrorCode = 6
    const val iRec_Tickets_NetStatus = iRec_Tickets_ErrorCode + sRec_Tickets_ErrorCode
    const val sRec_Tickets_NetStatus = 2
    const val iRec_Tickets_PrinterId = iRec_Tickets_NetStatus + sRec_Tickets_NetStatus
    const val sRec_Tickets_PrinterId = 7
    const val iRec_Tickets_InvMst = iRec_Tickets_PrinterId + sRec_Tickets_PrinterId
    const val sRec_Tickets_InvMst = 351
    const val sRec_Tickets = iRec_Tickets_InvMst + sRec_Tickets_InvMst
    var REC_Tickets = ByteArray(sRec_Tickets)

    /*
    struct Rec_CpnBrw {  //KIA 09/21/13 BSH 12. PRISM Coupon Modification. Added // RecordId = "CpnBrw" in Coupon.cpp; DbFunction 35; Format 110; CouponsLookup; PmWinDbm.app (42).
        unsigned char status;
        char RecordId[13];
        char Action[2];
        char SKU[10];
        char CouponMasterId[11];
        char Description[31];
        char CouponType[11];
        char AutoApply[2];
        char StartDate[11];
        char EndDate[11];
        char StartTime[9];
        char EndTime[9];
        char ReadStatus[2];      // Read Status 1=OK, 0=Failed         Server
        char ErrorString[81];    // Error String                       Server
        char ErrorCode[6];       // Error Code                         Server
        char NetStatus[2];       // Network Status 1=OK, 0=Failed      Server
    };
    */
    const val iRec_CpnBrw_Action = iRecordId + sRecordId
    const val sRec_CpnBrw_Action = 2
    const val iRec_CpnBrw_SKU = iRec_CpnBrw_Action + sRec_CpnBrw_Action
    const val sRec_CpnBrw_SKU = 10
    const val iRec_CpnBrw_CouponMasterId = iRec_CpnBrw_SKU + sRec_CpnBrw_SKU
    const val sRec_CpnBrw_CouponMasterId = 11
    const val iRec_CpnBrw_Description = iRec_CpnBrw_CouponMasterId + sRec_CpnBrw_CouponMasterId
    const val sRec_CpnBrw_Description = 31
    const val iRec_CpnBrw_CouponType = iRec_CpnBrw_Description + sRec_CpnBrw_Description
    const val sRec_CpnBrw_CouponType = 11
    const val iRec_CpnBrw_AutoApply = iRec_CpnBrw_CouponType + sRec_CpnBrw_CouponType
    const val sRec_CpnBrw_AutoApply = 2
    const val iRec_CpnBrw_StartDate = iRec_CpnBrw_AutoApply + sRec_CpnBrw_AutoApply
    const val sRec_CpnBrw_StartDate = 11
    const val iRec_CpnBrw_EndDate = iRec_CpnBrw_StartDate + sRec_CpnBrw_StartDate
    const val sRec_CpnBrw_EndDate = 11
    const val iRec_CpnBrw_StartTime = iRec_CpnBrw_EndDate + sRec_CpnBrw_EndDate
    const val sRec_CpnBrw_StartTime = 9
    const val iRec_CpnBrw_EndTime = iRec_CpnBrw_StartTime + sRec_CpnBrw_StartTime
    const val sRec_CpnBrw_EndTime = 9
    const val iRec_CpnBrw_ReadStatus = iRec_CpnBrw_EndTime + sRec_CpnBrw_EndTime
    const val sRec_CpnBrw_ReadStatus = 2
    const val iRec_CpnBrw_ErrorString = iRec_CpnBrw_ReadStatus + sRec_CpnBrw_ReadStatus
    const val sRec_CpnBrw_ErrorString = 81
    const val iRec_CpnBrw_ErrorCode = iRec_CpnBrw_ErrorString + sRec_CpnBrw_ErrorString
    const val sRec_CpnBrw_ErrorCode = 6
    const val iRec_CpnBrw_NetStatus = iRec_CpnBrw_ErrorCode + sRec_CpnBrw_ErrorCode
    const val sRec_CpnBrw_NetStatus = 2
    const val sRec_CpnBrw = iRec_CpnBrw_NetStatus + sRec_CpnBrw_NetStatus
    var REC_CpnBrw = ByteArray(sRec_CpnBrw)

    /*
    struct Rec_CpnSku {  //KIA 10/03/13 BSH 12. PRISM Coupon Modification. Added // RecordId = "CpnSku" in Pricing.cpp; DbFunction 36; Format 111; CouponSkuPref; PmWinDbm.app (43).
        unsigned char status;
        char RecordId[13];
        char SKU[10];
        char CouponMasterId[11];
        char Description[31];
        char CouponType[11];
        char Price[10];
        char ActiveDate[11];
        char StartDate[11];
        char EndDate[11];
        char StartTime[9];
        char EndTime[9];
        char ReadStatus[2];      // Read Status 1=OK, 0=Failed         Server
        char ErrorString[81];    // Error String                       Server
        char ErrorCode[6];       // Error Code                         Server
        char NetStatus[2];       // Network Status 1=OK, 0=Failed      Server
    };
    */
    const val iRec_CpnSku_SKU = iRecordId + sRecordId
    const val sRec_CpnSku_SKU = 10
    const val iRec_CpnSku_CouponMasterId = iRec_CpnSku_SKU + sRec_CpnSku_SKU
    const val sRec_CpnSku_CouponMasterId = 11
    const val iRec_CpnSku_Description = iRec_CpnSku_CouponMasterId + sRec_CpnSku_CouponMasterId
    const val sRec_CpnSku_Description = 31
    const val iRec_CpnSku_CouponType = iRec_CpnSku_Description + sRec_CpnSku_Description
    const val sRec_CpnSku_CouponType = 11
    const val iRec_CpnSku_Price = iRec_CpnSku_CouponType + sRec_CpnSku_CouponType
    const val sRec_CpnSku_Price = 10
    const val iRec_CpnSku_ActiveDate = iRec_CpnSku_Price + sRec_CpnSku_Price
    const val sRec_CpnSku_ActiveDate = 11
    const val iRec_CpnSku_StartDate = iRec_CpnSku_ActiveDate + sRec_CpnSku_ActiveDate
    const val sRec_CpnSku_StartDate = 11
    const val iRec_CpnSku_EndDate = iRec_CpnSku_StartDate + sRec_CpnSku_StartDate
    const val sRec_CpnSku_EndDate = 11
    const val iRec_CpnSku_StartTime = iRec_CpnSku_EndDate + sRec_CpnSku_EndDate
    const val sRec_CpnSku_StartTime = 9
    const val iRec_CpnSku_EndTime = iRec_CpnSku_StartTime + sRec_CpnSku_StartTime
    const val sRec_CpnSku_EndTime = 9
    const val iRec_CpnSku_ReadStatus = iRec_CpnSku_EndTime + sRec_CpnSku_EndTime
    const val sRec_CpnSku_ReadStatus = 2
    const val iRec_CpnSku_ErrorString = iRec_CpnSku_ReadStatus + sRec_CpnSku_ReadStatus
    const val sRec_CpnSku_ErrorString = 81
    const val iRec_CpnSku_ErrorCode = iRec_CpnSku_ErrorString + sRec_CpnSku_ErrorString
    const val sRec_CpnSku_ErrorCode = 6
    const val iRec_CpnSku_NetStatus = iRec_CpnSku_ErrorCode + sRec_CpnSku_ErrorCode
    const val sRec_CpnSku_NetStatus = 2
    const val sRec_CpnSku = iRec_CpnSku_NetStatus + sRec_CpnSku_NetStatus
    var REC_CpnSku = ByteArray(sRec_CpnSku)

    /*
    struct Rec_CpnDtl {  //KIA 10/03/13 BSH 12. PRISM Coupon Modification. Added // RecordId = "CpnDtl" in Pricing.cpp; DbFunction 37; Format 112; CouponDetail; PmWinDbm.app (44).
        unsigned char status;
        char RecordId[13];
        char CouponMasterId[11];
        char AutoApply[2];
        char BuyQty[4];
        char GetQty[4];
        char DiscountType[2];
        char CouponValue[10];
        char MaxCouponValue[10];
        char MaxCouponsPerTx[4];
        char ExcludeEmployee[2];
        char ExcludePromo[2];
        char MinPurchReq[10];
        char ReadStatus[2];      // Read Status 1=OK, 0=Failed         Server
        char ErrorString[81];    // Error String                       Server
        char ErrorCode[6];       // Error Code                         Server
        char NetStatus[2];       // Network Status 1=OK, 0=Failed      Server
        };
    */
    const val iRec_CpnDtl_CouponMasterId = iRecordId + sRecordId
    const val sRec_CpnDtl_CouponMasterId = 11
    const val iRec_CpnDtl_AutoApply = iRec_CpnDtl_CouponMasterId + sRec_CpnDtl_CouponMasterId
    const val sRec_CpnDtl_AutoApply = 2
    const val iRec_CpnDtl_BuyQty = iRec_CpnDtl_AutoApply + sRec_CpnDtl_AutoApply
    const val sRec_CpnDtl_BuyQty = 4
    const val iRec_CpnDtl_GetQty = iRec_CpnDtl_BuyQty + sRec_CpnDtl_BuyQty
    const val sRec_CpnDtl_GetQty = 4
    const val iRec_CpnDtl_DiscountType = iRec_CpnDtl_GetQty + sRec_CpnDtl_GetQty
    const val sRec_CpnDtl_DiscountType = 2
    const val iRec_CpnDtl_CouponValue = iRec_CpnDtl_DiscountType + sRec_CpnDtl_DiscountType
    const val sRec_CpnDtl_CouponValue = 10
    const val iRec_CpnDtl_MaxCouponValue = iRec_CpnDtl_CouponValue + sRec_CpnDtl_CouponValue
    const val sRec_CpnDtl_MaxCouponValue = 10
    const val iRec_CpnDtl_MaxCouponsPerTx = iRec_CpnDtl_MaxCouponValue + sRec_CpnDtl_MaxCouponValue
    const val sRec_CpnDtl_MaxCouponsPerTx = 4
    const val iRec_CpnDtl_ExcludeEmployee =
        iRec_CpnDtl_MaxCouponsPerTx + sRec_CpnDtl_MaxCouponsPerTx
    const val sRec_CpnDtl_ExcludeEmployee = 2
    const val iRec_CpnDtl_ExcludePromo = iRec_CpnDtl_ExcludeEmployee + sRec_CpnDtl_ExcludeEmployee
    const val sRec_CpnDtl_ExcludePromo = 2
    const val iRec_CpnDtl_MinPurchReq = iRec_CpnDtl_ExcludePromo + sRec_CpnDtl_ExcludePromo
    const val sRec_CpnDtl_MinPurchReq = 10
    const val iRec_CpnDtl_ReadStatus = iRec_CpnDtl_MinPurchReq + sRec_CpnDtl_MinPurchReq
    const val sRec_CpnDtl_ReadStatus = 2
    const val iRec_CpnDtl_ErrorString = iRec_CpnDtl_ReadStatus + sRec_CpnDtl_ReadStatus
    const val sRec_CpnDtl_ErrorString = 81
    const val iRec_CpnDtl_ErrorCode = iRec_CpnDtl_ErrorString + sRec_CpnDtl_ErrorString
    const val sRec_CpnDtl_ErrorCode = 6
    const val iRec_CpnDtl_NetStatus = iRec_CpnDtl_ErrorCode + sRec_CpnDtl_ErrorCode
    const val sRec_CpnDtl_NetStatus = 2
    const val sRec_CpnDtl = iRec_CpnDtl_NetStatus + sRec_CpnDtl_NetStatus
    var REC_CpnDtl = ByteArray(sRec_CpnDtl)

    /*
    struct Rec_Receiver { // RecordId = "VndLkup4307" in Lookup.cpp; DbFunction 12; Format 96; VendorLookup; PmWinDbm.app (12); Reads VenMst.tps
        unsigned char status;      // RecordId = "Receiver4307" in Receiving.cpp; DbFunction 2; Format 96; RcvLookup; PmWinDbm.app (5); Adds to ShpHdr.tps
        char RecordId[13];         // RecordId = "CrtRcv4307" in Receiving.cpp; DbFunction 13; Format 96; CreateReceiver; PmWinDbm.app (5); Calls GetOpenRcvHdr (RcvHdr.tps)in Recving.app;.Adds to ShpHdr.tps
        char CounterID[12];        // RecordId = "RTVDoc4307" not used; Base cpp: Scr_VendorBrowse,Scr_Shipment,Scr_RTVDocument g_RcvFlag[0] = 'W'; DbFunction 31; Format 96; BEGIN;END !RTVLookup(Env,PRM:FormatID,PRM:Message)
        char ShipNumber[9];        // RecordId = "DelRcv4307" not used; Base cpp: Scr_ReceiverTotals,Scr_Shipment,Scr_RecvItem; DbFunction 16; Format 96; DeleteReceiver; PmWinDbm.app (5); Deletes from ShpHdr.tps and RcvHdr.tps (?)
        char OrigLocation[10];     // RecordId = "RcvTot4307" not used; Base cpp: Scr_ReceiverTotals; DbFunction 15; Format 96; RcvHdr; PmWinDbm.app (5); Works with ShpHdr.tps and RcvHdr.tps (?)
        char OrigName[15];         // RecordId = "RTVRel4307" in Rtv.cpp; DbFunction 33; Format 96; RTVRelease; PmWinDbm.app (19); Calls ShpRelease, PrnShipRv(SHH:TrnNo,TRUE)
        char OrigShipDate[11];
        char ReadStatus[2];
        char ErrorString[81];
        char ErrorCode[6];
        char NetStatus[2];
        unsigned char RcvType;
        char LookupStatus[2];
        char LookupType[2];
        char Reference[16];
        char InvoiceAmt[12];     // Keeps SHH:Entries. See Receiving.cpp
        char FreightAmt[12];     // Keeps SHH:Cost
        char MiscAmt[12];
    };
    */
    const val iRec_Receiver_CounterID = iRecordId + sRecordId
    const val sRec_Receiver_CounterID = 12
    const val iRec_Receiver_ShipNumber = iRec_Receiver_CounterID + sRec_Receiver_CounterID
    const val sRec_Receiver_ShipNumber = 9
    const val iRec_Receiver_OrigLocation = iRec_Receiver_ShipNumber + sRec_Receiver_ShipNumber
    const val sRec_Receiver_OrigLocation = 10
    const val iRec_Receiver_OrigName = iRec_Receiver_OrigLocation + sRec_Receiver_OrigLocation
    const val sRec_Receiver_OrigName = 15
    const val iRec_Receiver_OrigShipDate = iRec_Receiver_OrigName + sRec_Receiver_OrigName
    const val sRec_Receiver_OrigShipDate = 11
    const val iRec_Receiver_ReadStatus = iRec_Receiver_OrigShipDate + sRec_Receiver_OrigShipDate
    const val sRec_Receiver_ReadStatus = 2
    const val iRec_Receiver_ErrorString = iRec_Receiver_ReadStatus + sRec_Receiver_ReadStatus
    const val sRec_Receiver_ErrorString = 81
    const val iRec_Receiver_ErrorCode = iRec_Receiver_ErrorString + sRec_Receiver_ErrorString
    const val sRec_Receiver_ErrorCode = 6
    const val iRec_Receiver_NetStatus = iRec_Receiver_ErrorCode + sRec_Receiver_ErrorCode
    const val sRec_Receiver_NetStatus = 2
    const val iRec_Receiver_RcvType = iRec_Receiver_NetStatus + sRec_Receiver_NetStatus
    const val sRec_Receiver_RcvType = 1
    const val iRec_Receiver_LookupStatus = iRec_Receiver_RcvType + sRec_Receiver_RcvType
    const val sRec_Receiver_LookupStatus = 2
    const val iRec_Receiver_LookupType = iRec_Receiver_LookupStatus + sRec_Receiver_LookupStatus
    const val sRec_Receiver_LookupType = 2
    const val iRec_Receiver_Reference = iRec_Receiver_LookupType + sRec_Receiver_LookupType
    const val sRec_Receiver_Reference = 16
    const val iRec_Receiver_InvoiceAmt = iRec_Receiver_Reference + sRec_Receiver_Reference
    const val sRec_Receiver_InvoiceAmt = 12
    const val iRec_Receiver_FreightAmt = iRec_Receiver_InvoiceAmt + sRec_Receiver_InvoiceAmt
    const val sRec_Receiver_FreightAmt = 12
    const val iRec_Receiver_MiscAmt = iRec_Receiver_FreightAmt + sRec_Receiver_FreightAmt
    const val sRec_Receiver_MiscAmt = 12
    const val sRec_Receiver = iRec_Receiver_MiscAmt + sRec_Receiver_MiscAmt
    var REC_Receiver = ByteArray(sRec_Receiver)

    /*
    struct Rec_CloseRcv { // RecordId = "CloseRcv203" in Receiving.cpp; DbFunction 14; Format 52; CloseRcv; PmWinDbm.app (5); Calls CloseReceiver in Recving.app
        unsigned char status;
        char RecordId[13];
        char ReadStatus[2];
        char ErrorString[81];
        char ErrorCode[6];
        char NetStatus[2];
        char CloseFlag[2];
    };
    */
    const val iRec_CloseRcv_ReadStatus = iRecordId + sRecordId
    const val sRec_CloseRcv_ReadStatus = 2
    const val iRec_CloseRcv_ErrorString = iRec_CloseRcv_ReadStatus + sRec_CloseRcv_ReadStatus
    const val sRec_CloseRcv_ErrorString = 81
    const val iRec_CloseRcv_ErrorCode = iRec_CloseRcv_ErrorString + sRec_CloseRcv_ErrorString
    const val sRec_CloseRcv_ErrorCode = 6
    const val iRec_CloseRcv_NetStatus = iRec_CloseRcv_ErrorCode + sRec_CloseRcv_ErrorCode
    const val sRec_CloseRcv_NetStatus = 2
    const val iRec_CloseRcv_CloseFlag = iRec_CloseRcv_NetStatus + sRec_CloseRcv_NetStatus
    const val sRec_CloseRcv_CloseFlag = 1
    const val sRec_CloseRcv = iRec_CloseRcv_CloseFlag + sRec_CloseRcv_CloseFlag
    var REC_CloseRcv = ByteArray(sRec_CloseRcv)

    /*
    struct Rec_RecvDtl {  // RecordId = "RecvDtl210" in Receiving.cpp; DbFunction 3; Format 78; ReceiverDtl; PmWinDbm.app (5); ShpDtl.tps, PdConfig.tps
        unsigned char status;
        char RecordId[13];
        char SKU[29];
        char Quantity[10];
        char Price[10];
        char User1Data[16];
        char User2Data[16];
        char ReadStatus[2];
        char ErrorString[81];
        char ErrorCode[6];
        char NetStatus[2];
        char InvMst[351];
    };
    */
    const val iRec_RecvDtl_SKU = iRecordId + sRecordId
    const val sRec_RecvDtl_SKU = 29
    const val iRec_RecvDtl_Quantity = iRec_RecvDtl_SKU + sRec_RecvDtl_SKU
    const val sRec_RecvDtl_Quantity = 10
    const val iRec_RecvDtl_Price = iRec_RecvDtl_Quantity + sRec_RecvDtl_Quantity
    const val sRec_RecvDtl_Price = 10
    const val iRec_RecvDtl_User1Data = iRec_RecvDtl_Price + sRec_RecvDtl_Price
    const val sRec_RecvDtl_User1Data = 16
    const val iRec_RecvDtl_User2Data = iRec_RecvDtl_User1Data + sRec_RecvDtl_User1Data
    const val sRec_RecvDtl_User2Data = 16
    const val iRec_RecvDtl_ReadStatus = iRec_RecvDtl_User2Data + sRec_RecvDtl_User2Data
    const val sRec_RecvDtl_ReadStatus = 2
    const val iRec_RecvDtl_ErrorString = iRec_RecvDtl_ReadStatus + sRec_RecvDtl_ReadStatus
    const val sRec_RecvDtl_ErrorString = 81
    const val iRec_RecvDtl_ErrorCode = iRec_RecvDtl_ErrorString + sRec_RecvDtl_ErrorString
    const val sRec_RecvDtl_ErrorCode = 6
    const val iRec_RecvDtl_NetStatus = iRec_RecvDtl_ErrorCode + sRec_RecvDtl_ErrorCode
    const val sRec_RecvDtl_NetStatus = 2
    const val iRec_RecvDtl_InvMst = iRec_RecvDtl_NetStatus + sRec_RecvDtl_NetStatus
    const val sRec_RecvDtl_InvMst = 351
    const val sRec_RecvDtl = iRec_RecvDtl_InvMst + sRec_RecvDtl_InvMst
    var REC_RecvDtl = ByteArray(sRec_RecvDtl)

    /*
    struct Rec_TrfOut { // RecordId = "TrfOut4307" in Trfout.cpp,Rtv.cpp; DbFunction 23; Format 98; ProcessTransferOut; PmWinDbm.app (14); StrMst.tps, ShpHdr.tps
        unsigned char status;    // RecordId = "TrfDtl4307" in Trfout.cpp,Rtv.cpp; DbFunction 24; Format 98; TrfOutDetail; PmWinDbm.app (14); ShpDtl.tps, ShpHdr.tps
        char RecordId[13];       // RecordId = "RelTrf4307" in Trfout.cpp; DbFunction 25; Format 98; ReleaseTrfOut; PmWinDbm.app (14); Calls ShpRelease, PrnShipTx(SHH:TrnNo,TRUE)
        char TranNo[9];
        char ToStore[10];
        char TrfType[2];
        char EmpID[12];
        char SKU[29];
        char Quantity[10];
        char Date[11];
        char Reference[16];
        char Entries[10];
        char ReadStatus[2];
        char ErrorString[81];
        char ErrorCode[6];
        char NetStatus[2];
        char InvMst[351];
    };
    */
    const val iRec_TrfOut_TranNo = iRecordId + sRecordId
    const val sRec_TrfOut_TranNo = 9
    const val iRec_TrfOut_ToStore = iRec_TrfOut_TranNo + sRec_TrfOut_TranNo
    const val sRec_TrfOut_ToStore = 10
    const val iRec_TrfOut_TrfType = iRec_TrfOut_ToStore + sRec_TrfOut_ToStore
    const val sRec_TrfOut_TrfType = 2
    const val iRec_TrfOut_EmpID = iRec_TrfOut_TrfType + sRec_TrfOut_TrfType
    const val sRec_TrfOut_EmpID = 12
    const val iRec_TrfOut_SKU = iRec_TrfOut_EmpID + sRec_TrfOut_EmpID
    const val sRec_TrfOut_SKU = 29
    const val iRec_TrfOut_Quantity = iRec_TrfOut_SKU + sRec_TrfOut_SKU
    const val sRec_TrfOut_Quantity = 10
    const val iRec_TrfOut_Date = iRec_TrfOut_Quantity + sRec_TrfOut_Quantity
    const val sRec_TrfOut_Date = 11
    const val iRec_TrfOut_Reference = iRec_TrfOut_Date + sRec_TrfOut_Date
    const val sRec_TrfOut_Reference = 16
    const val iRec_TrfOut_Entries = iRec_TrfOut_Reference + sRec_TrfOut_Reference
    const val sRec_TrfOut_Entries = 10
    const val iRec_TrfOut_ReadStatus = iRec_TrfOut_Entries + sRec_TrfOut_Entries
    const val sRec_TrfOut_ReadStatus = 2
    const val iRec_TrfOut_ErrorString = iRec_TrfOut_ReadStatus + sRec_TrfOut_ReadStatus
    const val sRec_TrfOut_ErrorString = 81
    const val iRec_TrfOut_ErrorCode = iRec_TrfOut_ErrorString + sRec_TrfOut_ErrorString
    const val sRec_TrfOut_ErrorCode = 6
    const val iRec_TrfOut_NetStatus = iRec_TrfOut_ErrorCode + sRec_TrfOut_ErrorCode
    const val sRec_TrfOut_NetStatus = 2
    const val iRec_TrfOut_InvMst = iRec_TrfOut_NetStatus + sRec_TrfOut_NetStatus
    const val sRec_TrfOut_InvMst = 351
    const val sRec_TrfOut = iRec_TrfOut_InvMst + sRec_TrfOut_InvMst
    var REC_TrfOut = ByteArray(sRec_TrfOut)

    /*
    struct Rec_StockCnt {
        unsigned char status;
        char RecordId[13];       // RecordId = "StockCnt4426" in Stock.cpp; DbFunction 6(35); Format 107; PhyInvHdr; PmWinDbm.app (3); Updates PhiShtHdr.tps
        char Action[2];
        char UserId[12];
        char CountId[13];
        char AreaId[13];
        char CountNumber[2];
        char CountStatus[3];
        char ReadStatus[2];
        char ErrorString[81];
        char ErrorCode[6];
        char NetStatus[2];
    };
    */
    const val iRec_StockCnt_Action = iRecordId + sRecordId
    const val sRec_StockCnt_Action = 2
    const val iRec_StockCnt_UserId = iRec_StockCnt_Action + sRec_StockCnt_Action
    const val sRec_StockCnt_UserId = 12
    const val iRec_StockCnt_CountId = iRec_StockCnt_UserId + sRec_StockCnt_UserId
    const val sRec_StockCnt_CountId = 13
    const val iRec_StockCnt_AreaId = iRec_StockCnt_CountId + sRec_StockCnt_CountId
    const val sRec_StockCnt_AreaId = 13
    const val iRec_StockCnt_CountNumber = iRec_StockCnt_AreaId + sRec_StockCnt_AreaId
    const val sRec_StockCnt_CountNumber = 2
    const val iRec_StockCnt_CountStatus = iRec_StockCnt_CountNumber + sRec_StockCnt_CountNumber
    const val sRec_StockCnt_CountStatus = 3
    const val iRec_StockCnt_ReadStatus = iRec_StockCnt_CountStatus + sRec_StockCnt_CountStatus
    const val sRec_StockCnt_ReadStatus = 2
    const val iRec_StockCnt_ErrorString = iRec_StockCnt_ReadStatus + sRec_StockCnt_ReadStatus
    const val sRec_StockCnt_ErrorString = 81
    const val iRec_StockCnt_ErrorCode = iRec_StockCnt_ErrorString + sRec_StockCnt_ErrorString
    const val sRec_StockCnt_ErrorCode = 6
    const val iRec_StockCnt_NetStatus = iRec_StockCnt_ErrorCode + sRec_StockCnt_ErrorCode
    const val sRec_StockCnt_NetStatus = 2
    const val sRec_StockCnt = iRec_StockCnt_NetStatus + sRec_StockCnt_NetStatus
    var REC_StockCnt = ByteArray(sRec_StockCnt)

    /*
    struct Rec_StockDtl { // RecordId = "StockDtl4426" in Stock.cpp; DbFunction 7(36); Format 108; PhyInvDtl; PmWinDbm.app (3); Updates PhiShtDtl.tps and PInvBal.tps
       unsigned char status;
       char RecordId[13];
       char SKU[29];
       char Description1[16];
       char Description2[21];
       char Quantity[10];
       char Price[10];
       char ReadStatus[2];
       char ErrorString[81];
       char ErrorCode[6];
       char NetStatus[2];
       char Count[10];
    };
    */
    const val iRec_StockDtl_SKU = iRecordId + sRecordId
    const val sRec_StockDtl_SKU = 29
    const val iRec_StockDtl_Description1 = iRec_StockDtl_SKU + sRec_StockDtl_SKU
    const val sRec_StockDtl_Description1 = 16
    const val iRec_StockDtl_Description2 = iRec_StockDtl_Description1 + sRec_StockDtl_Description1
    const val sRec_StockDtl_Description2 = 21
    const val iRec_StockDtl_Quantity = iRec_StockDtl_Description2 + sRec_StockDtl_Description2
    const val sRec_StockDtl_Quantity = 10
    const val iRec_StockDtl_Price = iRec_StockDtl_Quantity + sRec_StockDtl_Quantity
    const val sRec_StockDtl_Price = 10
    const val iRec_StockDtl_ReadStatus = iRec_StockDtl_Price + sRec_StockDtl_Price
    const val sRec_StockDtl_ReadStatus = 2
    const val iRec_StockDtl_ErrorString = iRec_StockDtl_ReadStatus + sRec_StockDtl_ReadStatus
    const val sRec_StockDtl_ErrorString = 81
    const val iRec_StockDtl_ErrorCode = iRec_StockDtl_ErrorString + sRec_StockDtl_ErrorString
    const val sRec_StockDtl_ErrorCode = 6
    const val iRec_StockDtl_NetStatus = iRec_StockDtl_ErrorCode + sRec_StockDtl_ErrorCode
    const val sRec_StockDtl_NetStatus = 2
    const val iRec_StockDtl_Count = iRec_StockDtl_NetStatus + sRec_StockDtl_NetStatus
    const val sRec_StockDtl_Count = 10
    const val sRec_StockDtl = iRec_StockDtl_Count + sRec_StockDtl_Count
    var REC_StockDtl = ByteArray(sRec_StockDtl)

    /*
    struct Rec_ClrHdr { // RecordId = "ClrHdr210" in Clr.cpp; DbFunction 28; Format 91; CartonHdr; PmWinDbm.app (15); Goes to CHD:CartonID to get from ClrHdr.tps
        unsigned char status;    // Description                        Filled by
        char RecordId[13];       // Record Identifier                  Client
        char TotalType[2];       // 1=by BOL, 2=by PO, 3=by Carton     Client REC_ClrHdr.TotalType[0] = (char)(iClrScanCartonTotalType + 0x31.toByte()); by Bill of Landing, by PO / Transfer, by Pallet Id, for Inquiry.
        char NewCartonId[29];    // Carton ID scanned for inquiry      Client
        char Source[2];          // P=PO, T=Transfer                   Server
        char TranNo[10];         // PO or Transfer Number              Server
        char BOL[31];            // Bill of Lading                     Server
        char SelQtyShp[10];      // Unit Shipped in Sell Units         Server
        char UpdCartonId[29];    // Update Carton ID as Received       Client
        char ReadStatus[2];      // Read Status 1=OK, 0=Failed         Server
        char ErrorString[81];    // Error String                       Server
        char ErrorCode[6];       // Error Code                         Server
        char NetStatus[2];       // Network Status 1=OK, 0=Failed      Server
    };
    */
    const val iRec_ClrHdr_TotalType = iRecordId + sRecordId
    const val sRec_ClrHdr_TotalType = 2
    const val iRec_ClrHdr_NewCartonId = iRec_ClrHdr_TotalType + sRec_ClrHdr_TotalType
    const val sRec_ClrHdr_NewCartonId = 29
    const val iRec_ClrHdr_Source = iRec_ClrHdr_NewCartonId + sRec_ClrHdr_NewCartonId
    const val sRec_ClrHdr_Source = 2
    const val iRec_ClrHdr_TranNo = iRec_ClrHdr_Source + sRec_ClrHdr_Source
    const val sRec_ClrHdr_TranNo = 10
    const val iRec_ClrHdr_BOL = iRec_ClrHdr_TranNo + sRec_ClrHdr_TranNo
    const val sRec_ClrHdr_BOL = 31
    const val iRec_ClrHdr_SelQtyShp = iRec_ClrHdr_BOL + sRec_ClrHdr_BOL
    const val sRec_ClrHdr_SelQtyShp = 10
    const val iRec_ClrHdr_UpdCartonId = iRec_ClrHdr_SelQtyShp + sRec_ClrHdr_SelQtyShp
    const val sRec_ClrHdr_UpdCartonId = 29
    const val iRec_ClrHdr_ReadStatus = iRec_ClrHdr_UpdCartonId + sRec_ClrHdr_UpdCartonId
    const val sRec_ClrHdr_ReadStatus = 2
    const val iRec_ClrHdr_ErrorString = iRec_ClrHdr_ReadStatus + sRec_ClrHdr_ReadStatus
    const val sRec_ClrHdr_ErrorString = 81
    const val iRec_ClrHdr_ErrorCode = iRec_ClrHdr_ErrorString + sRec_ClrHdr_ErrorString
    const val sRec_ClrHdr_ErrorCode = 6
    const val iRec_ClrHdr_NetStatus = iRec_ClrHdr_ErrorCode + sRec_ClrHdr_ErrorCode
    const val sRec_ClrHdr_NetStatus = 2
    const val sRec_ClrHdr = iRec_ClrHdr_NetStatus + sRec_ClrHdr_NetStatus
    var REC_ClrHdr = ByteArray(sRec_ClrHdr)

    /*
    struct Rec_ClrDtl { // RecordId = "ClrDtl210" in Clr.cpp; DbFunction 29; Format 92; CartonDtl; PmWinDbm.app (15); Calls ReceiveDetail or UnReceiveDetail in CLR.app (14/39)
        unsigned char status;    // Description                        Filled by
        char RecordId[13];       // Record Identifier                  Client
        char NewItem[29];        // SKU/UPC scanned for inquiry        Client
        char Description1[16];   // Item Description 1                 Client
        char Description2[21];   // Item Description 2                 Client
        char SelQtyRec[10];      // Total Units Received               Server
        char DefaultQty[10];     // Default Quantity                   Server
        char SKU[29];            // SKU to be Received                 Client
        char Quantity[10];       // Quantity to be received            Client
        char ReadStatus[2];      // Read Status 1=OK, 0=Failed         Server
        char ErrorString[81];    // Error String                       Server
        char ErrorCode[6];       // Error Code                         Server
        char NetStatus[2];       // Network Status 1=OK, 0=Failed      Server
    };
    */
    const val iRec_ClrDtl_NewItem = iRecordId + sRecordId
    const val sRec_ClrDtl_NewItem = 29
    const val iRec_ClrDtl_Description1 = iRec_ClrDtl_NewItem + sRec_ClrDtl_NewItem
    const val sRec_ClrDtl_Description1 = 16
    const val iRec_ClrDtl_Description2 = iRec_ClrDtl_Description1 + sRec_ClrDtl_Description1
    const val sRec_ClrDtl_Description2 = 21
    const val iRec_ClrDtl_SelQtyRec = iRec_ClrDtl_Description2 + sRec_ClrDtl_Description2
    const val sRec_ClrDtl_SelQtyRec = 10
    const val iRec_ClrDtl_DefaultQty = iRec_ClrDtl_SelQtyRec + sRec_ClrDtl_SelQtyRec
    const val sRec_ClrDtl_DefaultQty = 10
    const val iRec_ClrDtl_SKU = iRec_ClrDtl_DefaultQty + sRec_ClrDtl_DefaultQty
    const val sRec_ClrDtl_SKU = 29
    const val iRec_ClrDtl_Quantity = iRec_ClrDtl_SKU + sRec_ClrDtl_SKU
    const val sRec_ClrDtl_Quantity = 10
    const val iRec_ClrDtl_ReadStatus = iRec_ClrDtl_Quantity + sRec_ClrDtl_Quantity
    const val sRec_ClrDtl_ReadStatus = 2
    const val iRec_ClrDtl_ErrorString = iRec_ClrDtl_ReadStatus + sRec_ClrDtl_ReadStatus
    const val sRec_ClrDtl_ErrorString = 81
    const val iRec_ClrDtl_ErrorCode = iRec_ClrDtl_ErrorString + sRec_ClrDtl_ErrorString
    const val sRec_ClrDtl_ErrorCode = 6
    const val iRec_ClrDtl_NetStatus = iRec_ClrDtl_ErrorCode + sRec_ClrDtl_ErrorCode
    const val sRec_ClrDtl_NetStatus = 2
    const val iRec_ClrDtl_Count = iRec_ClrDtl_NetStatus + sRec_ClrDtl_NetStatus
    const val sRec_ClrDtl_Count = 10
    const val sRec_ClrDtl = iRec_ClrDtl_Count + sRec_ClrDtl_Count
    var REC_ClrDtl = ByteArray(sRec_ClrDtl)

    /*
    struct Rec_ClrTot { // RecordId = "ClrTot210" in Clr.cpp; DbFunction 30; Format 93; CartonTot; PmWinDbm.app (15); Calls CalculateCartons in CLR.app (6)
        unsigned char status;    // Description                        Filled by
        char RecordId[13];       // Record Identifier                  Client
        char PrintReport[2];     // Print Receiving Report?(Y/N)       Client
        char Source[2];          // P=PO, T=Transfer                   Client
        char TranNo[10];         // PO or Transfer Number              Client
        char BOL[31];            // Bill of Lading Number              Client
        char CtnQtyShp[10];      // Total Cartons Shipped              Server
        char CtnQtyRec[10];      // Total Cartons Received             Server
        char SelQtyShp[10];      // Total Units Shipped in Sell Units  Server
        char SelQtyRec[10];      // Total Units Received in Sell Units Server
        char ReadStatus[2];      // Read Status 1=OK, 0=Failed         Server
        char ErrorString[81];    // Error String                       Server
        char ErrorCode[6];       // Error Code                         Server
        char NetStatus[2];       // Network Status 1=OK, 0=Failed      Server
    };
    */
    const val iRec_ClrTot_PrintReport = iRecordId + sRecordId
    const val sRec_ClrTot_PrintReport = 2
    const val iRec_ClrTot_Source = iRec_ClrTot_PrintReport + sRec_ClrTot_PrintReport
    const val sRec_ClrTot_Source = 2
    const val iRec_ClrTot_TranNo = iRec_ClrTot_Source + sRec_ClrTot_Source
    const val sRec_ClrTot_TranNo = 10
    const val iRec_ClrTot_BOL = iRec_ClrTot_TranNo + sRec_ClrTot_TranNo
    const val sRec_ClrTot_BOL = 31
    const val iRec_ClrTot_CtnQtyShp = iRec_ClrTot_BOL + sRec_ClrTot_BOL
    const val sRec_ClrTot_CtnQtyShp = 10
    const val iRec_ClrTot_CtnQtyRec = iRec_ClrTot_CtnQtyShp + sRec_ClrTot_CtnQtyShp
    const val sRec_ClrTot_CtnQtyRec = 10
    const val iRec_ClrTot_SelQtyShp = iRec_ClrTot_CtnQtyRec + sRec_ClrTot_CtnQtyRec
    const val sRec_ClrTot_SelQtyShp = 10
    const val iRec_ClrTot_SelQtyRec = iRec_ClrTot_SelQtyShp + sRec_ClrTot_SelQtyShp
    const val sRec_ClrTot_SelQtyRec = 10
    const val iRec_ClrTot_ReadStatus = iRec_ClrTot_SelQtyRec + sRec_ClrTot_SelQtyRec
    const val sRec_ClrTot_ReadStatus = 2
    const val iRec_ClrTot_ErrorString = iRec_ClrTot_ReadStatus + sRec_ClrTot_ReadStatus
    const val sRec_ClrTot_ErrorString = 81
    const val iRec_ClrTot_ErrorCode = iRec_ClrTot_ErrorString + sRec_ClrTot_ErrorString
    const val sRec_ClrTot_ErrorCode = 6
    const val iRec_ClrTot_NetStatus = iRec_ClrTot_ErrorCode + sRec_ClrTot_ErrorCode
    const val sRec_ClrTot_NetStatus = 2
    const val sRec_ClrTot = iRec_ClrTot_NetStatus + sRec_ClrTot_NetStatus
    var REC_ClrTot = ByteArray(sRec_ClrTot)

    /*
    struct Rec_InvInq { // RecordId = "AssocStr4425" in Scr_AssocScanItem*.cpp; DbFunction 21; Format 105; StoreItemLookup; PmWinDbm.app (13); Calls ExtendedGetItem
        unsigned char status;    // RecordId = "AssocInv4425" in Inventory.cpp; DbFunction 22; Format 105; StoreInventoryLookup; PmWinDbm.app (13);
        char RecordId[13];       // RecordId = "InvInq4425" in Inventory.cpp; DbFunction 20; Format 105; ExtendedGetItem; PmWinDbm.app (13);
        char SKU[29];
        char FunctionID[3];
        char Description1[16];
        char Description2[21];
        char Price[10];
        char Vendor[16];
        char ReadStatus[2];
        char ErrorString[81];
        char ErrorCode[6];
        char NetStatus[2];
        char Style[16];
        char Color[6];
        char Size[5];
        char Dimension[5];
        char EventCode[8];
        char EventPrice[10];
        char VendorName[16];
        char StyleVendor[10];
        char StyleVendorName[16];
        char OnHand[10];
        char OnHold[10];
        char OnOrder[10];
        char VendorPart[16];
        char AssocLocation[10];
        char AssocFax[16];
        char AssocManager[16];
        char AssocManFax[16];
        char AssocManPhone[16];
        char AssocOnHand[10];
        char AssocPhone[16];
        char AssocShrtName[9];
        char AssocStoreName[16];
        char Buy_Unit[7];
        char Buy_Qty[13];
        char Class[4];
        char Department[4];
        char ManPartNo[16];
        char Sell_Unit[7];
        char Std_Case[7];
        char Sub_Class[4];
        char Sub_Dept[4];
        char BuyQty_Avg8Wk[13];
    };
    */
    const val iRec_InvInq_SKU = iRecordId + sRecordId
    const val sRec_InvInq_SKU = 29
    const val iRec_InvInq_FunctionID = iRec_InvInq_SKU + sRec_InvInq_SKU
    const val sRec_InvInq_FunctionID = 3
    const val iRec_InvInq_Description1 = iRec_InvInq_FunctionID + sRec_InvInq_FunctionID
    const val sRec_InvInq_Description1 = 16
    const val iRec_InvInq_Description2 = iRec_InvInq_Description1 + sRec_InvInq_Description1
    const val sRec_InvInq_Description2 = 21
    const val iRec_InvInq_Price = iRec_InvInq_Description2 + sRec_InvInq_Description2
    const val sRec_InvInq_Price = 10
    const val iRec_InvInq_Vendor = iRec_InvInq_Price + sRec_InvInq_Price
    const val sRec_InvInq_Vendor = 16
    const val iRec_InvInq_ReadStatus = iRec_InvInq_Vendor + sRec_InvInq_Vendor
    const val sRec_InvInq_ReadStatus = 2
    const val iRec_InvInq_ErrorString = iRec_InvInq_ReadStatus + sRec_InvInq_ReadStatus
    const val sRec_InvInq_ErrorString = 81
    const val iRec_InvInq_ErrorCode = iRec_InvInq_ErrorString + sRec_InvInq_ErrorString
    const val sRec_InvInq_ErrorCode = 6
    const val iRec_InvInq_NetStatus = iRec_InvInq_ErrorCode + sRec_InvInq_ErrorCode
    const val sRec_InvInq_NetStatus = 2
    const val iRec_InvInq_Style = iRec_InvInq_NetStatus + sRec_InvInq_NetStatus
    const val sRec_InvInq_Style = 16
    const val iRec_InvInq_Color = iRec_InvInq_Style + sRec_InvInq_Style
    const val sRec_InvInq_Color = 6
    const val iRec_InvInq_Size = iRec_InvInq_Color + sRec_InvInq_Color
    const val sRec_InvInq_Size = 5
    const val iRec_InvInq_Dimension = iRec_InvInq_Size + sRec_InvInq_Size
    const val sRec_InvInq_Dimension = 5
    const val iRec_InvInq_EventCode = iRec_InvInq_Dimension + sRec_InvInq_Dimension
    const val sRec_InvInq_EventCode = 8
    const val iRec_InvInq_EventPrice = iRec_InvInq_EventCode + sRec_InvInq_EventCode
    const val sRec_InvInq_EventPrice = 10
    const val iRec_InvInq_VendorName = iRec_InvInq_EventPrice + sRec_InvInq_EventPrice
    const val sRec_InvInq_VendorName = 16
    const val iRec_InvInq_StyleVendor = iRec_InvInq_VendorName + sRec_InvInq_VendorName
    const val sRec_InvInq_StyleVendor = 10
    const val iRec_InvInq_StyleVendorName = iRec_InvInq_StyleVendor + sRec_InvInq_StyleVendor
    const val sRec_InvInq_StyleVendorName = 16
    const val iRec_InvInq_OnHand = iRec_InvInq_StyleVendorName + sRec_InvInq_StyleVendorName
    const val sRec_InvInq_OnHand = 10
    const val iRec_InvInq_OnHold = iRec_InvInq_OnHand + sRec_InvInq_OnHand
    const val sRec_InvInq_OnHold = 10
    const val iRec_InvInq_OnOrder = iRec_InvInq_OnHold + sRec_InvInq_OnHold
    const val sRec_InvInq_OnOrder = 10
    const val iRec_InvInq_VendorPart = iRec_InvInq_OnOrder + sRec_InvInq_OnOrder
    const val sRec_InvInq_VendorPart = 16
    const val iRec_InvInq_AssocLocation = iRec_InvInq_VendorPart + sRec_InvInq_VendorPart
    const val sRec_InvInq_AssocLocation = 10
    const val iRec_InvInq_AssocFax = iRec_InvInq_AssocLocation + sRec_InvInq_AssocLocation
    const val sRec_InvInq_AssocFax = 16
    const val iRec_InvInq_AssocManager = iRec_InvInq_AssocFax + sRec_InvInq_AssocFax
    const val sRec_InvInq_AssocManager = 16
    const val iRec_InvInq_AssocManFax = iRec_InvInq_AssocManager + sRec_InvInq_AssocManager
    const val sRec_InvInq_AssocManFax = 16
    const val iRec_InvInq_AssocManPhone = iRec_InvInq_AssocManFax + sRec_InvInq_AssocManFax
    const val sRec_InvInq_AssocManPhone = 16
    const val iRec_InvInq_AssocOnHand = iRec_InvInq_AssocManPhone + sRec_InvInq_AssocManPhone
    const val sRec_InvInq_AssocOnHand = 10
    const val iRec_InvInq_AssocPhone = iRec_InvInq_AssocOnHand + sRec_InvInq_AssocOnHand
    const val sRec_InvInq_AssocPhone = 16
    const val iRec_InvInq_AssocShrtName = iRec_InvInq_AssocPhone + sRec_InvInq_AssocPhone
    const val sRec_InvInq_AssocShrtName = 9
    const val iRec_InvInq_AssocStoreName = iRec_InvInq_AssocShrtName + sRec_InvInq_AssocShrtName
    const val sRec_InvInq_AssocStoreName = 16
    const val iRec_InvInq_Buy_Unit = iRec_InvInq_AssocStoreName + sRec_InvInq_AssocStoreName
    const val sRec_InvInq_Buy_Unit = 7
    const val iRec_InvInq_Buy_Qty = iRec_InvInq_Buy_Unit + sRec_InvInq_Buy_Unit
    const val sRec_InvInq_Buy_Qty = 13
    const val iRec_InvInq_Class = iRec_InvInq_Buy_Qty + sRec_InvInq_Buy_Qty
    const val sRec_InvInq_Class = 4
    const val iRec_InvInq_Department = iRec_InvInq_Class + sRec_InvInq_Class
    const val sRec_InvInq_Department = 4
    const val iRec_InvInq_ManPartNo = iRec_InvInq_Department + sRec_InvInq_Department
    const val sRec_InvInq_ManPartNo = 16
    const val iRec_InvInq_Sell_Unit = iRec_InvInq_ManPartNo + sRec_InvInq_ManPartNo
    const val sRec_InvInq_Sell_Unit = 7
    const val iRec_InvInq_Std_Case = iRec_InvInq_Sell_Unit + sRec_InvInq_Sell_Unit
    const val sRec_InvInq_Std_Case = 7
    const val iRec_InvInq_Sub_Class = iRec_InvInq_Std_Case + sRec_InvInq_Std_Case
    const val sRec_InvInq_Sub_Class = 4
    const val iRec_InvInq_Sub_Dept = iRec_InvInq_Sub_Class + sRec_InvInq_Sub_Class
    const val sRec_InvInq_Sub_Dept = 4
    const val iRec_InvInq_BuyQty_Avg8Wk = iRec_InvInq_Sub_Dept + sRec_InvInq_Sub_Dept
    const val sRec_InvInq_BuyQty_Avg8Wk = 13
    const val sRec_InvInq = iRec_InvInq_BuyQty_Avg8Wk + sRec_InvInq_BuyQty_Avg8Wk
    var REC_InvInq = ByteArray(sRec_InvInq)
}