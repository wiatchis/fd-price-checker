package com.prismkt

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class LookupActivity : AppCompatActivity(), View.OnKeyListener {
    private val c: Common = Common.Companion.instance
    private val s: Server = Server.Companion.instance
    private var _hObj: Handler? = null
    private var _etCode: EditText? = null
    private var _etName: EditText? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnOK: Button? = null
    private var _btnSelect: Button? = null
    private var _sCode: String? = null
    private var _sName: String? = null
    private var _sLookupCode: String? = null
    private var _sLookupName: String? = null
    private var _iLookupWhat: Int = 0
    private var _iLookupCall: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lookup)
        _iLookupWhat = getIntent().getIntExtra("iLookupWhat", 0)
        if (_iLookupWhat == 0) { //LU_STRMST
            this.setTitle(R.string.act_lookup_stores)
        } else { //LU_VENMST
            this.setTitle(R.string.act_lookup_vendors)
        }
        _hObj = Handler(Looper.getMainLooper())
        _etCode = findViewById<View>(R.id.IDC_LOOKUP_LOOKUP_EDIT_CODE) as EditText?
        _etName = findViewById<View>(R.id.IDC_LOOKUP_LOOKUP_EDIT_NAME) as EditText?
        _btnOK = findViewById<View>(R.id.IDOK) as Button?
        _btnSelect = findViewById<View>(R.id.IDC_LOOKUP_LOOKUP_PUSHBUTTON_SELECT) as Button?
        _tvMsg = findViewById<View>(R.id.IDC_LOOKUP_LOOKUP_TEXT_MSG) as TextView?
        _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
        val imm: InputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        c.PrismReqFocus(_hObj, _etCode, imm)
        _etCode!!.setOnKeyListener(this)
        _etName!!.setOnKeyListener(this)
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(
            this,
            R.id.clLookup,
            (!_etCode!!.getText().toString().trim({ it <= ' ' }).isEmpty()) || (!_etName!!.getText()
                .toString().trim({ it <= ' ' }).isEmpty())
        )
        val filter: IntentFilter = IntentFilter()
        filter.addCategory(g.sFilterCategory)
        filter.addAction(g.sFilterAction)
        registerReceiver(scanBroadcastReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clLookup)
        unregisterReceiver(scanBroadcastReceiver)
    }

    public override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
        if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
            _btnOK!!.performClick()
            return true
        }
        return false
    }

    fun btnIDOK(view: View?) {
        _sCode = _etCode!!.getText().toString().trim({ it <= ' ' })
        _sName = _etName!!.getText().toString().trim({ it <= ' ' })
        _iLookupCall = 0
        executeAction()
    }

    fun btnIDC_LOOKUP_LOOKUP_PUSHBUTTON_FIRST(view: View?) {
        _sCode = _etCode!!.getText().toString().trim({ it <= ' ' })
        _sName = _etName!!.getText().toString().trim({ it <= ' ' })
        _iLookupCall = 1
        executeAction()
    }

    fun btnIDC_LOOKUP_LOOKUP_PUSHBUTTON_NEXT(view: View?) {
        _sCode = _etCode!!.getText().toString().trim({ it <= ' ' })
        _sName = _etName!!.getText().toString().trim({ it <= ' ' })
        _iLookupCall = 2
        executeAction()
    }

    fun btnIDC_LOOKUP_LOOKUP_PUSHBUTTON_SELECT(view: View?) {
        onBackPressed()
    }

    fun btnIDCANCEL(view: View?) {
        Arrays.fill(g.REC_Receiver, 0.toByte())
        onBackPressed()
    }

    public override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            Arrays.fill(g.REC_Receiver, 0.toByte())
            onBackPressed()
            return true
        }
        return (super.onOptionsItemSelected(item))
    }

    private fun executeAction() {
        Thread(object : Runnable {
            public override fun run() {
                var iMsg: Int
                var bCtl: Boolean = false
                Arrays.fill(g.REC_Receiver, 0.toByte())
                c.PrismCopyString(
                    g.REC_Receiver,
                    g.iRec_Receiver_OrigLocation,
                    g.sRec_Receiver_OrigLocation,
                    _sCode
                )
                c.PrismCopyString(
                    g.REC_Receiver,
                    g.iRec_Receiver_OrigName,
                    g.sRec_Receiver_OrigName,
                    _sName
                )
                if ((_iLookupCall != 0) || !(_sCode == _sLookupCode) || !(_sName == _sLookupName)) {
                    if (_iLookupCall <= 1) { //R.id.IDOK or R.id.IDC_LOOKUP_LOOKUP_PUSHBUTTON_FIRST
                        //R.id.IDOK or R.id.IDC_LOOKUP_LOOKUP_PUSHBUTTON_FIRST
                        g.REC_Receiver[g.iRec_Receiver_LookupStatus] = 0x49 //'I'
                        if (_iLookupWhat == 0) { //LU_STRMST
                            //LU_STRMST
                            g.REC_Receiver[g.iRec_Receiver_LookupType] = 0x31 //'1
                            iMsg = R.string.msg_getting_first_store
                        } else { //LU_VENMST
                            iMsg = R.string.msg_getting_first_vendor
                        }
                    } else { //R.id.IDC_LOOKUP_LOOKUP_PUSHBUTTON_NEXT
                        //R.id.IDC_LOOKUP_LOOKUP_PUSHBUTTON_NEXT
                        g.REC_Receiver[g.iRec_Receiver_LookupStatus] = 0x4E //'N'
                        if (_iLookupWhat == 0) {
                            g.REC_Receiver[g.iRec_Receiver_LookupType] = 0x31 //'1
                            iMsg = R.string.msg_getting_next_store
                        } else {
                            iMsg = R.string.msg_getting_next_vendor
                        }
                    }
                } else {
                    c.PrismReqFocus(_hObj, _etCode)
                    return
                }
                c.PrismMsg(_hObj, _tvMsg, iMsg)
                c.PrismCopyString(
                    g.REC_Receiver,
                    g.iRecordId,
                    g.sRecordId,
                    "VndLkup4307"
                ) //DbFunction 12; Format 96; VendorLookup; PmWinDbm.app (12); Reads VenMst.tps
                if (s.ServerSend(g.REC_Receiver, _hObj, _tvMsg, _pbAct)) {
                    if (g.REC_Receiver.get(g.iRec_Receiver_ReadStatus) == 0x31.toByte()) {
                        _sLookupCode = c.PrismGetString(
                            g.REC_Receiver,
                            g.iRec_Receiver_OrigLocation,
                            g.sRec_Receiver_OrigLocation
                        )
                        c.PrismMsg(_hObj, _etCode, _sLookupCode)
                        _sLookupName = c.PrismGetString(
                            g.REC_Receiver,
                            g.iRec_Receiver_OrigName,
                            g.sRec_Receiver_OrigName
                        )
                        c.PrismMsg(_hObj, _etName, _sLookupName)
                        if (_iLookupCall <= 1) { //R.id.IDOK or R.id.IDC_LOOKUP_LOOKUP_PUSHBUTTON_FIRST
                            if (_iLookupWhat == 0) { //LU_STRMST
                                iMsg = R.string.msg_got_first_store
                            } else { //LU_VENMST
                                iMsg = R.string.msg_got_first_vendor
                            }
                        } else { //R.id.IDC_LOOKUP_LOOKUP_PUSHBUTTON_NEXT
                            if (_iLookupWhat == 0) {
                                iMsg = R.string.msg_got_next_store
                            } else {
                                iMsg = R.string.msg_got_next_vendor
                            }
                        }
                        c.PrismMsg(_hObj, _tvMsg, iMsg)
                        bCtl = true
                    } else {
                        c.PrismMsg(
                            _hObj,
                            _tvMsg,
                            c.PrismGetString(
                                g.REC_Receiver,
                                g.iRec_Receiver_ErrorString,
                                g.sRec_Receiver_ErrorString
                            )
                        )
                    }
                }
                c.PrismBtnEnabled(_hObj, _btnSelect, bCtl)
            }
        }).start()
    }

    private val scanBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        public override fun onReceive(context: Context, intent: Intent) {
            if ((intent.getAction() == g.sFilterAction)) {
                try {
                    displayScanResult(intent)
                } catch (ignored: Exception) {
                }
            }
        }
    }

    private fun displayScanResult(i: Intent) {
        _etCode!!.setText(i.getStringExtra(g.sScanData))
        _sLookupCode = ""
        _sLookupName = ""
        _sCode = ""
        _sName = ""
        _btnOK!!.performClick()
    }
}