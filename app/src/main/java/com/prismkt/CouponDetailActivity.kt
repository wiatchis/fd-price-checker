package com.prismkt

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class CouponDetailActivity constructor() : AppCompatActivity() {
    private val c: Common = Common.Companion.instance
    private val s: Server = Server.Companion.instance
    private var _hObj: Handler? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _iCouponDetailWhat: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coupon_detail)
        _iCouponDetailWhat = getIntent().getIntExtra("iCouponDetailWhat", 0)
        _hObj = Handler(Looper.getMainLooper())
        _tvMsg = findViewById<View>(R.id.tvMsg) as TextView?
        _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
        getCoupon()
    }

    public override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed()
            return true
        }
        return (super.onOptionsItemSelected(item))
    }

    private fun getCoupon() { Thread(ExecuteThread()).start() }

    private fun gotCoupon() {
        var tv: TextView
        var sMsg: String?
        tv = findViewById<View>(R.id.tvCouponD) as TextView
        sMsg = c.PrismGetString(
            g.REC_CpnDtl,
            g.iRec_CpnDtl_CouponMasterId,
            g.sRec_CpnDtl_CouponMasterId
        )
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvDescD) as TextView
        if (_iCouponDetailWhat == 0) sMsg = c.PrismGetString(
            g.REC_CpnBrw,
            g.iRec_CpnBrw_Description,
            g.sRec_CpnBrw_Description
        ) else sMsg =
            c.PrismGetString(g.REC_CpnSku, g.iRec_CpnSku_Description, g.sRec_CpnSku_Description)
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvTypeD) as TextView
        if (_iCouponDetailWhat == 0) sMsg = c.PrismGetString(
            g.REC_CpnBrw,
            g.iRec_CpnBrw_CouponType,
            g.sRec_CpnBrw_CouponType
        ) else sMsg =
            c.PrismGetString(g.REC_CpnSku, g.iRec_CpnSku_CouponType, g.sRec_CpnSku_CouponType)
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvAutoApplyD) as TextView
        sMsg = c.PrismGetString(g.REC_CpnDtl, g.iRec_CpnDtl_AutoApply, g.sRec_CpnDtl_AutoApply)
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvFromD) as TextView
        if (_iCouponDetailWhat == 0) sMsg = c.PrismGetString(
            g.REC_CpnBrw,
            g.iRec_CpnBrw_StartDate,
            g.sRec_CpnBrw_StartDate
        ) else sMsg =
            c.PrismGetString(g.REC_CpnSku, g.iRec_CpnSku_StartDate, g.sRec_CpnSku_StartDate)
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvStartTimeD) as TextView
        if (_iCouponDetailWhat == 0) sMsg = c.PrismGetString(
            g.REC_CpnBrw,
            g.iRec_CpnBrw_StartTime,
            g.sRec_CpnBrw_StartTime
        ) else sMsg =
            c.PrismGetString(g.REC_CpnSku, g.iRec_CpnSku_StartTime, g.sRec_CpnSku_StartTime)
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvToD) as TextView
        if (_iCouponDetailWhat == 0) sMsg =
            c.PrismGetString(g.REC_CpnBrw, g.iRec_CpnBrw_EndDate, g.sRec_CpnBrw_EndDate) else sMsg =
            c.PrismGetString(g.REC_CpnSku, g.iRec_CpnSku_EndDate, g.sRec_CpnSku_EndDate)
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvEndTimeD) as TextView
        if (_iCouponDetailWhat == 0) sMsg =
            c.PrismGetString(g.REC_CpnBrw, g.iRec_CpnBrw_EndTime, g.sRec_CpnBrw_EndTime) else sMsg =
            c.PrismGetString(g.REC_CpnSku, g.iRec_CpnSku_EndTime, g.sRec_CpnSku_EndTime)
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvBuyQtyD) as TextView
        sMsg = c.PrismGetString(g.REC_CpnDtl, g.iRec_CpnDtl_BuyQty, g.sRec_CpnDtl_BuyQty)
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvGetQtyD) as TextView
        sMsg = c.PrismGetString(g.REC_CpnDtl, g.iRec_CpnDtl_GetQty, g.sRec_CpnDtl_GetQty)
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvDiscTypeD) as TextView
        sMsg =
            c.PrismGetString(g.REC_CpnDtl, g.iRec_CpnDtl_DiscountType, g.sRec_CpnDtl_DiscountType)
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvCouponValueD) as TextView
        sMsg = c.PrismGetString(g.REC_CpnDtl, g.iRec_CpnDtl_CouponValue, g.sRec_CpnDtl_CouponValue)
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvMaxCouponValueD) as TextView
        sMsg = c.PrismGetString(
            g.REC_CpnDtl,
            g.iRec_CpnDtl_MaxCouponValue,
            g.sRec_CpnDtl_MaxCouponValue
        )
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvMaxCouponsPerTxD) as TextView
        sMsg = c.PrismGetString(
            g.REC_CpnDtl,
            g.iRec_CpnDtl_MaxCouponsPerTx,
            g.sRec_CpnDtl_MaxCouponsPerTx
        )
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvExcludeEmployeeD) as TextView
        sMsg = c.PrismGetString(
            g.REC_CpnDtl,
            g.iRec_CpnDtl_ExcludeEmployee,
            g.sRec_CpnDtl_ExcludeEmployee
        )
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvExcludePromoD) as TextView
        sMsg =
            c.PrismGetString(g.REC_CpnDtl, g.iRec_CpnDtl_ExcludePromo, g.sRec_CpnDtl_ExcludePromo)
        tv.setText(sMsg)
        tv = findViewById<View>(R.id.tvMinPurchReqD) as TextView
        sMsg = c.PrismGetString(g.REC_CpnDtl, g.iRec_CpnDtl_MinPurchReq, g.sRec_CpnDtl_MinPurchReq)
        tv.setText(sMsg)
    }

    internal inner class ExecuteThread constructor() : Runnable {
        public override fun run() {
            Arrays.fill(g.REC_CpnDtl, 0.toByte())
            c.PrismCopyString(
                g.REC_CpnDtl,
                g.iRecordId,
                g.sRecordId,
                "CpnDtl"
            ) //DbFunction 37; Format 112; CouponDetail; PmWinDbm.app (44).
            if (_iCouponDetailWhat == 0) System.arraycopy(
                g.REC_CpnBrw,
                g.iRec_CpnBrw_CouponMasterId,
                g.REC_CpnDtl,
                g.iRec_CpnDtl_CouponMasterId,
                g.sRec_CpnDtl_CouponMasterId
            ) else System.arraycopy(
                g.REC_CpnSku,
                g.iRec_CpnSku_CouponMasterId,
                g.REC_CpnDtl,
                g.iRec_CpnDtl_CouponMasterId,
                g.sRec_CpnDtl_CouponMasterId
            )
            c.PrismMsg(_hObj, _tvMsg, R.string.msg_getting_coupon)
            if (s.ServerSend(g.REC_CpnDtl, _hObj, _tvMsg, _pbAct)) {
                if (g.REC_CpnDtl.get(g.iRec_CpnDtl_ReadStatus) == 0x31.toByte()) {
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_got_coupon)
                    _hObj!!.post(object : Runnable {
                        public override fun run() {
                            synchronized(Server.Companion._syncToken, {
                                _tvMsg!!.setVisibility(View.GONE)
                                _pbAct!!.setVisibility(View.GONE)
                                gotCoupon()
                            })
                        }
                    })
                } else {
                    c.PrismMsg(
                        _hObj,
                        _tvMsg,
                        c.PrismGetString(
                            g.REC_CpnBrw,
                            g.iRec_CpnBrw_ErrorString,
                            g.sRec_CpnBrw_ErrorString
                        )
                    )
                }
            }
        }
    }
}