package com.prismkt

import android.content.*
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class ClrScanBolActivity constructor() : AppCompatActivity() {
    internal enum class csbAction { GET, RECVINFULL, FIRST, NEXT, PREV, LAST, CLOSE }
    private val c: Common = Common.Companion.instance
    private val s: Server = Server.Companion.instance
    private var _hObj: Handler? = null
    private var _etShipment: EditText? = null
    private var _tvDate: TextView? = null
    private var _tvPalShipped: TextView? = null
    private var _tvPalReceived: TextView? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnIDOK: Button? = null
    private var _btnIDC_CLR_SCANBOL_PUSHBUTTON_PALLETS: Button? = null
    private var _btnIDC_CLR_SCANBOL_PUSHBUTTON_RECVINFULL: Button? = null
    private var _btnIDC_CLR_SCANBOL_PUSHBUTTON_NEXT: Button? = null
    private var _btnIDC_CLR_SCANBOL_PUSHBUTTON_PREV: Button? = null
    private var _btnIDCANCEL: Button? = null
    private var _sShipment: String? = ""
    private var _iClrScanCartonTotalType: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clr_scan_bol)
        _etShipment = findViewById<View>(R.id.IDC_CLR_SCANBOL_EDIT_BOL) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_CLR_SCANBOL_TEXT_MSG) as TextView?
        _btnIDOK = findViewById<View>(R.id.IDOK) as Button?
        _btnIDCANCEL = findViewById<View>(R.id.IDCANCEL) as Button?
        _btnIDC_CLR_SCANBOL_PUSHBUTTON_PALLETS =
            findViewById<View>(R.id.IDC_CLR_SCANBOL_PUSHBUTTON_PALLETS) as Button?
        _btnIDC_CLR_SCANBOL_PUSHBUTTON_RECVINFULL =
            findViewById<View>(R.id.IDC_CLR_SCANBOL_PUSHBUTTON_RECVINFULL) as Button?
        _btnIDC_CLR_SCANBOL_PUSHBUTTON_NEXT =
            findViewById<View>(R.id.IDC_CLR_SCANBOL_PUSHBUTTON_NEXT) as Button?
        _btnIDC_CLR_SCANBOL_PUSHBUTTON_PREV =
            findViewById<View>(R.id.IDC_CLR_SCANBOL_PUSHBUTTON_PREV) as Button?
        if (g.REC_Security.get(g.iRec_Security_Carton) == 0x30.toByte()) {
            _tvMsg!!.setText(R.string.msg_not_authorized)
            _etShipment!!.setEnabled(false)
            _btnIDOK!!.setEnabled(false)
            _btnIDC_CLR_SCANBOL_PUSHBUTTON_PALLETS!!.setEnabled(false)
            _btnIDC_CLR_SCANBOL_PUSHBUTTON_RECVINFULL!!.setEnabled(false)
            _btnIDC_CLR_SCANBOL_PUSHBUTTON_NEXT!!.setEnabled(false)
            _btnIDC_CLR_SCANBOL_PUSHBUTTON_PREV!!.setEnabled(false)
            var btn: Button = findViewById<View>(R.id.IDC_CLR_SCANBOL_PUSHBUTTON_FIRST) as Button
            btn.setEnabled(false)
            btn = findViewById<View>(R.id.IDC_CLR_SCANBOL_PUSHBUTTON_LAST) as Button
            btn.setEnabled(false)
            _btnIDCANCEL!!.requestFocus()
            return
        }
        _iClrScanCartonTotalType = getIntent().getIntExtra("iWhat", 0)
        _hObj = Handler(Looper.getMainLooper())
        _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
        _tvDate = findViewById<View>(R.id.IDC_CLR_SCANBOL_EDIT_DATE) as TextView?
        _tvPalShipped = findViewById<View>(R.id.IDC_CLR_SCANBOL_EDIT_PALSHIP) as TextView?
        _tvPalReceived = findViewById<View>(R.id.IDC_CLR_SCANBOL_EDIT_PALRECV) as TextView?
        _etShipment!!.requestFocus()
        _etShipment!!.setOnKeyListener(object : View.OnKeyListener {
            public override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
                if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
                    _btnIDOK!!.performClick()
                    return true
                }
                return false
            }
        })
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(this, R.id.clClrScanBol, (g.REC_ClrTot.get(g.iRec_ClrTot_BOL) != 0x00.toByte()))
        val filter: IntentFilter = IntentFilter()
        filter.addCategory(g.sFilterCategory)
        filter.addAction(g.sFilterAction)
        registerReceiver(scanBroadcastReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clClrScanBol)
        unregisterReceiver(scanBroadcastReceiver)
    }

    fun btnIDOK(view: View?) {
        _sShipment = _etShipment!!.getText().toString().trim({ it <= ' ' })
        if (_sShipment!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_shipment_must_be_entered)
            _etShipment!!.requestFocus()
        } else {
            executeAction(csbAction.GET)
        }
    }

    fun btnIDC_CLR_SCANBOL_PUSHBUTTON_RECVINFULL(view: View?) {
        _sShipment = _etShipment!!.getText().toString().trim({ it <= ' ' })
        if (_sShipment!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_shipment_must_be_entered)
            _etShipment!!.requestFocus()
        } else {
            executeAction(csbAction.RECVINFULL)
        }
    }

    fun btnIDC_CLR_SCANBOL_PUSHBUTTON_FIRST(view: View?) {
        executeAction(csbAction.FIRST)
    }

    fun btnIDC_CLR_SCANBOL_PUSHBUTTON_NEXT(view: View?) {
        executeAction(csbAction.NEXT)
    }

    fun btnIDC_CLR_SCANBOL_PUSHBUTTON_PREV(view: View?) {
        executeAction(csbAction.PREV)
    }

    fun btnIDC_CLR_SCANBOL_PUSHBUTTON_LAST(view: View?) {
        executeAction(csbAction.PREV)
    }

    fun btnIDC_CLR_SCANBOL_PUSHBUTTON_PALLETS(view: View?) {
        Arrays.fill(g.REC_ClrHdr, 0.toByte())
        val intent: Intent = Intent(this, ClrScanCartonActivity::class.java)
        intent.putExtra("iWhat", _iClrScanCartonTotalType)
        startActivity(intent)
    }

    fun btnIDCANCEL(view: View?) {
        checkExit()
    }

    public override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            checkExit()
            return true
        }
        return (super.onOptionsItemSelected(item))
    }

    public override fun onKeyDown(
        keyCode: Int,
        event: KeyEvent
    ): Boolean { //onBackPressed() api lvl 5+
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            checkExit()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun checkExit() {
        if (!_btnIDOK!!.isEnabled()) {
            onBackPressed()
        } else {
            executeAction(csbAction.CLOSE)
        }
    }

    private val scanBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        public override fun onReceive(context: Context, intent: Intent) {
            if ((intent.getAction() == g.sFilterAction)) {
                try {
                    displayScanResult(intent)
                } catch (ignored: Exception) {
                }
            }
        }
    }

    private fun displayScanResult(i: Intent) {
        _etShipment!!.setText(i.getStringExtra(g.sScanData))
        _btnIDOK!!.performClick()
    }

    private fun executeAction(eCall: csbAction) {
        Thread(ExecuteThread(eCall)).start()
    }

    internal inner class ExecuteThread constructor(private val _eCall: csbAction) : Runnable {
        public override fun run() {
            var iMsg: Int
            var sMsg: String?
            var bCtl: Boolean = false
            var bCtl2: Boolean
            Arrays.fill(g.REC_ClrTot, 0.toByte())
            c.PrismCopyString(
                g.REC_ClrTot,
                g.iRecordId,
                g.sRecordId,
                "ClrTot210"
            ) //DbFunction 30; Format 93; CartonTot; PmWinDbm.app (15)
            g.REC_ClrTot[g.iRec_ClrTot_PrintReport] = 0x42.toByte() //'B'
            c.PrismCopyString(g.REC_ClrTot, g.iRec_ClrTot_BOL, g.sRec_ClrTot_BOL, _sShipment)
            when (_eCall) {
                csbAction.GET -> {
                    g.REC_ClrTot[g.iRec_ClrTot_Source] = 0x47.toByte() //'G'
                    iMsg = R.string.msg_getting_shipment
                }
                csbAction.RECVINFULL -> {
                    g.REC_ClrTot[g.iRec_ClrTot_Source] = 0x52.toByte() //'R'
                    iMsg = R.string.msg_receiving_shipment_in_full
                }
                csbAction.FIRST -> {
                    g.REC_ClrTot[g.iRec_ClrTot_Source] = 0x46.toByte() //'F'
                    iMsg = R.string.msg_getting_first_open_shipment
                }
                csbAction.NEXT -> {
                    g.REC_ClrTot[g.iRec_ClrTot_Source] = 0x4E.toByte() //'N'
                    iMsg = R.string.msg_getting_next_open_shipment
                }
                csbAction.PREV -> {
                    g.REC_ClrTot[g.iRec_ClrTot_Source] = 0x50.toByte() //'P'
                    iMsg = R.string.msg_getting_previous_open_shipment
                }
                csbAction.LAST -> {
                    g.REC_ClrTot[g.iRec_ClrTot_Source] = 0x4C.toByte() //'L'
                    iMsg = R.string.msg_getting_last_open_shipment
                }
                csbAction.CLOSE -> {
                    g.REC_ClrTot[g.iRec_ClrTot_Source] = 0x45.toByte() //'E'
                    iMsg = R.string.msg_exiting
                }
                else -> return
            }
            c.PrismMsg(_hObj, _tvMsg, iMsg)
            if (s.ServerSend(g.REC_ClrTot, _hObj, _tvMsg, _pbAct)) {
                _sShipment = c.PrismGetString(g.REC_ClrTot, g.iRec_ClrTot_BOL, g.sRec_ClrTot_BOL)
                c.PrismMsg(_hObj, _etShipment, _sShipment)
                sMsg = c.PrismGetString(g.REC_ClrTot, g.iRec_ClrTot_TranNo, g.sRec_ClrTot_TranNo)
                c.PrismMsg(_hObj, _tvDate, sMsg)
                sMsg =
                    c.PrismGetString(g.REC_ClrTot, g.iRec_ClrTot_CtnQtyShp, g.sRec_ClrTot_CtnQtyShp)
                c.PrismMsg(_hObj, _tvPalShipped, sMsg)
                sMsg =
                    c.PrismGetString(g.REC_ClrTot, g.iRec_ClrTot_CtnQtyRec, g.sRec_ClrTot_CtnQtyRec)
                c.PrismMsg(_hObj, _tvPalReceived, sMsg)
                if (g.REC_ClrTot.get(g.iRec_ClrTot_ReadStatus) == 0x31.toByte()) {
                    when (_eCall) {
                        csbAction.GET -> iMsg = R.string.msg_got_shipment
                        csbAction.RECVINFULL -> iMsg = R.string.msg_received_shipment_in_full
                        csbAction.FIRST -> iMsg = R.string.msg_got_first_open_shipment
                        csbAction.NEXT -> if (g.REC_ClrTot.get(g.iRec_ClrTot_Source) == 0x4E.toByte()) { //'N'
                            iMsg = R.string.msg_got_next_open_shipment
                        } else {
                            iMsg = R.string.msg_got_last_open_shipment
                        }
                        csbAction.PREV -> if (g.REC_ClrTot.get(g.iRec_ClrTot_Source) == 0x50.toByte()) { //'P'
                            iMsg = R.string.msg_got_previous_open_shipment
                        } else {
                            iMsg = R.string.msg_got_last_open_shipment
                        }
                        csbAction.LAST -> iMsg = R.string.msg_got_last_open_shipment
                        csbAction.CLOSE -> {
                            _hObj!!.post(object : Runnable {
                                public override fun run() {
                                    synchronized(Server.Companion._syncToken, {
                                        _btnIDOK!!.setEnabled(false)
                                        _btnIDCANCEL!!.performClick()
                                    })
                                }
                            })
                            return
                        }
                    }
                    bCtl = true
                    c.PrismMsg(_hObj, _tvMsg, iMsg)
                } else {
                    sMsg = c.PrismGetString(
                        g.REC_ClrTot,
                        g.iRec_ClrTot_ErrorString,
                        g.sRec_ClrTot_ErrorString
                    )
                    c.PrismMsg(_hObj, _tvMsg, sMsg)
                }
            }
            bCtl2 = bCtl
            if (bCtl2 && (_eCall == csbAction.RECVINFULL)) bCtl2 = false
            c.PrismBtnEnabled(_hObj, _btnIDC_CLR_SCANBOL_PUSHBUTTON_RECVINFULL, bCtl2)
            bCtl2 =
                !bCtl || (_eCall != csbAction.NEXT) || (g.REC_ClrTot.get(g.iRec_ClrTot_Source) == 0x4E.toByte()) //'N'
            c.PrismBtnEnabled(_hObj, _btnIDC_CLR_SCANBOL_PUSHBUTTON_NEXT, bCtl2)
            bCtl2 =
                !bCtl || (_eCall != csbAction.PREV) || (g.REC_ClrTot.get(g.iRec_ClrTot_Source) == 0x50.toByte()) //'P'
            c.PrismBtnEnabled(_hObj, _btnIDC_CLR_SCANBOL_PUSHBUTTON_PREV, bCtl2)
            if (bCtl) {
                c.PrismReqFocus(_hObj, _btnIDC_CLR_SCANBOL_PUSHBUTTON_PALLETS)
            } else {
                c.PrismReqFocus(_hObj, _etShipment)
            }
        }
    }
}