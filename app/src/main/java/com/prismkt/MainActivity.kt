//
//KIA 07/25/21 - 10/20/21 BSH 16. Bargain! Shop PRISM. Created.
//KIA 12/20/22 - 12/20/22 BSH 16. For the damages reason code, can we put blank reason code. Then, force the user to select the reason code only when they create a new batch?
//KIA 10/21/23 - 12/26/23 DT 33. Price Checker.
//WEA 04/08/24 - 04/16/24 Added Serial Number to the title bar
//
package com.prismkt
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import org.w3c.dom.Document
import org.w3c.dom.NodeList
import org.xml.sax.InputSource
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.io.StringReader
import java.util.*
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory

class MainActivity : AppCompatActivity() {
    private val c: Common = Common.instance
    private val s: Server = Server.instance
    private var _etHost: EditText? = null
    private var _tvMsg: TextView? = null
    private var _tvMsgError: TextView? = null
    private var _iServerChange: Int = 0
    private var checkedPermission = PackageManager.PERMISSION_DENIED
    companion object {
        var serialNumber = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setCustomView(R.layout.custom_action_bar_layout)

        val tvTitleName = findViewById<View>(R.id.titleName) as TextView
        tvTitleName.text = getString(R.string.act_server_connect)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (Environment.isExternalStorageManager()) {
                checkedPermission = PackageManager.PERMISSION_GRANTED
            }
        }
        if (checkedPermission != PackageManager.PERMISSION_GRANTED) {
            val uri = Uri.parse(String.format("package:%s", applicationContext.packageName))
            startActivity(
                Intent(
                    Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION, uri
                )
            )
        }

        val tvSerialNumber = findViewById<View>(R.id.serialNumber) as TextView
        try {
            getSerialNumber()
            tvSerialNumber.text = getString(R.string.act_price_check_serial_number, serialNumber)
        } catch(e:Exception) {
            tvSerialNumber.text = e.message
        }

        val i: Intent = getIntent()
        _iServerChange = i.getIntExtra("iServerChange", 0)
        var sHost: String? = i.getStringExtra("server")
        setTitle(R.string.act_server_connect)
        if (g.iPriceChecker == 0) {
            setContentView(R.layout.activity_main)
        } else {
            setContentView(R.layout.activity_main_price_checker)
            _tvMsgError = findViewById<View>(R.id.IDC_SERVER_SERVERCONNECT_TEXT_MSG_ERROR) as TextView?
        }
        _etHost = findViewById<View>(R.id.IDC_SERVER_SERVERCONNECT_EDIT_HOST) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_SERVER_SERVERCONNECT_TEXT_MSG) as TextView?
        if (_iServerChange != 0 || sHost.isNullOrEmpty()) {
            try {
                openFileInput(Server.Companion.SERVERNAME).use { fis ->
                    InputStreamReader(fis, "UTF-8").use { isr ->
                        BufferedReader(isr).use { br ->
                            sHost = br.readLine()
                            if (sHost.isNullOrEmpty()) {
                                if (g.iPriceChecker == 0) _iServerChange = 1 else sHost = getResources().getString(R.string.act_server_address)
                            }
                        }
                    }
                }
            } catch (e: Exception) { if (g.iPriceChecker == 0) _iServerChange = 1 else sHost = getResources().getString(R.string.act_server_address) }
        }
        if (_iServerChange == 0) {
            _etHost!!.setText(sHost)
            val btnOK: Button = findViewById<View>(R.id.IDOK) as Button
            btnOK.performClick()
        } else {
            val imm: InputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            val hObj: Handler = Handler(Looper.getMainLooper())
            c.PrismReqFocus(hObj, _etHost, imm)
            _etHost!!.setOnKeyListener(object : View.OnKeyListener {
                override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
                    if (e.action == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
                        val btnOK: Button = findViewById<View>(R.id.IDOK) as Button
                        btnOK.performClick()
                        return true
                    }
                    return false
                }
            })
            /*
            _etHost!!.setOnKeyListener { v: View?, k: Int, e: KeyEvent ->
                if (e.action == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
                    val btnOK = findViewById<View>(R.id.IDOK) as Button
                    btnOK.performClick()
                    return@setOnKeyListener true
                }
                return@setOnKeyListener false
            }
            */
        }
    }

    private fun getSerialNumber() {
        val doc = readXml()
        serialNumber = getAttributeValuesByAttributeNameAndAttributeValue(
            doc, "CC610",
            "Device", "Serial"
        )
    }

    fun readXml(): Document {
        val xmlFile = File("/storage/emulated/0/Stats", "MxStats.xml")

        val dbFactory = DocumentBuilderFactory.newInstance()
        val dBuilder = dbFactory.newDocumentBuilder()
        val xmlInput = InputSource(StringReader(xmlFile.readText()))
        val doc = dBuilder.parse(xmlInput)

        return doc
    }

    fun getAttributeValuesByAttributeNameAndAttributeValue(doc: Document, attributeValue: String,
                                                           attributeName: String, attributeValueName: String): String {
        val xpFactory = XPathFactory.newInstance()
        val xPath = xpFactory.newXPath()

        // <MXStats End="2024/03/08 10:38:38" Start="2024/02/28 11:39:11" Device="CC610" Serial="22348524301423">
        val xpath = "/MXStats[contains(@$attributeName, '$attributeValue')]"

        val items = xPath.evaluate(xpath, doc, XPathConstants.NODESET) as NodeList
        val attributeValue = items.item(0).attributes.getNamedItem(attributeValueName)
        val value = attributeValue.nodeValue
//    return xpath.toString()
        return value
    }

    fun btnIDOK(view: View?) {
        val sHost: String = _etHost!!.getText().toString().trim { it <= ' ' }
        if (sHost.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_host_must_be_entered)
        } else {
            _tvMsg!!.setText(R.string.msg_connecting)
            val hObj: Handler = Handler(Looper.getMainLooper())
            s.Connect(sHost, this, hObj, _tvMsg)
        }
    }

    fun Connected() {
        _tvMsg!!.setText(R.string.msg_connected)
        try {
            openFileOutput(Server.Companion.SERVERNAME, MODE_PRIVATE).use { fos ->
                val sHost: String = _etHost!!.getText().toString().trim { it <= ' ' }
                fos.write(sHost.toByteArray(charset("UTF-8")))
            }
        } catch (ignored: Exception) {}
        setDataWedgeConfiguration()
        val intent: Intent
        if (g.iPriceChecker == 0) {
            intent = Intent(this, LogonActivity::class.java)
        } else {
            _tvMsgError!!.text = ""
            intent = Intent(this, PriceCheckerActivity::class.java)
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK) //FLAG_ACTIVITY_CLEAR_TOP
        if (_iServerChange <= 1) Arrays.fill(g.REC_Logon, 0.toByte())
        startActivity(intent)
        finish()
    }

    fun CannotConnect(msg: String?) {
        var sMsg: String?
        if (g.iPriceChecker == 0) {
            sMsg = msg
        } else {
            val i = msg!!.indexOf(resources.getString(R.string.msg_connect_failed),0,true)
            sMsg = resources.getString(R.string.msg_server_send_msg_error) + " " + (if (i < 0) msg else msg.substring(i));
            _tvMsgError!!.setText(sMsg)
            sMsg = resources.getString(R.string.msg_server_send_msg);
        }
        _tvMsg!!.text = sMsg
        s.Disconnect()
    }

    fun btnIDCANCEL(view: View?) {
        doExit()
    }

    public override fun onKeyDown(keyCode: Int,event: KeyEvent): Boolean { //onBackPressed() api lvl 5+
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            doExit()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun doExit() {
        if ((_iServerChange != 2) || (!s.Connected())) {
            s.Disconnect()
        } else { //Called by R.id.IDC_RADIO_MAINMENU_MAINMENU_SERVERCONNECT in MainMenuActivity and there was no connection attempt. Get back.
            val intent: Intent = Intent(this, MainMenuActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK) //FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
        }
        finish()
    }

    private fun setDataWedgeConfiguration() { //https://techdocs.zebra.com/datawedge/7-3/guide/api/setconfig/
        val bMain: Bundle = Bundle()
        val bConfigIntent: Bundle = Bundle()
        val bParamsIntent: Bundle = Bundle()
        val bConfigKeystroke: Bundle = Bundle()
        val bParamsKeystroke: Bundle = Bundle()
        val bConfigBarcode: Bundle = Bundle()
        val bParamsBarcode: Bundle = Bundle()
        val bApp: Bundle = Bundle()
        val bConfigs: ArrayList<Bundle> = ArrayList()
        bApp.putString("PACKAGE_NAME", getPackageName())
        bApp.putStringArray("ACTIVITY_LIST", arrayOf("*"))
        bParamsIntent.putString("intent_output_enabled", "true")
        bParamsIntent.putString("intent_action", "com.prismkt.udi")
        bParamsIntent.putString("intent_category", "zebra.intent.prismkt.UDI")
        bParamsIntent.putInt("intent_delivery", 2) //Use "0" for Start Activity, "1" for Start Service, "2" for Broadcast
        bConfigIntent.putString("PLUGIN_NAME", "INTENT")
        bConfigIntent.putString("RESET_CONFIG", "false")
        bConfigIntent.putBundle("PARAM_LIST", bParamsIntent)
        bParamsKeystroke.putString("keystroke_output_enabled", "false")
        bConfigKeystroke.putString("PLUGIN_NAME", "KEYSTROKE")
        bConfigKeystroke.putString("RESET_CONFIG", "false")
        bConfigKeystroke.putBundle("PARAM_LIST", bParamsKeystroke)
        bParamsBarcode.putString("scanner_selection", "auto")
        bParamsBarcode.putString("scanner_input_enabled", "true")
        bParamsBarcode.putString("decoder_qrcode", "false")
        bParamsBarcode.putString("decoder_upce0", "true")
        bParamsBarcode.putString("decoder_upce0_report_check_digit", "true")
        bConfigBarcode.putString("PLUGIN_NAME", "BARCODE")
        bConfigBarcode.putString("RESET_CONFIG", "false")
        bConfigBarcode.putBundle("PARAM_LIST", bParamsBarcode)
        bConfigs.add(bConfigBarcode)
        bConfigs.add(bConfigKeystroke)
        bConfigs.add(bConfigIntent)
        bMain.putString("PROFILE_NAME", "Dollar Tree Price Checker")
        bMain.putString("PROFILE_ENABLED", "true")
        bMain.putString("CONFIG_MODE", "CREATE_IF_NOT_EXIST")
        bMain.putParcelableArrayList("PLUGIN_CONFIG", bConfigs)
        bMain.putParcelableArray("APP_LIST", arrayOf(bApp))
        val i: Intent = Intent()
        i.setAction("com.symbol.datawedge.api.ACTION")
        i.putExtra("com.symbol.datawedge.api.SET_CONFIG", bMain)
        sendBroadcast(i)
        g.sFilterCategory = getResources().getString(R.string.activity_intent_filter_category)
        g.sFilterAction = getResources().getString(R.string.activity_intent_filter_action)
        g.sScanData = getResources().getString(R.string.datawedge_intent_key_data)
    }
}