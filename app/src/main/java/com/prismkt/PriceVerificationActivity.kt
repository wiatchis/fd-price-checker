package com.prismkt

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*

/*
com.prism.g
import androidx.constraintlayout.widget.ConstraintLayout
import com.prism.Common.IMMResult
import android.os.Bundle
import com.prism.Server.RecvThread
import com.prism.Server.Tick
import androidx.appcompat.app.AppCompatActivity
import android.os.Looper
import com.prism.CouponRecyclerViewAdapter.ItemClickListener
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.constraintlayout.widget.ConstraintSet
import com.prism.RcvHeaderActivity.rcvAction
import com.prism.RtvHeaderActivity.rtvAction
import com.prism.ClrScanBolActivity.csbAction
import com.prism.StockCountActivity.scaAction
import com.prism.TrfoutHeaderActivity.troAction
import com.prism.DamagesReportActivity.draAction
import android.view.LayoutInflater
import android.view.ViewGroup
import android.app.Activity
*/
class PriceVerificationActivity constructor() : AppCompatActivity(), View.OnKeyListener {
    private val c: Common = Common.Companion.instance
    private val s: Server = Server.Companion.instance
    private var _hObj: Handler? = null
    private var _etItem: EditText? = null
    private var _tvSKU: TextView? = null
    private var _tvDesc: TextView? = null
    private var _tvPrice: TextView? = null
    private var _etNewPrice: EditText? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnOK: Button? = null
    private var _sItem: String? = null
    private var _sNewPrice: String? = null
    private var _sPriceVerificationSKU: String? = null
    private var _sPriceVerificationNewPrice: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_price_verification)
        _etItem = findViewById<View>(R.id.IDC_PRICING_PRICEVER_EDIT_ITEM) as EditText?
        _etNewPrice = findViewById<View>(R.id.IDC_PRICING_PRICEVER_EDIT_NEWPRICE) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_PRICING_PRICEVER_TEXT_MSG) as TextView?
        _btnOK = findViewById<View>(R.id.IDOK) as Button?
        if (g.REC_Security.get(g.iRec_Security_PriceVerify) != 0x30.toByte()) {
            _hObj = Handler(Looper.getMainLooper())
            _tvMsg = findViewById<View>(R.id.IDC_PRICING_PRICEVER_TEXT_MSG) as TextView?
            _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
            _tvSKU = findViewById<View>(R.id.IDC_PRICING_PRICEVER_EDIT_SKU) as TextView?
            _tvDesc = findViewById<View>(R.id.IDC_PRICING_PRICEVER_EDIT_DESC) as TextView?
            _tvPrice = findViewById<View>(R.id.IDC_PRICING_PRICEVER_EDIT_PRICE) as TextView?
            _sPriceVerificationSKU = ""
            _sPriceVerificationNewPrice = ""
            _etItem!!.setOnKeyListener(this)
            _etNewPrice!!.setOnKeyListener(this)
            _etItem!!.requestFocus()
        } else {
            _tvMsg!!.setText(R.string.msg_not_authorized)
            _etItem!!.setEnabled(false)
            _etNewPrice!!.setEnabled(false)
            _btnOK!!.setEnabled(false)
            val btn: Button = findViewById<View>(R.id.IDCANCEL) as Button
            btn.requestFocus()
        }
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(
            this,
            R.id.clPriceVerification,
            !_etItem!!.getText().toString().trim({ it <= ' ' }).isEmpty()
        )
        val filter: IntentFilter = IntentFilter()
        filter.addCategory(g.sFilterCategory)
        filter.addAction(g.sFilterAction)
        registerReceiver(scanBroadcastReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clPriceVerification)
        unregisterReceiver(scanBroadcastReceiver)
    }

    public override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
        if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
            _btnOK!!.performClick()
            return true
        }
        return false
    }

    fun btnIDOK(view: View?) {
        _sItem = _etItem!!.getText().toString().trim({ it <= ' ' })
        if (_sItem!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_item_must_be_entered)
        } else {
            _sNewPrice = _etNewPrice!!.getText().toString().trim({ it <= ' ' })
            executeAction()
        }
    }

    fun btnIDCANCEL(view: View?) {
        onBackPressed()
    }

    private fun executeAction() {
        Thread(object : Runnable {
            public override fun run() {
                var sMsg: String?
                if (_sItem != _sPriceVerificationSKU) {
                    Arrays.fill(g.REC_InvMst, 0.toByte())
                    c.PrismCopyString(g.REC_InvMst,g.iRecordId,g.sRecordId,"InvMst210") //DbFunction 4; Format 79; GetItemDetail; PmWinDbm.app (11).
                    c.PrismCopyString(g.REC_InvMst, g.iRec_InvMst_SKU, g.sRec_InvMst_SKU, _sItem)
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_getting_item)
                    if (s.ServerSend(g.REC_InvMst, _hObj, _tvMsg, _pbAct)) {
                        if (g.REC_InvMst.get(g.iRec_InvMst_ReadStatus) == 0x31.toByte()) {
                            val sbSku: StringBuilder = StringBuilder()
                            _sPriceVerificationSKU = c.PrismGetItemSku(g.REC_InvMst,g.iRec_InvMst_SKU,g.sRec_InvMst_SKU,sbSku)
                            c.PrismMsg(_hObj, _etItem, _sPriceVerificationSKU)
                            sMsg = sbSku.toString()
                            c.PrismMsg(_hObj, _tvSKU, sMsg)
                            sMsg = c.PrismGetDescString()
                            c.PrismMsg(_hObj, _tvDesc, sMsg)
                            sMsg = c.PrismGetString(g.REC_InvMst,g.iRec_InvMst_Price,g.sRec_InvMst_Price)
                            c.PrismMsg(_hObj, _tvPrice, sMsg)
                            _sPriceVerificationNewPrice = ""
                            c.PrismMsg(_hObj, _etNewPrice, "")
                            c.PrismMsg(_hObj, _tvMsg, R.string.msg_got_item)
                        } else {
                            _sPriceVerificationSKU = ""
                            c.PrismMsg(_hObj, _tvSKU, "")
                            c.PrismMsg(_hObj, _tvDesc, "")
                            c.PrismMsg(_hObj, _tvPrice, "")
                            c.PrismMsg(_hObj, _etNewPrice, "")
                            sMsg = c.PrismGetString(g.REC_InvMst,g.iRec_InvMst_ErrorString,g.sRec_InvMst_ErrorString)
                            c.PrismMsg(_hObj, _tvMsg, sMsg)
                        }
                    }
                    return
                }
                if ((_sNewPrice != _sPriceVerificationNewPrice) && (g.REC_InvMst.get(g.iRec_InvMst_ReadStatus) == 0x31.toByte())) {
                    Arrays.fill(g.REC_PriceVar, 0.toByte())
                    c.PrismCopyString(g.REC_PriceVar,g.iRecordId,g.sRecordId,"PrcVar210") //DbFunction 8; Format 82; PDTPriceVariance; PmWinDbm.app (10); Adds to PdPrcVar.tps
                    c.PrismCopyString(g.REC_PriceVar,g.iRec_PriceVar_FloorPrice,g.sRec_PriceVar_FloorPrice,_sNewPrice)
                    System.arraycopy(g.REC_InvMst,g.iRec_InvMst_SKU,g.REC_PriceVar,g.iRec_PriceVar_SKU,g.sRec_InvMst_SKU)
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_sending_new_price)
                    if (s.ServerSend(g.REC_PriceVar, _hObj, _tvMsg, _pbAct)) {
                        if (g.REC_PriceVar.get(g.iRec_PriceVar_ReadStatus) == 0x31.toByte()) {
                            _sPriceVerificationNewPrice = c.PrismGetString(g.REC_PriceVar,g.iRec_PriceVar_FloorPrice,g.sRec_PriceVar_FloorPrice)
                            c.PrismMsg(_hObj, _etNewPrice, _sPriceVerificationNewPrice)
                            c.PrismMsg(_hObj, _tvMsg, R.string.msg_added_new_price)
                        } else {
                            sMsg = c.PrismGetString(g.REC_PriceVar,g.iRec_PriceVar_ErrorString,g.sRec_PriceVar_ErrorString)
                            c.PrismMsg(_hObj, _tvMsg, sMsg)
                        }
                    }
                    return
                }
                c.PrismMsg(_hObj, _tvMsg, "")
                _hObj!!.post(object : Runnable {
                    public override fun run() {
                        synchronized(
                            Server.Companion._syncToken,
                            { if (_etItem!!.isFocused()) _etNewPrice!!.requestFocus() else _etItem!!.requestFocus() })
                    }
                })
            }
        }).start()
    }

    private val scanBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        public override fun onReceive(context: Context, intent: Intent) {
            if ((intent.getAction() == g.sFilterAction)) {
                try {
                    displayScanResult(intent)
                } catch (ignored: Exception) {
                }
            }
        }
    }

    private fun displayScanResult(i: Intent) {
        _etItem!!.setText(i.getStringExtra(g.sScanData))
        _btnOK!!.performClick()
    }
}