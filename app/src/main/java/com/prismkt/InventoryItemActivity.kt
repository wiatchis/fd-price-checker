package com.prismkt
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class InventoryItemActivity constructor() : AppCompatActivity() {
    private val c: Common = Common.Companion.instance
    private val s: Server = Server.Companion.instance
    private var _hObj: Handler? = null
    private var _etItem: EditText? = null
    private var _tvSKU: TextView? = null
    private var _tvDesc: TextView? = null
    private var _tvPrice: TextView? = null
    private var _tvOnhandHoldOrd: TextView? = null
    private var _tvBuySellCase: TextView? = null
    private var _tvDeptGroup: TextView? = null
    private var _tvStyleSizeGroup: TextView? = null
    private var _tvVenManPartNo: TextView? = null
    private var _tvVendorName: TextView? = null
    private var _tvStyleVendor: TextView? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnOK: Button? = null
    private var _sItem: String? = null
    private var _sInventoryItemSKU: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inventory_item)
        _etItem = findViewById<View>(R.id.IDC_INVENTORY_INVENTORYITEM_EDIT_ITEM) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_INVENTORY_INVENTORYITEM_TEXT_MSG) as TextView?
        _btnOK = findViewById<View>(R.id.IDOK) as Button?
        if (g.REC_Security.get(g.iRec_Security_Inventory) != 0x30.toByte()) {
            _hObj = Handler(Looper.getMainLooper())
            _tvSKU = findViewById<View>(R.id.IDC_INVENTORY_INVENTORYITEM_EDIT_SKU) as TextView?
            _tvDesc = findViewById<View>(R.id.IDC_INVENTORY_INVENTORYITEM_EDIT_DESC) as TextView?
            _tvPrice = findViewById<View>(R.id.IDC_INVENTORY_INVENTORYITEM_EDIT_PRICE) as TextView?
            _tvOnhandHoldOrd =
                findViewById<View>(R.id.IDC_INVENTORY_INVENTORYITEM_EDIT_ONHANDHOLDORDER) as TextView?
            _tvBuySellCase =
                findViewById<View>(R.id.IDC_INVENTORY_INVENTORYITEM_EDIT_BUYSELLUNITSCASE) as TextView?
            _tvDeptGroup =
                findViewById<View>(R.id.IDC_INVENTORY_INVENTORYITEM_EDIT_DEPTGROUP) as TextView?
            _tvStyleSizeGroup =
                findViewById<View>(R.id.IDC_INVENTORY_INVENTORYITEM_EDIT_STYLESIZEGROUPS) as TextView?
            _tvVenManPartNo =
                findViewById<View>(R.id.IDC_INVENTORY_INVENTORYITEM_EDIT_VENMANPARTNO) as TextView?
            _tvVendorName =
                findViewById<View>(R.id.IDC_INVENTORY_INVENTORYITEM_EDIT_VENDORNAME) as TextView?
            _tvStyleVendor =
                findViewById<View>(R.id.IDC_INVENTORY_INVENTORYITEM_EDIT_STYLEVENDORNAME) as TextView?
            _tvMsg = findViewById<View>(R.id.IDC_INVENTORY_INVENTORYITEM_TEXT_MSG) as TextView?
            _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
            _sInventoryItemSKU = ""
            _etItem!!.requestFocus()
            _etItem!!.setOnKeyListener(object : View.OnKeyListener {
                public override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
                    if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
                        _btnOK!!.performClick()
                        return true
                    }
                    return false
                }
            })
        } else {
            _tvMsg!!.setText(R.string.msg_not_authorized)
            _etItem!!.setEnabled(false)
            _btnOK!!.setEnabled(false)
            val btn: Button = findViewById<View>(R.id.IDCANCEL) as Button
            btn.requestFocus()
        }
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(
            this,
            R.id.clInventoryItem,
            !_etItem!!.getText().toString().trim({ it <= ' ' }).isEmpty()
        )
        val filter: IntentFilter = IntentFilter()
        filter.addCategory(g.sFilterCategory)
        filter.addAction(g.sFilterAction)
        registerReceiver(scanBroadcastReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clInventoryItem)
        unregisterReceiver(scanBroadcastReceiver)
    }

    fun btnIDOK(view: View?) {
        _sItem = _etItem!!.getText().toString().trim({ it <= ' ' })
        if (_sItem!!.isEmpty()) _tvMsg!!.setText(R.string.msg_item_must_be_entered) else executeAction()
    }

    fun btnIDCANCEL(view: View?) {
        onBackPressed()
    }

    private fun executeAction() {
        Thread(object : Runnable {
            public override fun run() {
                var sMsg: String?
                if (!(_sItem == _sInventoryItemSKU)) {
                    Arrays.fill(g.REC_InvInq, 0.toByte())
                    c.PrismCopyString(
                        g.REC_InvInq,
                        g.iRecordId,
                        g.sRecordId,
                        "InvInq4425"
                    ) //DbFunction 20; Format 105; ExtendedGetItem; PmWinDbm.app (13);
                    c.PrismCopyString(g.REC_InvInq, g.iRec_InvInq_SKU, g.sRec_InvInq_SKU, _sItem)
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_getting_item)
                    if (s.ServerSend(g.REC_InvInq, _hObj, _tvMsg, _pbAct)) {
                        if (g.REC_InvInq.get(g.iRec_InvInq_ReadStatus) == 0x31.toByte()) {
                            val sbSku: StringBuilder = StringBuilder()
                            _sInventoryItemSKU = c.PrismGetItemSku(
                                g.REC_InvInq,
                                g.iRec_InvInq_SKU,
                                g.sRec_InvInq_SKU,
                                sbSku
                            )
                            c.PrismMsg(_hObj, _etItem, _sInventoryItemSKU)
                            sMsg = sbSku.toString()
                            c.PrismMsg(_hObj, _tvSKU, sMsg)
                            System.arraycopy(
                                g.REC_InvInq,
                                g.iRec_InvInq_Description1,
                                g.REC_InvMst,
                                g.iRec_InvMst_Description1,
                                g.sRec_InvMst_Description1
                            )
                            System.arraycopy(
                                g.REC_InvInq,
                                g.iRec_InvInq_Description2,
                                g.REC_InvMst,
                                g.iRec_InvMst_Description2,
                                g.sRec_InvMst_Description2
                            )
                            sMsg = c.PrismGetDescString()
                            c.PrismMsg(_hObj, _tvDesc, sMsg)
                            sMsg = c.PrismGetString(
                                g.REC_InvInq,
                                g.iRec_InvInq_Price,
                                g.sRec_InvInq_Price
                            )
                            c.PrismMsg(_hObj, _tvPrice, sMsg)
                            sMsg = (c.PrismGetString(
                                g.REC_InvInq,
                                g.iRec_InvInq_OnHand,
                                g.sRec_InvInq_OnHand
                            ) + ";" +
                                    c.PrismGetString(
                                        g.REC_InvInq,
                                        g.iRec_InvInq_OnHold,
                                        g.sRec_InvInq_OnHold
                                    ) + ";" +
                                    c.PrismGetString(
                                        g.REC_InvInq,
                                        g.iRec_InvInq_OnOrder,
                                        g.sRec_InvInq_OnOrder
                                    ))
                            c.PrismMsg(_hObj, _tvOnhandHoldOrd, sMsg)
                            sMsg = (c.PrismGetString(
                                g.REC_InvInq,
                                g.iRec_InvInq_Buy_Unit,
                                g.sRec_InvInq_Buy_Unit
                            ) + ";" +
                                    c.PrismGetString(
                                        g.REC_InvInq,
                                        g.iRec_InvInq_Sell_Unit,
                                        g.sRec_InvInq_Sell_Unit
                                    ) + ";" +
                                    c.PrismGetString(
                                        g.REC_InvInq,
                                        g.iRec_InvInq_Std_Case,
                                        g.sRec_InvInq_Std_Case
                                    ))
                            c.PrismMsg(_hObj, _tvBuySellCase, sMsg)
                            sMsg = (c.PrismGetString(
                                g.REC_InvInq,
                                g.iRec_InvInq_Department,
                                g.sRec_InvInq_Department
                            ) + ";" +
                                    c.PrismGetString(
                                        g.REC_InvInq,
                                        g.iRec_InvInq_Sub_Dept,
                                        g.sRec_InvInq_Sub_Dept
                                    ) + ";" +
                                    c.PrismGetString(
                                        g.REC_InvInq,
                                        g.iRec_InvInq_Class,
                                        g.sRec_InvInq_Class
                                    ) + ";" +
                                    c.PrismGetString(
                                        g.REC_InvInq,
                                        g.iRec_InvInq_Sub_Class,
                                        g.sRec_InvInq_Sub_Class
                                    ))
                            c.PrismMsg(_hObj, _tvDeptGroup, sMsg)
                            sMsg = (c.PrismGetString(
                                g.REC_InvInq,
                                g.iRec_InvInq_Style,
                                g.sRec_InvInq_Style
                            ) + ";" +
                                    c.PrismGetString(
                                        g.REC_InvInq,
                                        g.iRec_InvInq_Color,
                                        g.sRec_InvInq_Color
                                    ) + ";" +
                                    c.PrismGetString(
                                        g.REC_InvInq,
                                        g.iRec_InvInq_Size,
                                        g.sRec_InvInq_Size
                                    ) + ";" +
                                    c.PrismGetString(
                                        g.REC_InvInq,
                                        g.iRec_InvInq_Dimension,
                                        g.sRec_InvInq_Dimension
                                    ))
                            c.PrismMsg(_hObj, _tvStyleSizeGroup, sMsg)
                            sMsg = (c.PrismGetString(
                                g.REC_InvInq,
                                g.iRec_InvInq_VendorPart,
                                g.sRec_InvInq_Vendor
                            ) + ";" +
                                    c.PrismGetString(
                                        g.REC_InvInq,
                                        g.iRec_InvInq_ManPartNo,
                                        g.sRec_InvInq_ManPartNo
                                    ))
                            c.PrismMsg(_hObj, _tvVenManPartNo, sMsg)
                            sMsg = (c.PrismGetString(
                                g.REC_InvInq,
                                g.iRec_InvInq_Vendor,
                                g.sRec_InvInq_Vendor
                            ) + ";" +
                                    c.PrismGetString(
                                        g.REC_InvInq,
                                        g.iRec_InvInq_VendorName,
                                        g.sRec_InvInq_VendorName
                                    ))
                            c.PrismMsg(_hObj, _tvVendorName, sMsg)
                            sMsg = (c.PrismGetString(
                                g.REC_InvInq,
                                g.iRec_InvInq_StyleVendor,
                                g.sRec_InvInq_StyleVendor
                            ) + ";" +
                                    c.PrismGetString(
                                        g.REC_InvInq,
                                        g.iRec_InvInq_StyleVendorName,
                                        g.sRec_InvInq_StyleVendorName
                                    ))
                            c.PrismMsg(_hObj, _tvStyleVendor, sMsg)
                            c.PrismMsg(_hObj, _tvMsg, R.string.msg_got_item)
                        } else {
                            _sInventoryItemSKU = ""
                            c.PrismMsg(_hObj, _tvSKU, "")
                            c.PrismMsg(_hObj, _tvDesc, "")
                            c.PrismMsg(_hObj, _tvPrice, "")
                            c.PrismMsg(_hObj, _tvOnhandHoldOrd, "")
                            c.PrismMsg(_hObj, _tvBuySellCase, "")
                            c.PrismMsg(_hObj, _tvDeptGroup, "")
                            c.PrismMsg(_hObj, _tvStyleSizeGroup, "")
                            c.PrismMsg(_hObj, _tvVenManPartNo, "")
                            c.PrismMsg(_hObj, _tvVendorName, "")
                            c.PrismMsg(_hObj, _tvStyleVendor, "")
                            sMsg = c.PrismGetString(
                                g.REC_InvInq,
                                g.iRec_InvInq_ErrorString,
                                g.sRec_InvInq_ErrorString
                            )
                            c.PrismMsg(_hObj, _tvMsg, sMsg)
                        }
                    }
                }
            }
        }).start()
    }

    private val scanBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        public override fun onReceive(context: Context, intent: Intent) {
            if ((intent.getAction() == g.sFilterAction)) {
                try {
                    displayScanResult(intent)
                } catch (ignored: Exception) {
                }
            }
        }
    }

    private fun displayScanResult(i: Intent) {
        _etItem!!.setText(i.getStringExtra(g.sScanData))
        _btnOK!!.performClick()
    }
}