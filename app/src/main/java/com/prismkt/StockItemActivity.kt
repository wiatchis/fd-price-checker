package com.prismkt

import android.content.*
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class StockItemActivity : AppCompatActivity(), View.OnKeyListener {
    private val c: Common = Common.instance
    private val s: Server = Server.instance
    private var _hObj: Handler? = null
    private var _etItem: EditText? = null
    private var _tvSKU: TextView? = null
    private var _tvDesc: TextView? = null
    private var _tvOhQty: TextView? = null
    private var _tvTotal: TextView? = null
    private var _etQty: EditText? = null
    private lateinit var _rbArray: Array<RadioButton?>
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnOK: Button? = null
    private var _sItem: String? = null
    private var _sQty: String? = null
    private var _sStockItemNew: String? = null
    private var _sStockItemQty: String? = null
    private var _iStockItemScanFlag: Int = 0
    private var _iR: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stock_item)
        _hObj = Handler(Looper.getMainLooper())
        _etItem = findViewById<View>(R.id.IDC_STOCK_STOCKITEM_EDIT_ITEM) as EditText?
        _tvSKU = findViewById<View>(R.id.IDC_STOCK_STOCKITEM_EDIT_SKU) as TextView?
        _tvDesc = findViewById<View>(R.id.IDC_STOCK_STOCKITEM_EDIT_DESC) as TextView?
        _tvOhQty = findViewById<View>(R.id.IDC_STOCK_STOCKITEM_EDIT_OHQTY) as TextView?
        _tvTotal = findViewById<View>(R.id.IDC_STOCK_STOCKITEM_EDIT_TOTAL) as TextView?
        _etQty = findViewById<View>(R.id.IDC_STOCK_STOCKITEM_EDIT_QTY) as EditText?
        _rbArray = arrayOfNulls(2)
        _rbArray[0] = findViewById<View>(R.id.IDC_STOCK_STOCKITEM_RADIO_COUNT) as RadioButton?
        _rbArray[1] = findViewById<View>(R.id.IDC_STOCK_STOCKITEM_RADIO_GET) as RadioButton?
        _tvMsg = findViewById<View>(R.id.IDC_STOCK_STOCKITEM_TEXT_MSG) as TextView?
        _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
        _btnOK = findViewById<View>(R.id.IDOK) as Button?
        _sStockItemNew = ""
        _sStockItemQty = ""
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setTitle(R.string.act_stock_count_items_land)
        }
        setTitle(
            getTitle().toString() + " " + c.PrismGetString(
                g.REC_StockCnt,
                g.iRec_StockCnt_CountId,
                g.sRec_StockCnt_CountId
            )
        )
        _etItem!!.setOnKeyListener(this)
        _etQty!!.setOnKeyListener(this)
        _etItem!!.requestFocus()
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(
            this,
            R.id.clStockItem,
            !_etItem!!.getText().toString().trim({ it <= ' ' }).isEmpty()
        )
        if (_iStockItemScanRadio >= 0 && _iStockItemScanRadio < _rbArray.size) {
            _rbArray.get(_iStockItemScanRadio)!!.setChecked(true)
        }
        val filter: IntentFilter = IntentFilter()
        filter.addCategory(g.sFilterCategory)
        filter.addAction(g.sFilterAction)
        registerReceiver(scanBroadcastReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clStockItem)
        unregisterReceiver(scanBroadcastReceiver)
    }

    public override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
        if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
            _btnOK!!.performClick()
            return true
        }
        return false
    }

    fun rbClicked(view: View?) {
        _iStockItemScanRadio = c.PrismGetMenuItem(_rbArray)
    }

    fun btnIDOK(view: View?) {
        _sItem = _etItem!!.getText().toString().trim({ it <= ' ' })
        if (_sItem!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_item_must_be_entered)
        } else {
            _iR = 0
            _sQty = _etQty!!.getText().toString().trim({ it <= ' ' })
            if (((_sStockItemNew == _sItem) &&
                        !(_sStockItemQty == _sQty) && (
                        g.REC_StockDtl.get(g.iRec_StockDtl_ReadStatus) == 0x31.toByte()) && (
                        g.REC_StockDtl.get(g.iRec_StockDtl_SKU) != 0x00.toByte()))
            ) {
                /* 05/10/07. If a user scans an item, which is counted earlier on the sheet then ask:
                  "Do you wish to add to existing count Y/N?".
                  If the user replies 'Y' then add to the previous count.
                  If the user replies 'N' then replace the previous count.
                  Added (REC_StockDtl.Count[0] != '\0') and MessageBox to the logical expression below.
                */
                if ((g.REC_StockDtl.get(g.iRec_StockDtl_Count) != 0x00.toByte()) && (
                            _sQty!!.get(0) >= '0') && (_sQty!!.get(0) <= '9')
                ) {
                    val alertDialog: AlertDialog = AlertDialog.Builder(this).create()
                    alertDialog.setTitle(R.string.dlg_title_prism)
                    alertDialog.setMessage(getResources().getString(R.string.dlg_question_add_count))
                    alertDialog.setButton(
                        AlertDialog.BUTTON_POSITIVE,
                        getResources().getString(R.string.dlg_answer_no),
                        object : DialogInterface.OnClickListener {
                            public override fun onClick(arg0: DialogInterface, arg1: Int) {
                                _iR = 1
                                g.REC_StockDtl[g.iRec_StockDtl_Quantity] = 0x52 //'R'
                                executeAction()
                            }
                        })
                    alertDialog.setButton(
                        AlertDialog.BUTTON_NEGATIVE,
                        getResources().getString(R.string.dlg_answer_yes),
                        object : DialogInterface.OnClickListener {
                            public override fun onClick(arg0: DialogInterface, arg1: Int) {
                                executeAction()
                            }
                        })
                    alertDialog.show()
                } else {
                    executeAction()
                }
            } else {
                executeAction()
            }
        }
    }

    fun btnIDCANCEL(view: View?) {
        onBackPressed()
    }

    public override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed()
            return true
        }
        return (super.onOptionsItemSelected(item))
    }

    private fun executeAction() {
        Thread(ExecuteThread()).start()
    }

    private val scanBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        public override fun onReceive(context: Context, intent: Intent) {
            if ((intent.getAction() == g.sFilterAction)) {
                try {
                    displayScanResult(intent)
                } catch (ignored: Exception) {}
            }
        }
    }

    private fun displayScanResult(i: Intent) {
        _etItem!!.setText(i.getStringExtra(g.sScanData))
        _sStockItemNew = ""
        _iStockItemScanFlag = 1
        _btnOK!!.performClick()
    }

    internal inner class ExecuteThread constructor() : Runnable {
        var _bQty: Boolean = false
        public override fun run() {
            var sMsg: String?
            var iMsg: Int
            if (!(_sItem == _sStockItemNew)) {
                Arrays.fill(g.REC_StockDtl, 0.toByte())
                c.PrismCopyString(
                    g.REC_StockDtl,
                    g.iRecordId,
                    g.sRecordId,
                    "StockDtl4426"
                ) //DbFunction 7(36); Format 108; PhyInvDtl; PmWinDbm.app (3); Updates PhiShtDtl.tps and PInvBal.tps
                c.PrismCopyString(g.REC_StockDtl, g.iRec_StockDtl_SKU, g.sRec_StockDtl_SKU, _sItem)
                if ((_iStockItemScanFlag == 1) && (_iStockItemScanRadio == 0)) {
                    g.REC_StockDtl[g.iRec_StockDtl_Quantity] = 0x31 //'1'
                    iMsg = R.string.msg_counting_item
                } else {
                    iMsg = R.string.msg_getting_item
                }
                c.PrismMsg(_hObj, _tvMsg, iMsg)
                if (s.ServerSend(g.REC_StockDtl, _hObj, _tvMsg, _pbAct)) {
                    if (g.REC_StockDtl.get(g.iRec_StockDtl_ReadStatus) == 0x31.toByte()) {
                        val sbSku: StringBuilder = StringBuilder()
                        _sStockItemNew = c.PrismGetItemSku(
                            g.REC_StockDtl,
                            g.iRec_StockDtl_SKU,
                            g.sRec_StockDtl_SKU,
                            sbSku
                        )
                        c.PrismMsg(_hObj, _etItem, _sStockItemNew)
                        sMsg = sbSku.toString()
                        c.PrismMsg(_hObj, _tvSKU, sMsg)
                        System.arraycopy(
                            g.REC_StockDtl,
                            g.iRec_StockDtl_Description1,
                            g.REC_InvMst,
                            g.iRec_InvMst_Description1,
                            g.sRec_InvMst_Description1
                        )
                        System.arraycopy(
                            g.REC_StockDtl,
                            g.iRec_StockDtl_Description2,
                            g.REC_InvMst,
                            g.iRec_InvMst_Description2,
                            g.sRec_InvMst_Description2
                        )
                        sMsg = c.PrismGetDescString()
                        c.PrismMsg(_hObj, _tvDesc, sMsg)
                        sMsg = c.PrismGetString(
                            g.REC_StockDtl,
                            g.iRec_StockDtl_Price,
                            g.sRec_StockDtl_Price
                        )
                        c.PrismMsg(_hObj, _tvOhQty, sMsg)
                        sMsg = c.PrismGetString(
                            g.REC_StockDtl,
                            g.iRec_StockDtl_Count,
                            g.sRec_StockDtl_Count
                        )
                        c.PrismMsg(_hObj, _tvTotal, sMsg)
                        sMsg = c.PrismGetString(
                            g.REC_StockDtl,
                            g.iRec_StockDtl_Quantity,
                            g.sRec_StockDtl_Quantity
                        )
                        c.PrismMsg(_hObj, _etQty, sMsg)
                        if ((_iStockItemScanFlag == 1) && (_iStockItemScanRadio == 0)) {
                            _sStockItemQty = sMsg
                            iMsg = R.string.msg_counted_item
                        } else {
                            _sStockItemQty = ""
                            iMsg = R.string.msg_got_item
                        }
                        if (g.REC_StockDtl.get(g.iRec_StockDtl_ErrorString) != 0x00.toByte()) {
                            if ((_iStockItemScanFlag == 1) && (_iStockItemScanRadio == 0)) {
                                sMsg = getResources().getString(R.string.msg_counted_item)
                            } else {
                                sMsg = getResources().getString(R.string.msg_got_item)
                            }
                            sMsg += (" " + getResources().getString(R.string.msg_with_error) + " " +
                                    c.PrismGetString(
                                        g.REC_StockDtl,
                                        g.iRec_StockDtl_ErrorString,
                                        g.sRec_StockDtl_ErrorString
                                    ))
                            c.PrismMsg(_hObj, _tvMsg, sMsg)
                            c.PrismReqFocus(_hObj, _etItem)
                        } else {
                            c.PrismMsg(_hObj, _tvMsg, iMsg)
                            _bQty = true
                        }
                    } else {
                        c.PrismMsg(_hObj, _tvSKU, "")
                        c.PrismMsg(_hObj, _tvDesc, "")
                        c.PrismMsg(_hObj, _tvOhQty, "")
                        c.PrismMsg(_hObj, _tvTotal, "")
                        c.PrismMsg(_hObj, _etQty, "")
                        _sQty = ""
                        sMsg = c.PrismGetString(
                            g.REC_StockDtl,
                            g.iRec_StockDtl_ErrorString,
                            g.sRec_StockDtl_ErrorString
                        )
                        c.PrismMsg(_hObj, _tvMsg, sMsg)
                        c.PrismReqFocus(_hObj, _etItem)
                    }
                } //if (s.ServerSend(g.REC_StockDtl, _hObj, _tvMsg, _pbAct)) {
                exit()
                return
            } //if (_sItem.equals(_sStockItemNew) == false) {
            if (!(_sStockItemQty == _sQty) &&
                 (g.REC_StockDtl.get(g.iRec_StockDtl_ReadStatus) == 0x31.toByte()) &&
                 (g.REC_StockDtl.get(g.iRec_StockDtl_SKU) != 0x00.toByte()))
            {
                c.PrismCopyString(g.REC_StockDtl,g.iRec_StockDtl_Quantity + _iR,g.sRec_StockDtl_Quantity - _iR, _sQty)
                c.PrismMsg(_hObj, _tvMsg, R.string.msg_updating_qty)
                if (s.ServerSend(g.REC_StockDtl, _hObj, _tvMsg, _pbAct)) {
                    if (g.REC_StockDtl.get(g.iRec_StockDtl_ReadStatus) == 0x31.toByte()) {
                        val sbSku: StringBuilder = StringBuilder()
                        _sStockItemNew = c.PrismGetItemSku(
                            g.REC_StockDtl,
                            g.iRec_StockDtl_SKU,
                            g.sRec_StockDtl_SKU,
                            sbSku
                        )
                        c.PrismMsg(_hObj, _etItem, _sStockItemNew)
                        sMsg = sbSku.toString()
                        c.PrismMsg(_hObj, _tvSKU, sMsg)
                        System.arraycopy(
                            g.REC_StockDtl,
                            g.iRec_StockDtl_Description1,
                            g.REC_InvMst,
                            g.iRec_InvMst_Description1,
                            g.sRec_InvMst_Description1
                        )
                        System.arraycopy(
                            g.REC_StockDtl,
                            g.iRec_StockDtl_Description2,
                            g.REC_InvMst,
                            g.iRec_InvMst_Description2,
                            g.sRec_InvMst_Description2
                        )
                        sMsg = c.PrismGetDescString()
                        c.PrismMsg(_hObj, _tvDesc, sMsg)
                        sMsg = c.PrismGetString(
                            g.REC_StockDtl,
                            g.iRec_StockDtl_Price,
                            g.sRec_StockDtl_Price
                        )
                        c.PrismMsg(_hObj, _tvOhQty, sMsg)
                        sMsg = c.PrismGetString(
                            g.REC_StockDtl,
                            g.iRec_StockDtl_Count,
                            g.sRec_StockDtl_Count
                        )
                        c.PrismMsg(_hObj, _tvTotal, sMsg)
                        sMsg = c.PrismGetString(
                            g.REC_StockDtl,
                            g.iRec_StockDtl_Quantity + _iR,
                            g.sRec_StockDtl_Quantity - _iR
                        )
                        c.PrismMsg(_hObj, _etQty, sMsg)
                        _sStockItemQty = sMsg
                        if (g.REC_StockDtl.get(g.iRec_StockDtl_ErrorString) != 0x00.toByte()) {
                            sMsg = (getResources().getString(R.string.msg_updated_qty) + " " +
                                    getResources().getString(R.string.msg_with_error) + " " +
                                    c.PrismGetString(
                                        g.REC_StockDtl,
                                        g.iRec_StockDtl_ErrorString,
                                        g.sRec_StockDtl_ErrorString
                                    ))
                            c.PrismMsg(_hObj, _tvMsg, sMsg)
                        } else {
                            c.PrismMsg(_hObj, _tvMsg, R.string.msg_updated_qty)
                        }
                        c.PrismReqFocus(_hObj, _etItem)
                    } else {
                        sMsg = c.PrismGetString(
                            g.REC_StockDtl,
                            g.iRec_StockDtl_ErrorString,
                            g.sRec_StockDtl_ErrorString
                        )
                        c.PrismMsg(_hObj, _tvMsg, sMsg)
                        _bQty = true
                    }
                } //if (s.ServerSend(g.REC_TrfOut, _hObj, _tvMsg, _pbAct)) {
                exit()
                return
            } //if (!_sStockItemQty.equals(_sQty) &&...
            c.PrismMsg(_hObj, _tvMsg, "")
        }

        private fun exit() {
            _iStockItemScanFlag = 0
            if (_bQty) {
                val imm: InputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                c.PrismReqFocus(_hObj, _etQty, imm)
            }
        }
    }

    companion object {
        private var _iStockItemScanRadio: Int = 1
    }
}