package com.prismkt

import android.os.Handler
import android.widget.TextView
import java.io.IOException
import java.net.Socket

class Printer private constructor() {
    private val c: Common = Common.Companion.instance
    private var _tvMsg: TextView? = null
    fun Connect(host: String?, port: Int, oActivity: PrintConnectActivity?) {
        if (_socket != null) Disconnect()
        _printerName = host
        _printerPort = port
        val connectThread: Thread = Thread(ConnectThread(oActivity))
        connectThread.start()
    }

    fun Disconnect() {
        if (_socket != null) {
            try {
                _socket!!.close()
            } catch (ignored: IOException) {
            }
            _socket = null
            _iPrinterConnected = 0
        }
    }

    fun PrinterSend(bData: ByteArray?, hObj: Handler?, tvMsg: TextView?): Boolean {
        var ret: Boolean
        try {
            _hObj = hObj
            _tvMsg = tvMsg
            Thread(bData?.let { SendThread(it) }).start()
            ret = true
        } catch (e: Exception) {
            c.PrismMsg(_hObj, _tvMsg, e.getLocalizedMessage())
            ret = false
        }
        return ret
    }

    internal class ConnectThread constructor(private val _oActivity: PrintConnectActivity?) :
        Runnable {
        public override fun run() {
            try {
                _socket = Socket(_printerName, _printerPort)
                _iPrinterConnected = 1
                if (_oActivity != null) {
                    _oActivity.runOnUiThread(object : Runnable {
                        public override fun run() {
                            _oActivity.Connected()
                        }
                    })
                }
            } catch (e: Exception) {
                _iPrinterConnected = 0
                _oActivity!!.runOnUiThread(object : Runnable {
                    public override fun run() {
                        _oActivity.CannotConnect(e.getLocalizedMessage())
                    }
                })
            }
        }
    }

    internal inner class SendThread constructor(bData: ByteArray) : Runnable {
        private val _buffer: ByteArray
        public override fun run() {
            try {
                _socket!!.getOutputStream().write(_buffer)
                _socket!!.getOutputStream().flush()
            } catch (e: IOException) {
                c.PrismMsg(_hObj, _tvMsg, e.getLocalizedMessage())
            }
        }

        init {
            _buffer = ByteArray(bData.size)
            System.arraycopy(bData, 0, _buffer, 0, bData.size)
        }
    }

    companion object {
        val instance: Printer = Printer()
        private var _printerName: String? = null
        private var _printerPort: Int = 6101
        private var _socket: Socket? = null
        private var _hObj: Handler? = null
        val PRINTERPORT: String = "6101"
        val PRINTERNAME: String = "PrismPrinter.txt"
        var _iPrinterConnected: Int = 0
    }
}