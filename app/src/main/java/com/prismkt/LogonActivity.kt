package com.prismkt
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class LogonActivity : AppCompatActivity() {
    private val c: Common = Common.instance
    private val s: Server = Server.instance
    private val p: Printer = Printer.instance
    private var _aObj: LogonActivity? = null
    private var _hObj: Handler? = null
    private var _etUserId: EditText? = null
    private var _etPassword: EditText? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnOK: Button? = null
    private var _sUserId: String? = null
    private var _sPassword: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logon)
        _aObj = this
        _hObj = Handler(Looper.getMainLooper())
        _etUserId = findViewById<View>(R.id.IDC_LOGON_LOGON_EDIT_USER) as EditText?
        _etPassword = findViewById<View>(R.id.IDC_LOGON_LOGON_EDIT_PASSWORD) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_LOGON_LOGON_TEXT_MSG) as TextView?
        _btnOK = findViewById<View>(R.id.IDOK) as Button?
        _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
        if (g.REC_Logon.get(g.iRecordId) != 0x00.toByte()) {
            _sUserId = c.PrismGetString(g.REC_Logon, g.iRec_Logon_UserId, g.sRec_Logon_UserId)
            _etUserId!!.setText(_sUserId, TextView.BufferType.NORMAL)
            _sPassword = c.PrismGetString(g.REC_Logon, g.iRec_Logon_Password, g.sRec_Logon_Password)
            _etPassword!!.setText(_sPassword, TextView.BufferType.NORMAL)
            _btnOK!!.performClick()
        } else {
            val imm: InputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            c.PrismReqFocus(_hObj, _etUserId, imm)
            _etPassword!!.setOnKeyListener(object : View.OnKeyListener {
                override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
                    if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
                        _btnOK!!.performClick()
                        return true
                    }
                    return false
                }
            })
        }
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(this, R.id.clLogon, (g.REC_Logon.get(g.iRecordId) != 0x00.toByte()))
        val filter = IntentFilter()
        filter.addCategory(g.sFilterCategory)
        filter.addAction(g.sFilterAction)
        registerReceiver(scanBroadcastReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clLogon)
        unregisterReceiver(scanBroadcastReceiver)
    }

    fun btnIDOK(view: View?) {
        _sUserId = _etUserId!!.getText().toString().trim({ it <= ' ' })
        if (_sUserId!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_user_must_be_entered)
            return
        }
        _sPassword = _etPassword!!.getText().toString().trim({ it <= ' ' })
        if (_sPassword!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_password_must_be_entered)
            return
        }
        executeAction()
    }

    fun btnIDCANCEL(view: View?) {
        s.Disconnect()
        p.Disconnect()
        onBackPressed()
    }

    public override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean { //onBackPressed() api lvl 5+
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            s.Disconnect()
            p.Disconnect()
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun executeAction() {
        Thread(object : Runnable {
            override fun run() {
                Arrays.fill(g.REC_Logon, 0.toByte())
                c.PrismCopyString(g.REC_Logon, g.iRecordId, g.sRecordId, "Logon4307") //DbFunction 1; Format 95; SecurityCheck; PmWinDbm.app (7).
                c.PrismCopyString(g.REC_Logon, g.iRec_Logon_UserId, g.sRec_Logon_UserId, _sUserId)
                c.PrismCopyString(g.REC_Logon, g.iRec_Logon_Password, g.sRec_Logon_Password, _sPassword)
                c.PrismMsg(_hObj, _tvMsg, "Logging in")
                if (s.ServerSend(g.REC_Logon, _hObj, _tvMsg, _pbAct)) {
                    if (g.REC_Logon.get(g.iRec_Logon_ReadStatus) == 0x31.toByte()) {
                        Arrays.fill(g.REC_PDConfig, 0.toByte())
                        c.PrismCopyString(g.REC_PDConfig, g.iRecordId, g.sRecordId,"PDConfig4206") //DbFunction 10; Format 70; ConfigurePDT; PmWinDbm.app (9); Gets from PdConfig.tps
                        c.PrismMsg(_hObj, _tvMsg, R.string.msg_getting_config)
                        if (s.ServerSend(g.REC_PDConfig, _hObj, _tvMsg, _pbAct)) {
                            if (g.REC_PDConfig.get(g.iRec_PDConfig_ReadStatus) == 0x31.toByte()) {
                                c.PrismMsg(_hObj, _tvMsg, R.string.msg_got_config)
                                Arrays.fill(g.REC_BPrtList, 0.toByte())
                                c.PrismCopyString(g.REC_BPrtList, g.iRecordId, g.sRecordId,"BPrtList4201") //DbFunction 26; Format 94; SendPrinterList; PmPrt.app (7); Gets from BPrinter.tps
                                c.PrismMsg(_hObj, _tvMsg, R.string.msg_getting_printer_id)
                                if (s.ServerSend(g.REC_BPrtList, _hObj, _tvMsg, _pbAct)) {
                                    _hObj!!.post(object : Runnable {
                                        override fun run() {
                                            _aObj!!.LogonSetGlobal()
                                            val intent = Intent(_aObj, MainMenuActivity::class.java)
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK) //FLAG_ACTIVITY_CLEAR_TOP
                                            startActivity(intent)
                                            finish()
                                        }
                                    })
                                }
                            } else {
                                c.PrismMsg(_hObj, _tvMsg, c.PrismGetString(g.REC_PDConfig, g.iRec_PDConfig_ErrorString, g.sRec_PDConfig_ErrorString))
                            }
                        }
                    } else {
                        c.PrismMsg(_hObj, _tvMsg, c.PrismGetString(g.REC_Logon, g.iRec_Logon_ErrorString, g.sRec_Logon_ErrorString))
                        _hObj!!.post(object : Runnable {
                            override fun run() {
                                val imm: InputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
                            }
                        })
                    }
                }
            }
        }).start()
    }

    fun LogonSetGlobal() {
        Arrays.fill(g.REC_Security, 0.toByte())
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security, g.REC_Security, g.iRec_Security_Inventory, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 1, g.REC_Security, g.iRec_Security_Pricing, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 2, g.REC_Security, g.iRec_Security_POS, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 3, g.REC_Security, g.iRec_Security_Utilities, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 4, g.REC_Security, g.iRec_Security_Receiving, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 5, g.REC_Security, g.iRec_Security_RTV, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 6, g.REC_Security, g.iRec_Security_StoreOrders, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 7, g.REC_Security, g.iRec_Security_PhysicalInv, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 8, g.REC_Security, g.iRec_Security_PriceVerify, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 9, g.REC_Security, g.iRec_Security_PrnItem, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 10, g.REC_Security, g.iRec_Security_PrnShelf, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 11, g.REC_Security, g.iRec_Security_NewSale, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 12, g.REC_Security, g.iRec_Security_ResumeSale, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 13, g.REC_Security, g.iRec_Security_DateTime, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 14, g.REC_Security, g.iRec_Security_IPAddress, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 15, g.REC_Security, g.iRec_Security_HostName, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 16, g.REC_Security, g.iRec_Security_UpdProp, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 17, g.REC_Security, g.iRec_Security_VendorLookup, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 18, g.REC_Security, g.iRec_Security_ShipLookup, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 19, g.REC_Security, g.iRec_Security_CreateRcv, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 20, g.REC_Security, g.iRec_Security_AddItemToPO, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 21, g.REC_Security, g.iRec_Security_DOAReceiving, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 22, g.REC_Security, g.iRec_Security_ToleranceOver, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 23, g.REC_Security, g.iRec_Security_TransferOut, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 24, g.REC_Security, g.iRec_Security_ReleaseTrfOut, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 25, g.REC_Security, g.iRec_Security_Printer, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 26, g.REC_Security, g.iRec_Security_Carton, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 27, g.REC_Security, g.iRec_Security_ChangeQty, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 28, g.REC_Security, g.iRec_Security_CreateRTV, 1)
        System.arraycopy(g.REC_Logon, g.iRec_Logon_Security + 29, g.REC_Security, g.iRec_Security_ReleaseRTV, 1)
    }

    private val scanBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        public override fun onReceive(context: Context, intent: Intent) {
            if ((intent.getAction() == g.sFilterAction)) {
                try {
                    displayScanResult(intent)
                } catch (ignored: Exception) {
                }
            }
        }
    }

    private fun displayScanResult(i: Intent) {
        if (_etUserId!!.isFocused()) {
            _etUserId!!.setText(i.getStringExtra(g.sScanData))
            _btnOK!!.performClick()
        } else {
            if (_etPassword!!.isFocused()) {
                _etPassword!!.setText(i.getStringExtra(g.sScanData))
                _btnOK!!.performClick()
            }
        }
    }
}