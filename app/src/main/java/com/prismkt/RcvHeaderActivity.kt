package com.prismkt

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class RcvHeaderActivity : AppCompatActivity(), View.OnKeyListener {
    internal enum class rcvAction { SELECT, FIRST, NEXT, CREATE_RELEASED_PO, RECEIVE, PRINT }
    private val c: Common = Common.Companion.instance
    private val s: Server = Server.Companion.instance
    private var _hObj: Handler? = null
    private var _etFromVendor: EditText? = null
    private var _tvName: TextView? = null
    private var _tvTransaction: TextView? = null
    private var _tvDate: TextView? = null
    private var _tvReference: TextView? = null
    private var _etShipment: EditText? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnIDC_RCV_RCVHEADER_PUSHBUTTON_FIRST: Button? = null
    private var _btnIDC_RCV_RCVHEADER_PUSHBUTTON_DETAILS: Button? = null
    private var _btnIDC_RCV_RCVHEADER_PUSHBUTTON_RECEIVE: Button? = null
    private var _btnIDC_RCV_RCVHEADER_PUSHBUTTON_PRINT: Button? = null
    private var _sFromVendor: String? = null
    private var _sName: String? = null
    private var _sTransaction: String? = null
    private var _sShipment: String? = null
    private var _eRcvHeaderCall: rcvAction? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rcv_header)
        _etFromVendor = findViewById<View>(R.id.IDC_RCV_RCVHEADER_EDIT_FROMVENDOR) as EditText?
        _etShipment = findViewById<View>(R.id.IDC_RCV_RCVHEADER_EDIT_SHIPMENT) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_RCV_RCVHEADER_TEXT_MSG) as TextView?
        _btnIDC_RCV_RCVHEADER_PUSHBUTTON_FIRST =
            findViewById<View>(R.id.IDC_RCV_RCVHEADER_PUSHBUTTON_FIRST) as Button?
        _btnIDC_RCV_RCVHEADER_PUSHBUTTON_DETAILS =
            findViewById<View>(R.id.IDC_RCV_RCVHEADER_PUSHBUTTON_DETAILS) as Button?
        _btnIDC_RCV_RCVHEADER_PUSHBUTTON_RECEIVE =
            findViewById<View>(R.id.IDC_RCV_RCVHEADER_PUSHBUTTON_RECEIVE) as Button?
        _btnIDC_RCV_RCVHEADER_PUSHBUTTON_PRINT =
            findViewById<View>(R.id.IDC_RCV_RCVHEADER_PUSHBUTTON_PRINT) as Button?
        if (g.REC_Security.get(g.iRec_Security_Receiving) != 0x30.toByte()) {
            Arrays.fill(g.REC_Receiver, 0.toByte())
            _hObj = Handler(Looper.getMainLooper())
            _tvName = findViewById<View>(R.id.IDC_RCV_RCVHEADER_EDIT_NAME) as TextView?
            _tvTransaction = findViewById<View>(R.id.IDC_RCV_RCVHEADER_EDIT_TRANNO) as TextView?
            _tvDate = findViewById<View>(R.id.IDC_RCV_RCVHEADER_EDIT_DATE) as TextView?
            _tvReference = findViewById<View>(R.id.IDC_RCV_RCVHEADER_EDIT_REFERENCE) as TextView?
            _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
            if (g.REC_Security.get(g.iRec_Security_VendorLookup) == 0x30.toByte()) {
                val btn: Button =
                    findViewById<View>(R.id.IDC_RCV_RCVHEADER_PUSHBUTTON_VENDORS) as Button
                btn.setEnabled(false)
            }
            _etFromVendor!!.setOnKeyListener(this)
            _etShipment!!.setOnKeyListener(this)
            _etFromVendor!!.requestFocus()
        } else {
            _tvMsg!!.setText(R.string.msg_not_authorized)
            _etFromVendor!!.setEnabled(false)
            _etShipment!!.setEnabled(false)
            _btnIDC_RCV_RCVHEADER_PUSHBUTTON_FIRST!!.setEnabled(false)
            var btn: Button = findViewById<View>(R.id.IDOK) as Button
            btn.setEnabled(false)
            btn = findViewById<View>(R.id.IDC_RCV_RCVHEADER_PUSHBUTTON_NEXT) as Button
            btn.setEnabled(false)
            btn = findViewById<View>(R.id.IDC_RCV_RCVHEADER_PUSHBUTTON_VENDORS) as Button
            btn.setEnabled(false)
            btn = findViewById<View>(R.id.IDC_RCV_RCVHEADER_PUSHBUTTON_CREATERELEASEDPO) as Button
            btn.setEnabled(false)
            btn = findViewById<View>(R.id.IDCANCEL) as Button
            btn.requestFocus()
        }
    }

    override fun onResume() {
        super.onResume()
        if ((g.REC_Receiver.get(g.iRec_Receiver_OrigLocation) != 0x00.toByte()) || (!_etFromVendor!!.getText()
                .toString().trim({ it <= ' ' }).isEmpty())
        ) { //At orientation change, TextView loses text, EditText doesn't.
            c.PrismLayoutRestore(this, R.id.clRcvHeader, true)
            if (g.REC_Receiver.get(g.iRec_Receiver_ShipNumber) != 0x00.toByte()) { //Return from RcvDetailActivity
                _sTransaction = c.PrismGetString(
                    g.REC_Receiver,
                    g.iRec_Receiver_ShipNumber,
                    g.sRec_Receiver_ShipNumber
                ) //SHH:TrnNo
                _sTransaction += " " + getResources().getString(R.string.msg_entries) + " "
                _sTransaction += c.PrismGetString(
                    g.REC_Receiver,
                    g.iRec_Receiver_InvoiceAmt,
                    g.sRec_Receiver_InvoiceAmt
                )
                _sTransaction += " " + getResources().getString(R.string.msg_cost) + " "
                _sTransaction += c.PrismGetString(
                    g.REC_Receiver,
                    g.iRec_Receiver_FreightAmt,
                    g.sRec_Receiver_FreightAmt
                )
                _tvTransaction!!.setText(_sTransaction)
                _btnIDC_RCV_RCVHEADER_PUSHBUTTON_RECEIVE!!.setEnabled(
                    (g.REC_Security.get(g.iRec_Security_CreateRcv) != 0x30.toByte()) && (g.REC_Receiver.get(
                        g.iRec_Receiver_InvoiceAmt
                    ) != 0x30.toByte())
                )
            }
            if (g.REC_Receiver.get(g.iRec_Receiver_OrigLocation) != 0x00.toByte()) { //LookupActivity
                _sFromVendor = c.PrismGetString(
                    g.REC_Receiver,
                    g.iRec_Receiver_OrigLocation,
                    g.sRec_Receiver_OrigLocation
                )
                _etFromVendor!!.setText(_sFromVendor)
                _sName = c.PrismGetString(
                    g.REC_Receiver,
                    g.iRec_Receiver_OrigName,
                    g.sRec_Receiver_OrigName
                )
                _tvName!!.setText(_sName)
            }
        } else {
            c.PrismLayoutRestore(this, R.id.clRcvHeader, false)
        }
        val filter: IntentFilter = IntentFilter()
        filter.addCategory(g.sFilterCategory)
        filter.addAction(g.sFilterAction)
        registerReceiver(scanBroadcastReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clRcvHeader)
        unregisterReceiver(scanBroadcastReceiver)
    }

    public override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
        if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
            _btnIDC_RCV_RCVHEADER_PUSHBUTTON_FIRST!!.performClick()
            return true
        }
        return false
    }

    fun btnIDOK(view: View?) {
        _sFromVendor = _etFromVendor!!.getText().toString().trim({ it <= ' ' })
        _sShipment = _etShipment!!.getText().toString().trim({ it <= ' ' })
        executeAction(rcvAction.SELECT)
    }

    fun btnIDC_RCV_RCVHEADER_PUSHBUTTON_FIRST(view: View?) {
        _sFromVendor = _etFromVendor!!.getText().toString().trim({ it <= ' ' })
        _sShipment = _etShipment!!.getText().toString().trim({ it <= ' ' })
        executeAction(rcvAction.FIRST)
    }

    fun btnIDC_RCV_RCVHEADER_PUSHBUTTON_NEXT(view: View?) {
        _sFromVendor = _etFromVendor!!.getText().toString().trim({ it <= ' ' })
        _sShipment = _etShipment!!.getText().toString().trim({ it <= ' ' })
        executeAction(rcvAction.NEXT)
    }

    fun btnIDC_RCV_RCVHEADER_PUSHBUTTON_CREATERELEASEDPO(view: View?) {
        _sFromVendor = _etFromVendor!!.getText().toString().trim({ it <= ' ' })
        _sShipment = _etShipment!!.getText().toString().trim({ it <= ' ' })
        executeAction(rcvAction.CREATE_RELEASED_PO)
    }

    fun btnIDC_RCV_RCVHEADER_PUSHBUTTON_RECEIVE(view: View?) {
        _eRcvHeaderCall = rcvAction.RECEIVE
        executeReceive()
    }

    fun btnIDC_RCV_RCVHEADER_PUSHBUTTON_PRINT(view: View?) {
        _eRcvHeaderCall = rcvAction.PRINT
        executeReceive()
    }

    fun btnIDC_RCV_RCVHEADER_PUSHBUTTON_VENDORS(view: View?) {
        val intent: Intent = Intent(this, LookupActivity::class.java)
        intent.putExtra("iLookupWhat", 1) //LU_VENMST
        startActivity(intent)
    }

    fun btnIDC_RCV_RCVHEADER_PUSHBUTTON_DETAILS(view: View?) {
        val intent: Intent = Intent(this, RcvDetailActivity::class.java)
        startActivity(intent)
    }

    fun btnIDCANCEL(view: View?) {
        onBackPressed()
    }

    public override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed()
            return true
        }
        return (super.onOptionsItemSelected(item))
    }

    private val scanBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        public override fun onReceive(context: Context, intent: Intent) {
            if ((intent.getAction() == g.sFilterAction)) {
                try {
                    displayScanResult(intent)
                } catch (ignored: Exception) {
                }
            }
        }
    }

    private fun displayScanResult(i: Intent) {
        if (_etFromVendor!!.isFocused()) {
            _etFromVendor!!.setText(i.getStringExtra(g.sScanData))
            _btnIDC_RCV_RCVHEADER_PUSHBUTTON_FIRST!!.performClick()
        } else {
            if (_etShipment!!.isFocused()) {
                _etShipment!!.setText(i.getStringExtra(g.sScanData))
                _btnIDC_RCV_RCVHEADER_PUSHBUTTON_FIRST!!.performClick()
            }
        }
    }

    private fun executeAction(eCall: rcvAction) {
        Thread(ExecuteThread(eCall)).start()
    }

    private fun executeReceive() {
        Thread(object : Runnable {
            public override fun run() {
                var iMsg: Int
                Arrays.fill(g.REC_CloseRcv, 0.toByte())
                c.PrismCopyString(
                    g.REC_CloseRcv,
                    g.iRecordId,
                    g.sRecordId,
                    "CloseRcv203"
                ) //DbFunction 14; Format 52; CloseRcv; PmWinDbm.app (5); Calls CloseReceiver or PrnRcvHdr in Recving.app
                if (_eRcvHeaderCall == rcvAction.RECEIVE) { //R.id.IDC_RCV_RCVHEADER_PUSHBUTTON_RECEIVE:
                    iMsg = R.string.msg_rcv_receiving
                    g.REC_CloseRcv[g.iRec_CloseRcv_CloseFlag] = 0x31 //'1'
                } else { //R.id.IDC_RCV_RCVHEADER_PUSHBUTTON_PRINTE:
                    iMsg = R.string.msg_rcv_printing
                    g.REC_CloseRcv[g.iRec_CloseRcv_CloseFlag] = 0x32 //'2'
                }
                c.PrismMsg(_hObj, _tvMsg, iMsg)
                if (s.ServerSend(g.REC_CloseRcv, _hObj, _tvMsg, _pbAct)) {
                    if (g.REC_CloseRcv.get(g.iRec_CloseRcv_ReadStatus) == 0x31.toByte()) {
                        if (_eRcvHeaderCall == rcvAction.RECEIVE) { //R.id.IDC_RCV_RCVHEADER_PUSHBUTTON_RECEIVE:
                            iMsg = R.string.msg_rcv_received
                            c.PrismBtnEnabled(
                                _hObj,
                                _btnIDC_RCV_RCVHEADER_PUSHBUTTON_RECEIVE,
                                false
                            )
                            c.PrismBtnEnabled(
                                _hObj,
                                _btnIDC_RCV_RCVHEADER_PUSHBUTTON_DETAILS,
                                false
                            )
                        } else { //R.id.IDC_RCV_RCVHEADER_PUSHBUTTON_PRINT:
                            iMsg = R.string.msg_sent_to_printer
                            g.REC_CloseRcv[g.iRec_CloseRcv_CloseFlag] = 0x32 //'2'
                        }
                        c.PrismMsg(_hObj, _tvMsg, iMsg)
                    } else {
                        val sMsg: String? = c.PrismGetString(
                            g.REC_CloseRcv,
                            g.iRec_CloseRcv_ErrorString,
                            g.sRec_CloseRcv_ErrorString
                        )
                        c.PrismMsg(_hObj, _tvMsg, sMsg)
                    }
                }
            }
        }).start()
    }

    internal inner class ExecuteThread constructor(private val _eCall: rcvAction) : Runnable {
        private var _bCtl: Boolean = false
        public override fun run() {
            var iMsg: Int
            var sMsg: String?
            Arrays.fill(g.REC_Receiver, 0.toByte())
            c.PrismCopyString(
                g.REC_Receiver,
                g.iRec_Receiver_OrigLocation,
                g.sRec_Receiver_OrigLocation,
                _sFromVendor
            )
            c.PrismCopyString(
                g.REC_Receiver,
                g.iRec_Receiver_ShipNumber,
                g.sRec_Receiver_ShipNumber,
                _sShipment
            )
            when (_eCall) {
                rcvAction.SELECT -> {
                    exit()
                    return
                }
                rcvAction.FIRST -> {
                    iMsg = R.string.msg_getting_first_rcv
                    c.PrismCopyString(
                        g.REC_Receiver,
                        g.iRecordId,
                        g.sRecordId,
                        "Receiver4307"
                    ) //DbFunction 2; Format 96; RcvLookup; PmWinDbm.app (5)
                    g.REC_Receiver[g.iRec_Receiver_RcvType] = 0x06 //'\6'
                }
                rcvAction.NEXT -> {
                    iMsg = R.string.msg_getting_next_rcv
                    c.PrismCopyString(
                        g.REC_Receiver,
                        g.iRecordId,
                        g.sRecordId,
                        "Receiver4307"
                    ) //DbFunction 2; Format 96; RcvLookup; PmWinDbm.app (5)
                    g.REC_Receiver[g.iRec_Receiver_RcvType] = 0x02 //'\2'
                }
                rcvAction.CREATE_RELEASED_PO -> {
                    if (g.REC_Receiver.get(g.iRec_Receiver_OrigLocation) == 0x00.toByte()) {
                        iMsg = R.string.msg_vendor_must_be_entered
                        c.PrismMsg(_hObj, _tvMsg, iMsg)
                        exit()
                        return
                    }
                    iMsg = R.string.msg_creating_released_rcv
                    c.PrismCopyString(
                        g.REC_Receiver,
                        g.iRecordId,
                        g.sRecordId,
                        "CrtRcv4307"
                    ) //DbFunction 13; Format 96; CreateReceiver; PmWinDbm.app (5); Calls GetOpenRcvHdr (RcvHdr.tps) in Recving.app
                }
                else -> return
            }
            c.PrismMsg(_hObj, _tvMsg, iMsg)
            if (s.ServerSend(g.REC_Receiver, _hObj, _tvMsg, _pbAct)) {
                _sFromVendor = c.PrismGetString(
                    g.REC_Receiver,
                    g.iRec_Receiver_OrigLocation,
                    g.sRec_Receiver_OrigLocation
                )
                c.PrismMsg(_hObj, _etFromVendor, _sFromVendor)
                _sName = c.PrismGetString(
                    g.REC_Receiver,
                    g.iRec_Receiver_OrigName,
                    g.sRec_Receiver_OrigName
                )
                c.PrismMsg(_hObj, _tvName, _sName)
                _sTransaction = c.PrismGetString(
                    g.REC_Receiver,
                    g.iRec_Receiver_ShipNumber,
                    g.sRec_Receiver_ShipNumber
                ) //SHH:TrnNo
                if (g.REC_Receiver.get(g.iRec_Receiver_ShipNumber) != 0x00.toByte()) {
                    _sTransaction += " " + getResources().getString(R.string.msg_entries) + " "
                    _sTransaction += c.PrismGetString(
                        g.REC_Receiver,
                        g.iRec_Receiver_InvoiceAmt,
                        g.sRec_Receiver_InvoiceAmt
                    )
                    _sTransaction += " " + getResources().getString(R.string.msg_cost) + " "
                    _sTransaction += c.PrismGetString(
                        g.REC_Receiver,
                        g.iRec_Receiver_FreightAmt,
                        g.sRec_Receiver_FreightAmt
                    )
                    c.PrismBtnEnabled(_hObj, _btnIDC_RCV_RCVHEADER_PUSHBUTTON_PRINT, true)
                } else {
                    c.PrismBtnEnabled(_hObj, _btnIDC_RCV_RCVHEADER_PUSHBUTTON_PRINT, false)
                }
                c.PrismMsg(_hObj, _tvTransaction, _sTransaction)
                sMsg = c.PrismGetString(
                    g.REC_Receiver,
                    g.iRec_Receiver_OrigShipDate,
                    g.sRec_Receiver_OrigShipDate
                )
                c.PrismMsg(_hObj, _tvDate, sMsg)
                sMsg = c.PrismGetString(
                    g.REC_Receiver,
                    g.iRec_Receiver_Reference,
                    g.sRec_Receiver_Reference
                )
                c.PrismMsg(_hObj, _tvReference, sMsg)
                if (g.REC_Receiver.get(g.iRec_Receiver_ReadStatus) == 0x31.toByte()) {
                    /* when (_eCall) {
                        rcvAction.FIRST -> iMsg = R.string.msg_got_first_rcv
                        rcvAction.NEXT -> iMsg = R.string.msg_got_next_rcv
                        rcvAction.CREATE_RELEASED_PO -> iMsg = R.string.msg_created_released_rcv
                    } */
                    _bCtl = true
                    c.PrismMsg(_hObj, _tvMsg, iMsg)
                } else {
                    sMsg = c.PrismGetString(
                        g.REC_Receiver,
                        g.iRec_Receiver_ErrorString,
                        g.sRec_Receiver_ErrorString
                    )
                    c.PrismMsg(_hObj, _tvMsg, sMsg)
                }
            }
            exit()
        }

        private fun exit() {
            c.PrismBtnEnabled(_hObj, _btnIDC_RCV_RCVHEADER_PUSHBUTTON_DETAILS, _bCtl)
            c.PrismBtnEnabled(
                _hObj, _btnIDC_RCV_RCVHEADER_PUSHBUTTON_RECEIVE,
                ((_bCtl && (g.REC_Security.get(g.iRec_Security_CreateRcv) != 0x30.toByte()) &&
                        (g.REC_Receiver.get(g.iRec_Receiver_InvoiceAmt) != 0x30.toByte())))
            )
            if (_bCtl) {
                c.PrismReqFocus(_hObj, _btnIDC_RCV_RCVHEADER_PUSHBUTTON_DETAILS)
            } else {
                c.PrismReqFocus(_hObj, _etFromVendor)
            }
        }
    }
}