package com.prismkt

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class RcvDetailActivity constructor() : AppCompatActivity(), View.OnKeyListener {
    private val c: Common = Common.Companion.instance
    private val s: Server = Server.Companion.instance
    private var _hObj: Handler? = null
    private var _etItem: EditText? = null
    private var _tvSKU: TextView? = null
    private var _tvDesc: TextView? = null
    private var _tvOrdered: TextView? = null
    private var _tvReceived: TextView? = null
    private var _etQty: EditText? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnOK: Button? = null
    private var _sItem: String? = null
    private var _sQty: String? = null
    private var _sMsg: String? = null
    private var _sRcvDetailNew: String? = null
    private var _sRcvDetailQty: String? = null
    private var _iRcvDetailScanFlag: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rcv_detail)
        _hObj = Handler(Looper.getMainLooper())
        _etItem = findViewById<View>(R.id.IDC_RCV_RCVDETAIL_EDIT_ITEM) as EditText?
        _tvSKU = findViewById<View>(R.id.IDC_RCV_RCVDETAIL_EDIT_SKU) as TextView?
        _tvDesc = findViewById<View>(R.id.IDC_RCV_RCVDETAIL_EDIT_DESC) as TextView?
        _tvOrdered = findViewById<View>(R.id.IDC_RCV_RCVDETAIL_EDIT_ORDERED) as TextView?
        _tvReceived = findViewById<View>(R.id.IDC_RCV_RCVDETAIL_EDIT_RECEIVED) as TextView?
        _etQty = findViewById<View>(R.id.IDC_RCV_RCVDETAIL_EDIT_QTY) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_RCV_RCVDETAIL_TEXT_MSG) as TextView?
        _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
        _btnOK = findViewById<View>(R.id.IDOK) as Button?
        _sRcvDetailNew = ""
        _iRcvDetailScanFlag = 0
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setTitle(R.string.act_rcv_detail)
        } else {
            setTitle(R.string.act_rcv_detail_port)
        }
        setTitle(
            getTitle().toString() + " " + c.PrismGetString(
                g.REC_Receiver,
                g.iRec_Receiver_ShipNumber,
                g.sRec_Receiver_ShipNumber
            )
        )
        _etItem!!.setOnKeyListener(this)
        _etQty!!.setOnKeyListener(this)
        _etItem!!.requestFocus()
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(
            this,
            R.id.clRcvDetail,
            !_etItem!!.getText().toString().trim({ it <= ' ' }).isEmpty()
        )
        val filter: IntentFilter = IntentFilter()
        filter.addCategory(g.sFilterCategory)
        filter.addAction(g.sFilterAction)
        registerReceiver(scanBroadcastReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clRcvDetail)
        unregisterReceiver(scanBroadcastReceiver)
    }

    public override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
        if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
            _btnOK!!.performClick()
            return true
        }
        return false
    }

    fun btnIDOK(view: View?) {
        _sItem = _etItem!!.getText().toString().trim({ it <= ' ' })
        if (_sItem!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_item_must_be_entered)
        } else {
            _sQty = _etQty!!.getText().toString().trim({ it <= ' ' })
            executeAction()
        }
    }

    fun btnIDCANCEL(view: View?) {
        onBackPressed()
    }

    public override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed()
            return true
        }
        return (super.onOptionsItemSelected(item))
    }

    private fun executeAction() {
        Thread(ExecuteThread()).start()
    }

    private val scanBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        public override fun onReceive(context: Context, intent: Intent) {
            if ((intent.getAction() == g.sFilterAction)) {
                try {
                    displayScanResult(intent)
                } catch (ignored: Exception) {
                }
            }
        }
    }

    private fun displayScanResult(i: Intent) {
        _etItem!!.setText(i.getStringExtra(g.sScanData))
        _sRcvDetailNew = ""
        _iRcvDetailScanFlag = 1
        if (g.REC_PDConfig.get(g.iRec_PDConfig_ImmediateScan) != 0x31.toByte()) {
            val imm: InputMethodManager =
                getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            c.PrismReqFocus(_hObj, _etQty, imm)
        }
        _btnOK!!.performClick()
    }

    internal inner class ExecuteThread constructor() : Runnable {
        public override fun run() {
            if (!(_sItem == _sRcvDetailNew)) {
                if ((_iRcvDetailScanFlag != 0) && (g.REC_PDConfig.get(g.iRec_PDConfig_ImmediateScan) == 0x31.toByte())) {
                    Arrays.fill(g.REC_RecvDtl, 0.toByte())
                    c.PrismCopyString(
                        g.REC_RecvDtl,
                        g.iRecordId,
                        g.sRecordId,
                        "RecvDtl210"
                    ) //DbFunction 3; Format 78; ReceiverDtl; PmWinDbm.app (5); ShpDtl.tps, PdConfig.tps
                    c.PrismCopyString(g.REC_RecvDtl, g.iRec_RecvDtl_SKU, g.sRec_RecvDtl_SKU, _sItem)
                    g.REC_RecvDtl[g.iRec_RecvDtl_Quantity] = 0x31 //'1'
                    c.PrismMsg(_hObj, _etQty, "1")
                    _sQty = "1"
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_sending_item)
                    if (s.ServerSend(g.REC_RecvDtl, _hObj, _tvMsg, _pbAct)) {
                        if (g.REC_RecvDtl.get(g.iRec_RecvDtl_ReadStatus) == 0x31.toByte()) {
                            val sbSku: StringBuilder = StringBuilder()
                            _sRcvDetailNew = c.PrismGetItemSku(
                                g.REC_RecvDtl,
                                g.iRec_RecvDtl_SKU,
                                g.sRec_RecvDtl_SKU,
                                sbSku
                            )
                            c.PrismMsg(_hObj, _etItem, _sRcvDetailNew)
                            _sMsg = sbSku.toString()
                            c.PrismMsg(_hObj, _tvSKU, _sMsg)
                            System.arraycopy(
                                g.REC_RecvDtl,
                                g.iRec_RecvDtl_InvMst,
                                g.REC_InvMst,
                                0,
                                g.sRec_InvMst
                            )
                            _sMsg = c.PrismGetDescString()
                            c.PrismMsg(_hObj, _tvDesc, _sMsg)
                            _sMsg = c.PrismGetString(
                                g.REC_RecvDtl,
                                g.iRec_RecvDtl_User1Data,
                                g.sRec_RecvDtl_User1Data
                            )
                            c.PrismMsg(_hObj, _tvOrdered, _sMsg)
                            _sRcvDetailQty = _sMsg
                            _sMsg = c.PrismGetString(
                                g.REC_RecvDtl,
                                g.iRec_RecvDtl_User2Data,
                                g.sRec_RecvDtl_User2Data
                            )
                            c.PrismMsg(_hObj, _tvReceived, _sMsg)
                            _sMsg = getResources().getString(R.string.msg_sent_item)
                            if (g.REC_RecvDtl.get(g.iRec_RecvDtl_ErrorString) != 0x00.toByte()) {
                                _sMsg += (" " + getResources().getString(R.string.msg_with_error) +
                                        " " + c.PrismGetString(
                                    g.REC_RecvDtl,
                                    g.iRec_RecvDtl_ErrorString,
                                    g.sRec_RecvDtl_ErrorString
                                ))
                                c.PrismReqFocus(_hObj, _etItem)
                            } else {
                                Arrays.fill(
                                    g.REC_Receiver,
                                    g.iRec_Receiver_InvoiceAmt,
                                    g.iRec_Receiver_InvoiceAmt + g.sRec_Receiver_InvoiceAmt - 1,
                                    0.toByte()
                                )
                                Arrays.fill(
                                    g.REC_Receiver,
                                    g.iRec_Receiver_FreightAmt,
                                    g.iRec_Receiver_FreightAmt + g.sRec_Receiver_FreightAmt - 1,
                                    0.toByte()
                                )
                                System.arraycopy(
                                    g.REC_InvMst,
                                    g.iRec_InvMst_Quantity,
                                    g.REC_Receiver,
                                    g.iRec_Receiver_InvoiceAmt,
                                    g.sRec_InvMst_Quantity
                                )
                                System.arraycopy(
                                    g.REC_InvMst,
                                    g.iRec_InvMst_Price,
                                    g.REC_Receiver,
                                    g.iRec_Receiver_FreightAmt,
                                    g.sRec_InvMst_Price
                                )
                                c.PrismReqFocus(_hObj, _etQty)
                            }
                        } else {
                            c.PrismMsg(_hObj, _tvSKU, "")
                            c.PrismMsg(_hObj, _tvDesc, "")
                            c.PrismMsg(_hObj, _tvOrdered, "")
                            c.PrismMsg(_hObj, _tvReceived, "")
                            c.PrismMsg(_hObj, _etQty, "")
                            _sQty = ""
                            _sMsg = c.PrismGetString(
                                g.REC_RecvDtl,
                                g.iRec_RecvDtl_ErrorString,
                                g.sRec_RecvDtl_ErrorString
                            )
                            c.PrismReqFocus(_hObj, _etItem)
                        }
                        c.PrismMsg(_hObj, _tvMsg, _sMsg)
                    } //if (s.ServerSend(g.REC_RecvDtl, _hObj, _tvMsg, _pbAct)) {
                } else { //if ((_iRcvDetailScanFlag != 0) && (g.REC_PDConfig[g.iRec_PDConfig_ImmediateScan] == 0x31.toByte())){
                    Arrays.fill(g.REC_InvMst, 0.toByte())
                    c.PrismCopyString(
                        g.REC_InvMst,
                        g.iRecordId,
                        g.sRecordId,
                        "InvMst210"
                    ) //DbFunction 4; Format 79; GetItemDetail; PmWinDbm.app (11).
                    g.REC_InvMst[g.iRec_InvMst_FunctionID] = 0x43 //'C' KIA 04/01/17 BSH 13. Added New Function. See GetItemDetail.
                    c.PrismCopyString(g.REC_InvMst, g.iRec_InvMst_SKU, g.sRec_InvMst_SKU, _sItem)
                    c.PrismMsg(_hObj, _tvOrdered, "")
                    c.PrismMsg(_hObj, _tvReceived, "")
                    c.PrismMsg(_hObj, _etQty, "")
                    _sRcvDetailQty = ""
                    _sQty = ""
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_getting_item)
                    if (s.ServerSend(g.REC_InvMst, _hObj, _tvMsg, _pbAct)) {
                        if (g.REC_InvMst.get(g.iRec_InvMst_ReadStatus) == 0x31.toByte()) {
                            val sbSku: StringBuilder = StringBuilder()
                            _sRcvDetailNew = c.PrismGetItemSku(
                                g.REC_InvMst,
                                g.iRec_InvMst_SKU,
                                g.sRec_InvMst_SKU,
                                sbSku
                            )
                            c.PrismMsg(_hObj, _etItem, _sRcvDetailNew)
                            _sMsg = sbSku.toString()
                            c.PrismMsg(_hObj, _tvSKU, _sMsg)
                            _sMsg = c.PrismGetDescString()
                            c.PrismMsg(_hObj, _tvDesc, _sMsg)
                            _sMsg = getResources().getString(R.string.msg_got_item_enter_qty)
                            c.PrismReqFocus(_hObj, _etQty)
                        } else {
                            c.PrismMsg(_hObj, _tvSKU, "")
                            c.PrismMsg(_hObj, _tvDesc, "")
                            _sMsg = c.PrismGetString(
                                g.REC_InvMst,
                                g.iRec_InvMst_ErrorString,
                                g.sRec_InvMst_ErrorString
                            )
                            c.PrismReqFocus(_hObj, _etItem)
                        }
                    }
                }
                exit()
                return
            } //if (_sItem.equals(_sRcvDetailNew) == false) {
            if (!(_sQty == _sRcvDetailQty)) {
                Arrays.fill(g.REC_RecvDtl, 0.toByte())
                c.PrismCopyString(
                    g.REC_RecvDtl,
                    g.iRecordId,
                    g.sRecordId,
                    "RecvDtl210"
                ) //DbFunction 3; Format 78; ReceiverDtl; PmWinDbm.app (5); ShpDtl.tps, PdConfig.tps
                c.PrismCopyString(
                    g.REC_RecvDtl,
                    g.iRec_RecvDtl_SKU,
                    g.sRec_RecvDtl_SKU,
                    _sRcvDetailNew
                )
                c.PrismCopyString(
                    g.REC_RecvDtl,
                    g.iRec_RecvDtl_Quantity,
                    g.sRec_RecvDtl_Quantity,
                    _sQty
                )
                _sRcvDetailQty = _sQty
                _sMsg = getResources().getString(R.string.msg_sending_qty)
                c.PrismMsg(_hObj, _tvMsg, _sMsg)
                if (s.ServerSend(g.REC_RecvDtl, _hObj, _tvMsg, _pbAct)) {
                    if (g.REC_RecvDtl.get(g.iRec_RecvDtl_ReadStatus) == 0x31.toByte()) {
                        val sbSku: StringBuilder = StringBuilder()
                        _sRcvDetailNew = c.PrismGetItemSku(
                            g.REC_RecvDtl,
                            g.iRec_RecvDtl_SKU,
                            g.sRec_RecvDtl_SKU,
                            sbSku
                        )
                        c.PrismMsg(_hObj, _etItem, _sRcvDetailNew)
                        _sMsg = sbSku.toString()
                        c.PrismMsg(_hObj, _tvSKU, _sMsg)
                        System.arraycopy(
                            g.REC_RecvDtl,
                            g.iRec_RecvDtl_InvMst,
                            g.REC_InvMst,
                            0,
                            g.sRec_InvMst
                        )
                        _sMsg = c.PrismGetDescString()
                        c.PrismMsg(_hObj, _tvDesc, _sMsg)
                        _sMsg = c.PrismGetString(
                            g.REC_RecvDtl,
                            g.iRec_RecvDtl_User1Data,
                            g.sRec_RecvDtl_User1Data
                        )
                        c.PrismMsg(_hObj, _tvOrdered, _sMsg)
                        _sMsg = c.PrismGetString(
                            g.REC_RecvDtl,
                            g.iRec_RecvDtl_User2Data,
                            g.sRec_RecvDtl_User2Data
                        )
                        c.PrismMsg(_hObj, _tvReceived, _sMsg)
                        _sMsg = getResources().getString(R.string.msg_sent_qty)
                        if (g.REC_RecvDtl.get(g.iRec_RecvDtl_ErrorString) != 0x00.toByte()) {
                            _sMsg += (" " + getResources().getString(R.string.msg_with_error) +
                                    " " + c.PrismGetString(
                                g.REC_RecvDtl,
                                g.iRec_RecvDtl_ErrorString,
                                g.sRec_RecvDtl_ErrorString
                            ))
                        } else {
                            Arrays.fill(
                                g.REC_Receiver,
                                g.iRec_Receiver_InvoiceAmt,
                                g.iRec_Receiver_InvoiceAmt + g.sRec_Receiver_InvoiceAmt - 1,
                                0.toByte()
                            )
                            Arrays.fill(
                                g.REC_Receiver,
                                g.iRec_Receiver_FreightAmt,
                                g.iRec_Receiver_FreightAmt + g.sRec_Receiver_FreightAmt - 1,
                                0.toByte()
                            )
                            System.arraycopy(
                                g.REC_InvMst,
                                g.iRec_InvMst_Quantity,
                                g.REC_Receiver,
                                g.iRec_Receiver_InvoiceAmt,
                                g.sRec_InvMst_Quantity
                            )
                            System.arraycopy(
                                g.REC_InvMst,
                                g.iRec_InvMst_Price,
                                g.REC_Receiver,
                                g.iRec_Receiver_FreightAmt,
                                g.sRec_InvMst_Price
                            )
                        }
                        c.PrismReqFocus(_hObj, _etItem)
                    } else {
                        c.PrismMsg(_hObj, _tvSKU, "")
                        c.PrismMsg(_hObj, _tvDesc, "")
                        c.PrismMsg(_hObj, _tvOrdered, "")
                        c.PrismMsg(_hObj, _tvReceived, "")
                        c.PrismMsg(_hObj, _etQty, "")
                        _sQty = ""
                        _sMsg = c.PrismGetString(
                            g.REC_RecvDtl,
                            g.iRec_RecvDtl_ErrorString,
                            g.sRec_RecvDtl_ErrorString
                        )
                        c.PrismReqFocus(_hObj, _etQty)
                    }
                } //if (s.ServerSend(g.REC_RecvDtl, _hObj, _tvMsg, _pbAct)) {
            } //if (_sQty.equals(_sRcvDetailQty) == false) {
            exit()
        }

        private fun exit() {
            _iRcvDetailScanFlag = 0
            c.PrismMsg(_hObj, _tvMsg, _sMsg)
        }
    }
}