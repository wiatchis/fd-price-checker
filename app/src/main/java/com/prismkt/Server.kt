package com.prismkt

import android.os.Handler
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.text.isDigitsOnly
import java.io.IOException
import java.net.Socket
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock
import kotlin.experimental.and

class Server private constructor() {
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null

    fun Connect(host: String?, main: MainActivity?, hObj: Handler?, tvMsg: TextView?) {
        if (_socket != null) Disconnect()
        var lstHost: List<String>? = host?.split(':')
        if ((lstHost != null) && (lstHost.count() == 2) && lstHost!![1].isDigitsOnly()) {
            _serverName = lstHost!![0]
            _serverPort = lstHost!![1].toInt()
        } else {
            _serverName = host
        }
        _hObj = hObj //It's for RecvThread
        _tvMsg = tvMsg //It's for RecvThread
        val connectThread = Thread(ConnectThread(main))
        connectThread.start()
    }

    fun Disconnect() {
        if (_socket != null) {
            try { _socket!!.close() } catch (ignored: IOException) { }
            _socket = null
        }
    }

    fun Connected(): Boolean { return (_socket != null) }

    fun ServerSend(bData: ByteArray, hObj: Handler?, tvMsg: TextView?, pbAct: ProgressBar?): Boolean {
        var ret: Boolean
        try {
            _hObj = hObj
            _tvMsg = tvMsg
            _pbAct = pbAct
            _timer = Timer()
            _recvQueue = LinkedList<ByteArray?>()
            c.PrismProgressBar(_hObj, _pbAct, View.VISIBLE)
            Thread(TimerThread()).start()
            Thread(SendThread(bData)).start()
            var iSize: Int
            _syncToken.withLock {
                _condition.await(_recvTimeOut.toLong(),TimeUnit.MILLISECONDS)
                (_recvQueue as LinkedList<ByteArray?>).poll()
                if ((_recvBuffer != null) && (_recvBuffer!!.size > (SERVERBUFHEAD - 2))) {
                    iSize = Math.min((_recvBuffer!!.size - (SERVERBUFHEAD - 2)), bData.size)
                    System.arraycopy(_recvBuffer!!, SERVERBUFHEAD - 2, bData, 0, iSize)
                }
            }
            /*
            synchronized(_syncToken, {
                _syncToken.wait(_recvTimeOut.toLong()) //Releases _syncToken and waits.
                (_recvQueue as LinkedList<ByteArray?>).poll()
                if ((_recvBuffer != null) && (_recvBuffer!!.size > (SERVERBUFHEAD - 2))) {
                    iSize = Math.min((_recvBuffer!!.size - (SERVERBUFHEAD - 2)), bData.size)
                    System.arraycopy(_recvBuffer, SERVERBUFHEAD - 2, bData, 0, iSize)
                }
            })
             */
            ret = true
        } catch (e: InterruptedException) {
            c.PrismMsg(_hObj, _tvMsg, e.getLocalizedMessage())
            ret = false
        }
        _timer!!.cancel()
        c.PrismProgressBar(_hObj, _pbAct, View.GONE)
        return ret
    }

    fun ServerReceive(bData: ByteArray, hObj: Handler?, tvMsg: TextView?, pbAct: ProgressBar?): Int {
        var ret = 0
        try {
            _hObj = hObj
            _tvMsg = tvMsg
            _pbAct = pbAct
            _timer = Timer()
            c.PrismProgressBar(_hObj, _pbAct, View.VISIBLE)
            Thread(TimerThread()).start()
            var iSize: Int
            _syncToken.withLock {
                _recvBuffer = _recvQueue!!.poll()
                if (_recvBuffer == null) {
                    _condition.await(_recvTimeOut.toLong(),TimeUnit.MILLISECONDS)
                    _recvBuffer = _recvQueue!!.poll()
                }
                if (_recvBuffer != null) {
                    iSize = Math.min(_recvBuffer!!.size, bData.size)
                    System.arraycopy(_recvBuffer!!, 0, bData, 0, iSize)
                    ret = _recvBuffer!!.size
                } else {
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_no_print_data)
                }
            }
            /*
            synchronized(_syncToken, {
                _recvBuffer = _recvQueue!!.poll()
                if (_recvBuffer == null) {
                    _syncToken.wait(_recvTimeOut.toLong()) //Releases _syncToken and waits.
                    _recvBuffer = _recvQueue!!.poll()
                }
                if (_recvBuffer != null) {
                    iSize = Math.min(_recvBuffer!!.size, bData.size)
                    System.arraycopy(_recvBuffer, 0, bData, 0, iSize)
                    ret = _recvBuffer!!.size
                } else {
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_no_print_data)
                }
            })
             */
        } catch (e: InterruptedException) {
            c.PrismMsg(_hObj, _tvMsg, e.getLocalizedMessage())
        }
        _timer!!.cancel()
        c.PrismProgressBar(_hObj, _pbAct, View.GONE)
        return ret
    }

    internal inner class ConnectThread constructor(private val _main: MainActivity?) : Runnable {
        override fun run() {
            try {
                _socket = Socket(_serverName, _serverPort)
                _recvQueue = LinkedList<ByteArray?>()
                Thread(RecvThread()).start()
                _syncToken.withLock {
                    _condition.await()
                    (_recvQueue as LinkedList<ByteArray?>).poll()
                }
                /*
                synchronized(_syncToken, {
                    _syncToken.wait() //Releases _syncToken and waits.
                    (_recvQueue as LinkedList<ByteArray?>).poll()
                })
                 */
                if (_main != null) {
                    _main.runOnUiThread(object : Runnable {
                        override fun run() {
                            _main.Connected()
                        }
                    })
                }
            } catch (e: Exception) {
                _main!!.runOnUiThread(object : Runnable {
                    override fun run() {
                        _main.CannotConnect(e.getLocalizedMessage())
                    }
                })
            }
        }
    }

    internal inner class RecvThread : Runnable {
        override fun run() {
            val buffer = ByteArray(2)
            var iSize = 0
            while (true) {
                try {
                    iSize = _socket!!.getInputStream().read(buffer)
                    if (iSize == 2) {
                        var i: Int = (buffer[0].toInt() and 0xFF) + ((buffer[1].toInt() and 0xFF) shl 8)
                        //var i: Int = ((buffer[0] and 0xFF.toByte()).toUInt() + ((buffer[1] and 0xFF.toByte()).toUInt() shl 8)).toInt()
                        //var i: Int = ((buffer.get(0) and 0xFF)) + ((buffer.get(1) and 0xFF) shl 8)
                        _syncToken.withLock {
                            _recvBuffer = ByteArray(i)
                            i = _socket!!.getInputStream().read(_recvBuffer)
                            if (i == _recvBuffer!!.size) {
                                _recvQueue!!.add(_recvBuffer)
                                _condition.signalAll()
                            }
                        }
                        /*
                        synchronized(_syncToken, {
                            _recvBuffer = ByteArray(i)
                            i = _socket!!.getInputStream().read(_recvBuffer)
                            if (i == _recvBuffer!!.size) {
                                _recvQueue!!.add(_recvBuffer)
                                _syncToken.notifyAll()
                            }
                        })
                         */
                    } else {
                        break
                    }
                } catch (e: IOException) {
                    c.PrismMsg(_hObj, _tvMsg, e.getLocalizedMessage())
                    break
                }
            }
            _syncToken.withLock {
                _recvBuffer = null
                _condition.signalAll()
            }
            /*
            synchronized(_syncToken, {
                _recvBuffer = null
                _syncToken.notifyAll()
            })
             */
        }
    }

    internal inner class SendThread constructor(bData: ByteArray) : Runnable {
        private val _buffer: ByteArray
        override fun run() {
            try {
                _socket!!.getOutputStream().write(_buffer)
                _socket!!.getOutputStream().flush()
            } catch (e: IOException) {
                c.PrismMsg(_hObj, _tvMsg, e.getLocalizedMessage())
                _syncToken.withLock { _condition.signalAll() }
                //synchronized(_syncToken, { _syncToken.notifyAll() })
            }
        }

        init {
            _buffer = ByteArray(bData.size + SERVERBUFHEAD)
            Arrays.fill(_buffer, 0x00.toByte())
            var i: Int = _buffer.size
            i = i - 2
            _buffer[0] = i.toByte()
            _buffer[1] = (i ushr 8).toByte()
            //_buffer.get(0) = i.toByte()
            //_buffer.get(1) = (i ushr 8).toByte()
            System.arraycopy(bData, 0, _buffer, 2, 14)
            System.arraycopy(bData, 0, _buffer, SERVERBUFHEAD, bData.size)
        }
    }

    internal inner class TimerThread : Runnable {
        override fun run() {
            _tickCount = 0
            try {
                _timer!!.scheduleAtFixedRate(Tick(), 0, _ticPeriod.toLong())
            } catch (ignored: Exception) { }
        }
    }

    internal inner class Tick : TimerTask() {
        override fun run() {
            _hObj!!.post(object : Runnable {
                override fun run() {
                    _syncToken.withLock {
                        _tvMsg!!.setText(_tvMsg!!.getText())
                        val max: Int = _pbAct!!.getMax()
                        val cur: Int = ((((++_tickCount) * _ticPeriod) * max) / _recvTimeOut)
                        _pbAct!!.setProgress(cur)
                    }
                    /*
                    synchronized(_syncToken, {
                        _tvMsg!!.setText(_tvMsg!!.getText())
                        val max: Int = _pbAct!!.getMax()
                        val cur: Int = ((((++_tickCount) * _ticPeriod) * max) / _recvTimeOut)
                        _pbAct!!.setProgress(cur)
                    })
                     */
                }
            })
        }
    }

    companion object {
        val instance: Server = Server()
        private val c: Common = Common.instance //It was: Common.Companion.instance
        val _syncToken = ReentrantLock() //It was: val _syncToken: Any = Any() converted by Android Studio
        val _condition = _syncToken.newCondition() //Added
        const val SERVERNAME: String = "PrismServer.txt"
        private var _serverName: String? = null
        private var _serverPort: Int = 3517
        private var _socket: Socket? = null
        private const val SERVERBUFHEAD: Int = 49
        private const val _recvTimeOut: Int = 5000
        private const val _ticPeriod: Int = 100
        private var _recvBuffer: ByteArray? = null
        private var _recvQueue: Queue<ByteArray?>? = null
        private var _timer: Timer? = null
        private var _tickCount: Int = 0
        private var _hObj: Handler? = null
    }
}