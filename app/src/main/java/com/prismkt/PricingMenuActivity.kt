package com.prismkt

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity

class PricingMenuActivity constructor() : AppCompatActivity() {
    private lateinit var _rbArray: Array<RadioButton?>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pricing_menu)
        _rbArray = arrayOfNulls(7)
        _rbArray[0] = findViewById<View>(R.id.IDC_RADIO_PRICING_PRICINGMENU_PRICEVERIFICATION) as RadioButton
        _rbArray[1] = findViewById<View>(R.id.IDC_RADIO_PRICING_PRICINGMENU_EVENTVERIFICATION) as RadioButton
        _rbArray[2] = findViewById<View>(R.id.IDC_RADIO_PRICING_PRICINGMENU_PRINTITEMLABELS) as RadioButton
        _rbArray[3] = findViewById<View>(R.id.IDC_RADIO_PRICING_PRICINGMENU_PRINTSHELFLABELS) as RadioButton
        _rbArray[4] = findViewById<View>(R.id.IDC_RADIO_PRICING_PRICINGMENU_PRINTFUTUREPRICES) as RadioButton
        _rbArray[5] = findViewById<View>(R.id.IDC_RADIO_PRICING_PRICINGMENU_PRINTPRICESONLY) as RadioButton
        _rbArray[6] = findViewById<View>(R.id.IDC_RADIO_PRICING_PRICINGMENU_COUPONINQUIRY) as RadioButton
    }

    override fun onResume() {
        super.onResume()
        val bEnable: Boolean
        if (_rbArrayIndex >= 0 && _rbArrayIndex < _rbArray.size) {
            _rbArray.get(_rbArrayIndex)!!.setChecked(true)
            bEnable = true
        } else {
            bEnable = false
        }
        val btnOK: Button = findViewById<View>(R.id.IDOK) as Button
        btnOK.setEnabled(bEnable)
    }

    fun rbClicked(view: View?) {
        executeAction()
    }

    fun btnIDOK(view: View?) {
        executeAction()
    }

    fun btnIDCANCEL(view: View?) {
        onBackPressed()
    }

    public override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode >= KeyEvent.KEYCODE_1 && keyCode <= KeyEvent.KEYCODE_7) {
            _rbArrayIndex = keyCode - KeyEvent.KEYCODE_1
            _rbArray.get(_rbArrayIndex)!!.setChecked(true)
            val btnOK: Button = findViewById<View>(R.id.IDOK) as Button
            btnOK.performClick()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun executeAction() {
        val intent: Intent
        _rbArrayIndex = Common.Companion.instance.PrismGetMenuItem(_rbArray)
        when (_rbArrayIndex) {
            0 -> intent = Intent(this, PriceVerificationActivity::class.java)
            1 -> intent = Intent(this, EventVerificationActivity::class.java)
            2, 3, 4, 5 -> {
                intent = Intent(this, PrintItemActivity::class.java)
                intent.putExtra("iPrinterItemShelfLabels", _rbArrayIndex - 2)
            }
            else -> {
                intent = Intent(this, CouponActivity::class.java)
                intent.putExtra("iCouponWhat", 0)
            }
        }
        startActivity(intent)
    }

    companion object {
        private var _rbArrayIndex: Int = -1
    }
}