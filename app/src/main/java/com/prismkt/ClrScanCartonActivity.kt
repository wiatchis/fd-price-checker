package com.prismkt

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class ClrScanCartonActivity constructor() : AppCompatActivity() {
    private val c: Common = Common.Companion.instance
    private val s: Server = Server.Companion.instance
    private var _hObj: Handler? = null
    private var _etPallet: EditText? = null
    private var _tvDoc: TextView? = null
    private var _tvBOL: TextView? = null
    private var _tvUnits: TextView? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnIDOK: Button? = null
    private var _btnIDC_CLR_SCANCARTON_PUSHBUTTON_DETAILS: Button? = null
    private var _btnIDC_CLR_SCANCARTON_PUSHBUTTON_RECEIVE: Button? = null
    private var _btnIDC_CLR_SCANCARTON_PUSHBUTTON_TOTALS: Button? = null
    private var _btnIDCANCEL: Button? = null
    private var _sPallet: String? = ""
    private var _sClrScanCartonId: String? = ""
    private var _iClrScanCartonTotalType: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clr_scan_carton)
        _etPallet = findViewById<View>(R.id.IDC_CLR_SCANCARTON_EDIT_ID) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_CLR_SCANCARTON_TEXT_MSG) as TextView?
        _btnIDOK = findViewById<View>(R.id.IDOK) as Button?
        _btnIDCANCEL = findViewById<View>(R.id.IDCANCEL) as Button?
        _iClrScanCartonTotalType = getIntent().getIntExtra("iWhat", 0)
        val sMsg: String
        val sItems: Array<String>
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            sMsg = getResources().getString(R.string.act_clr_carton)
            sItems = getResources().getStringArray(R.array.cartons)
        } else {
            sMsg = getResources().getString(R.string.act_clr_carton_port)
            sItems = getResources().getStringArray(R.array.cartons_port)
        }
        setTitle(sMsg + " " + sItems.get(_iClrScanCartonTotalType))
        if (g.REC_Security.get(g.iRec_Security_Carton) == 0x30.toByte()) {
            _tvMsg!!.setText(R.string.msg_not_authorized)
            _etPallet!!.setEnabled(false)
            _btnIDOK!!.setEnabled(false)
            _btnIDCANCEL!!.requestFocus()
            return
        }
        _sClrScanCartonId = ""
        _hObj = Handler(Looper.getMainLooper())
        _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
        _tvDoc = findViewById<View>(R.id.IDC_CLR_SCANCARTON_EDIT_DOC) as TextView?
        _tvBOL = findViewById<View>(R.id.IDC_CLR_SCANCARTON_EDIT_BOL) as TextView?
        _tvUnits = findViewById<View>(R.id.IDC_CLR_SCANCARTON_EDIT_UNITS) as TextView?
        _btnIDC_CLR_SCANCARTON_PUSHBUTTON_DETAILS =
            findViewById<View>(R.id.IDC_CLR_SCANCARTON_PUSHBUTTON_DETAILS) as Button?
        _btnIDC_CLR_SCANCARTON_PUSHBUTTON_RECEIVE =
            findViewById<View>(R.id.IDC_CLR_SCANCARTON_PUSHBUTTON_RECEIVE) as Button?
        _btnIDC_CLR_SCANCARTON_PUSHBUTTON_TOTALS =
            findViewById<View>(R.id.IDC_CLR_SCANCARTON_PUSHBUTTON_TOTALS) as Button?
        val imm: InputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        c.PrismReqFocus(_hObj, _etPallet, imm)
        _etPallet!!.setOnKeyListener(object : View.OnKeyListener {
            public override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
                if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
                    _btnIDOK!!.performClick()
                    return true
                }
                return false
            }
        })
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(
            this,
            R.id.clClrScanCarton,
            (g.REC_ClrHdr.get(g.iRec_ClrHdr_NewCartonId) != 0x00.toByte())
        )
        val filter: IntentFilter = IntentFilter()
        filter.addCategory(g.sFilterCategory)
        filter.addAction(g.sFilterAction)
        registerReceiver(scanBroadcastReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clClrScanCarton)
        unregisterReceiver(scanBroadcastReceiver)
    }

    fun btnIDOK(view: View?) {
        _sPallet = _etPallet!!.getText().toString().trim({ it <= ' ' })
        if (_sPallet!!.isEmpty()) {
            _tvMsg!!.setText(R.string.msg_pallet_id_must_be_entered)
            _etPallet!!.requestFocus()
        } else {
            executeGet()
        }
    }

    fun btnIDC_CLR_SCANCARTON_PUSHBUTTON_DETAILS(view: View?) {
        val intent: Intent = Intent(this, ClrScanItemActivity::class.java)
        startActivity(intent)
    }

    fun btnIDC_CLR_SCANCARTON_PUSHBUTTON_RECEIVE(view: View?) {
        _sPallet = _etPallet!!.getText().toString().trim({ it <= ' ' })
        if (_sPallet!!.isEmpty() || !(_sPallet == _sClrScanCartonId)) {
            _tvMsg!!.setText(R.string.msg_pallet_id_has_changed)
            _etPallet!!.requestFocus()
        } else {
            executeReceive()
        }
    }

    fun btnIDC_CLR_SCANCARTON_PUSHBUTTON_TOTALS(view: View?) {
        val intent: Intent = Intent(this, ClrTotalsActivity::class.java)
        startActivity(intent)
    }

    fun btnIDCANCEL(view: View?) {
        checkExit()
    }

    public override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            checkExit()
            return true
        }
        return (super.onOptionsItemSelected(item))
    }

    public override fun onKeyDown(
        keyCode: Int,
        event: KeyEvent
    ): Boolean { //onBackPressed() api lvl 5+
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            checkExit()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun checkExit() {
        if ((_iClrScanCartonTotalType < 1) || (_iClrScanCartonTotalType > 2) || !_btnIDOK!!.isEnabled()) {
            onBackPressed()
        } else {
            executeExit()
        }
    }

    private val scanBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        public override fun onReceive(context: Context, intent: Intent) {
            if ((intent.getAction() == g.sFilterAction)) {
                try {
                    displayScanResult(intent)
                } catch (ignored: Exception) {
                }
            }
        }
    }

    private fun displayScanResult(i: Intent) {
        var sMsg: String? = i.getStringExtra(g.sScanData)
        if (sMsg!!.length > 10) sMsg = sMsg.substring(9)
        _etPallet!!.setText(sMsg)
        _sClrScanCartonId = ""
        _btnIDOK!!.performClick()
    }

    private fun executeGet() {
        Thread(object : Runnable {
            public override fun run() {
                var sMsg: String?
                var bCtl: Boolean = false
                val bCtl2: Boolean
                if (!(_sPallet == _sClrScanCartonId)) {
                    Arrays.fill(g.REC_ClrHdr, 0.toByte())
                    c.PrismCopyString(
                        g.REC_ClrHdr,
                        g.iRecordId,
                        g.sRecordId,
                        "ClrHdr210"
                    ) //DbFunction 28; Format 91; CartonHdr; PmWinDbm.app (15); Goes to CHD:CartonID to get from ClrHdr.tps
                    c.PrismCopyString(
                        g.REC_ClrHdr,
                        g.iRec_ClrHdr_NewCartonId,
                        g.sRec_ClrHdr_NewCartonId,
                        _sPallet
                    )
                    g.REC_ClrHdr[g.iRec_ClrHdr_TotalType] = (0x31 + _iClrScanCartonTotalType).toByte() //'1'
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_getting_pallet)
                    if (s.ServerSend(g.REC_ClrHdr, _hObj, _tvMsg, _pbAct)) {
                        if (g.REC_ClrHdr.get(g.iRec_ClrHdr_ReadStatus) == 0x31.toByte()) {
                            _sPallet = c.PrismGetString(
                                g.REC_ClrHdr,
                                g.iRec_ClrHdr_NewCartonId,
                                g.sRec_ClrHdr_NewCartonId
                            )
                            _sClrScanCartonId = _sPallet
                            sMsg = c.PrismGetString(
                                g.REC_ClrHdr,
                                g.iRec_ClrHdr_Source,
                                g.sRec_ClrHdr_Source
                            )
                            c.PrismMsg(
                                _hObj,
                                _tvDoc,
                                sMsg + c.PrismGetString(
                                    g.REC_ClrHdr,
                                    g.iRec_ClrHdr_TranNo,
                                    g.sRec_ClrHdr_TranNo
                                )
                            )
                            c.PrismMsg(
                                _hObj,
                                _tvBOL,
                                c.PrismGetString(g.REC_ClrHdr, g.iRec_ClrHdr_BOL, g.sRec_ClrHdr_BOL)
                            )
                            c.PrismMsg(
                                _hObj,
                                _tvUnits,
                                c.PrismGetString(
                                    g.REC_ClrHdr,
                                    g.iRec_ClrHdr_SelQtyShp,
                                    g.sRec_ClrHdr_SelQtyShp
                                )
                            )
                            if (g.REC_ClrHdr.get(g.iRec_ClrHdr_TotalType) == 0x39.toByte()) {
                                c.PrismMsg(_hObj, _tvMsg, R.string.msg_pallet_already_received)
                            } else {
                                c.PrismMsg(_hObj, _tvMsg, R.string.msg_got_pallet)
                                bCtl = true
                            }
                            if (g.REC_ClrHdr.get(g.iRec_ClrHdr_ErrorString) != 0x00.toByte()) {
                                if (g.REC_ClrHdr.get(g.iRec_ClrHdr_TotalType) == 0x39.toByte()) {
                                    sMsg =
                                        getResources().getString(R.string.msg_pallet_already_received)
                                } else {
                                    sMsg = getResources().getString(R.string.msg_got_pallet)
                                }
                                sMsg += " " + getResources().getString(R.string.msg_with_error) + " " + c.PrismGetString(
                                    g.REC_ClrHdr,
                                    g.iRec_ClrHdr_ErrorString,
                                    g.sRec_ClrHdr_ErrorString
                                )
                                c.PrismMsg(_hObj, _tvMsg, sMsg)
                            }
                        } else {
                            c.PrismMsg(_hObj, _tvDoc, "")
                            c.PrismMsg(_hObj, _tvBOL, "")
                            c.PrismMsg(_hObj, _tvUnits, "")
                            sMsg = c.PrismGetString(
                                g.REC_ClrHdr,
                                g.iRec_ClrHdr_ErrorString,
                                g.sRec_ClrHdr_ErrorString
                            )
                            c.PrismMsg(_hObj, _tvMsg, sMsg)
                        }
                    } //if (s.ServerSend(g.REC_ClrHdr, _hObj, _tvMsg, _pbAct)) {
                    c.PrismBtnEnabled(_hObj, _btnIDC_CLR_SCANCARTON_PUSHBUTTON_DETAILS, bCtl)
                    if (g.REC_ClrHdr.get(g.iRec_ClrHdr_TotalType) != 0x34.toByte()) { //'4'
                        c.PrismBtnEnabled(_hObj, _btnIDC_CLR_SCANCARTON_PUSHBUTTON_RECEIVE, bCtl)
                        bCtl2 = (g.REC_ClrHdr.get(g.iRec_ClrHdr_TotalType) != 0x39.toByte()) || (bCtl)
                        c.PrismBtnEnabled(_hObj, _btnIDC_CLR_SCANCARTON_PUSHBUTTON_TOTALS, bCtl2)
                    }
                    if (bCtl) {
                        c.PrismReqFocus(_hObj, _btnIDC_CLR_SCANCARTON_PUSHBUTTON_DETAILS)
                    } else {
                        val imm: InputMethodManager =
                            getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                        c.PrismReqFocus(_hObj, _etPallet, imm)
                    }
                } //if (!_sPallet.equals(_sClrScanCartonId)) {
            }
        }).start()
    }

    private fun executeReceive() {
        Thread(object : Runnable {
            override fun run() {
                var bCtl = false
                System.arraycopy(g.REC_ClrHdr, g.iRec_ClrHdr_NewCartonId, g.REC_ClrHdr, g.iRec_ClrHdr_UpdCartonId, g.sRec_ClrHdr_UpdCartonId)
                Arrays.fill(g.REC_ClrHdr,g.iRec_ClrHdr_SelQtyShp,g.iRec_ClrHdr_SelQtyShp + g.sRec_ClrHdr_SelQtyShp - 1,0.toByte())
                c.PrismMsg(_hObj, _tvMsg, R.string.msg_receiving_pallet)
                if (s.ServerSend(g.REC_ClrHdr, _hObj, _tvMsg, _pbAct)) {
                    if (g.REC_ClrHdr.get(g.iRec_ClrHdr_ReadStatus) == 0x31.toByte()) {
                        c.PrismMsg(_hObj, _tvMsg, R.string.msg_received_pallet)
                        if (g.REC_ClrHdr.get(g.iRec_ClrHdr_ErrorString) == 0x00.toByte()) {
                            c.PrismMsg(_hObj, _tvMsg, R.string.msg_received_pallet)
                            if (g.REC_ClrHdr.get(g.iRec_ClrHdr_TotalType) != 0x39.toByte()) bCtl = true
                            c.PrismBtnEnabled(
                                _hObj,
                                _btnIDC_CLR_SCANCARTON_PUSHBUTTON_DETAILS,
                                bCtl
                            )
                            c.PrismBtnEnabled(
                                _hObj,
                                _btnIDC_CLR_SCANCARTON_PUSHBUTTON_RECEIVE,
                                bCtl
                            )
                        } else {
                            c.PrismMsg(
                                _hObj, _tvMsg,
                                (getResources().getString(R.string.msg_received_pallet) + " " +
                                        getResources().getString(R.string.msg_and_got_error) + " " +
                                        c.PrismGetString(
                                            g.REC_ClrHdr,
                                            g.iRec_ClrHdr_ErrorString,
                                            g.sRec_ClrHdr_ErrorString
                                        ))
                            )
                        }
                    } else {
                        c.PrismMsg(
                            _hObj,
                            _tvMsg,
                            c.PrismGetString(
                                g.REC_ClrHdr,
                                g.iRec_ClrHdr_ErrorString,
                                g.sRec_ClrHdr_ErrorString
                            )
                        )
                    }
                } //if (s.ServerSend(g.REC_ClrHdr, _hObj, _tvMsg, _pbAct)) {
                Arrays.fill(
                    g.REC_ClrHdr,
                    g.iRec_ClrHdr_UpdCartonId,
                    g.iRec_ClrHdr_UpdCartonId + g.sRec_ClrHdr_UpdCartonId - 1,
                    0.toByte()
                )
                if (bCtl) {
                    c.PrismReqFocus(_hObj, _etPallet)
                } else {
                    c.PrismReqFocus(_hObj, _btnIDCANCEL)
                }
            } //public void run() {
        }).start()
    }

    private fun executeExit() {
        Thread(object : Runnable {
            public override fun run() {
                Arrays.fill(g.REC_ClrTot, 0.toByte())
                c.PrismCopyString(
                    g.REC_ClrTot,
                    g.iRecordId,
                    g.sRecordId,
                    "ClrTot210"
                ) //DbFunction 30; Format 93; CartonTot; PmWinDbm.app (15)
                g.REC_ClrTot[g.iRec_ClrTot_PrintReport] = 0x42 //'B'
                g.REC_ClrTot[g.iRec_ClrTot_Source] = 0x45 //'E'
                c.PrismMsg(_hObj, _tvMsg, R.string.msg_exiting)
                if (s.ServerSend(g.REC_ClrTot, _hObj, _tvMsg, _pbAct)) {
                    _hObj!!.post(object : Runnable {
                        public override fun run() {
                            synchronized(Server.Companion._syncToken, {
                                _btnIDOK!!.setEnabled(false)
                                _btnIDCANCEL!!.performClick()
                            })
                        }
                    })
                } else {
                    c.PrismMsg(
                        _hObj,
                        _tvMsg,
                        c.PrismGetString(
                            g.REC_ClrTot,
                            g.iRec_ClrTot_ErrorString,
                            g.sRec_ClrTot_ErrorString
                        )
                    )
                }
            }
        }).start()
    }
}