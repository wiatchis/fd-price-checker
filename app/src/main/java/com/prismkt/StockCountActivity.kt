package com.prismkt

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class StockCountActivity : AppCompatActivity() {
    internal enum class scaAction { GET_SHEET, ADD_SHEET, FIRST, NEXT, SUBMIT, PRINT }
    private val c: Common = Common.instance
    private val s: Server = Server.instance
    private var _hObj: Handler? = null
    private var _etSheet: EditText? = null
    private var _tvTotal: TextView? = null
    private var _tvCreated: TextView? = null
    private var _tvCreatedBy: TextView? = null
    private var _tvMsg: TextView? = null
    private var _pbAct: ProgressBar? = null
    private var _btnIDOK: Button? = null
    private var _btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_SUBMIT: Button? = null
    private var _btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_COUNT: Button? = null
    private var _btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_NEXT: Button? = null
    private var _sSheet: String? = null
    private var _bCtl: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stock_count)
        _etSheet = findViewById<View>(R.id.IDC_STOCK_STOCKCOUNT_EDIT_SHEET) as EditText?
        _tvMsg = findViewById<View>(R.id.IDC_STOCK_STOCKCOUNT_TEXT_MSG) as TextView?
        _btnIDOK = findViewById<View>(R.id.IDOK) as Button?
        _btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_SUBMIT =
            findViewById<View>(R.id.IDC_STOCK_STOCKCOUNT_PUSHBUTTON_SUBMIT) as Button?
        _btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_COUNT =
            findViewById<View>(R.id.IDC_STOCK_STOCKCOUNT_PUSHBUTTON_COUNT) as Button?
        _btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_NEXT =
            findViewById<View>(R.id.IDC_STOCK_STOCKCOUNT_PUSHBUTTON_NEXT) as Button?
        if (g.REC_Security.get(g.iRec_Security_PhysicalInv) == 0x30.toByte()) {
            _tvMsg!!.setText(R.string.msg_not_authorized)
            _etSheet!!.setEnabled(false)
            _btnIDOK!!.setEnabled(false)
            _btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_NEXT!!.setEnabled(false)
            var btn: Button =
                findViewById<View>(R.id.IDC_STOCK_STOCKCOUNT_PUSHBUTTON_ADDSHEET) as Button
            btn.setEnabled(false)
            btn = findViewById<View>(R.id.IDC_STOCK_STOCKCOUNT_PUSHBUTTON_FIRST) as Button
            btn.setEnabled(false)
            btn = findViewById<View>(R.id.IDCANCEL) as Button
            btn.requestFocus()
            return
        }
        _hObj = Handler(Looper.getMainLooper())
        _pbAct = findViewById<View>(R.id.progressBar) as ProgressBar?
        _etSheet!!.setText(_sStockCountSheet)
        _tvTotal = findViewById<View>(R.id.IDC_STOCK_STOCKCOUNT_EDIT_COUNTTOTAL) as TextView?
        _tvCreated = findViewById<View>(R.id.IDC_STOCK_STOCKCOUNT_EDIT_DATETIME) as TextView?
        _tvCreatedBy = findViewById<View>(R.id.IDC_STOCK_STOCKCOUNT_EDIT_CREATEDBY) as TextView?
        _etSheet!!.requestFocus()
        if ((_sStockCountSheet == "")) {
            val imm: InputMethodManager =
                getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            c.PrismReqFocus(_hObj, _etSheet, imm)
        }
        _etSheet!!.setOnKeyListener(object : View.OnKeyListener {
            public override fun onKey(v: View, k: Int, e: KeyEvent): Boolean {
                if (e.getAction() == KeyEvent.ACTION_DOWN && k == KeyEvent.KEYCODE_ENTER) {
                    if (_bCtl) _btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_COUNT!!.performClick() else _btnIDOK!!.performClick()
                    return true
                }
                return false
            }
        })
    }

    override fun onResume() {
        super.onResume()
        c.PrismLayoutRestore(
            this,
            R.id.clStockCount,
            (g.REC_StockCnt.get(g.iRec_StockCnt_CountId) != 0x00.toByte())
        )
    }

    override fun onPause() {
        super.onPause()
        c.PrismLayoutSave(this, R.id.clStockCount)
    }

    fun btnIDOK(view: View?) {
        _sSheet = _etSheet!!.getText().toString().trim({ it <= ' ' })
        executeAction(scaAction.GET_SHEET)
    }

    fun btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_ADDSHEET(view: View?) {
        _sSheet = _etSheet!!.getText().toString().trim({ it <= ' ' })
        executeAction(scaAction.ADD_SHEET)
    }

    fun btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_FIRST(view: View?) {
        _sSheet = _etSheet!!.getText().toString().trim({ it <= ' ' })
        executeAction(scaAction.FIRST)
    }

    fun btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_NEXT(view: View?) {
        _sSheet = _etSheet!!.getText().toString().trim({ it <= ' ' })
        executeAction(scaAction.NEXT)
    }

    fun btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_SUBMIT(view: View?) {
        _sSheet = _etSheet!!.getText().toString().trim({ it <= ' ' })
        executeAction(scaAction.SUBMIT)
    }

    fun btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_REPORT(view: View?) {
        _sSheet = _etSheet!!.getText().toString().trim({ it <= ' ' })
        executeAction(scaAction.PRINT)
    }

    fun btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_COUNT(view: View?) {
        val intent: Intent = Intent(this, StockItemActivity::class.java)
        startActivity(intent)
    }

    fun btnIDCANCEL(view: View?) {
        checkExit()
    }

    private fun executeAction(eCall: scaAction) {
        Thread(ExecuteThread(eCall)).start()
    }

    public override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            checkExit()
            return true
        }
        return (super.onOptionsItemSelected(item))
    }

    override fun onKeyDown(
        keyCode: Int,
        event: KeyEvent
    ): Boolean { //onBackPressed() api lvl 5+
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            checkExit()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun checkExit() {
        if (!_btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_SUBMIT!!.isEnabled()) {
            onBackPressed()
        } else {
            val alertDialog: AlertDialog = AlertDialog.Builder(this).create()
            alertDialog.setTitle(R.string.dlg_title_stock_count)
            alertDialog.setMessage(getResources().getString(R.string.dlg_question_submit_count))
            alertDialog.setButton(
                AlertDialog.BUTTON_NEGATIVE,
                getResources().getString(R.string.dlg_answer_yes),
                object : DialogInterface.OnClickListener {
                    public override fun onClick(arg0: DialogInterface, arg1: Int) {
                        onBackPressed()
                    }
                })
            alertDialog.setButton(
                AlertDialog.BUTTON_POSITIVE,
                getResources().getString(R.string.dlg_answer_no),
                object : DialogInterface.OnClickListener {
                    public override fun onClick(arg0: DialogInterface, arg1: Int) {}
                })
            alertDialog.show()
        }
    }

    internal inner class ExecuteThread constructor(private val _eCall: scaAction) : Runnable {
        public override fun run() {
            var iMsg: Int
            var sMsg: String?
            _bCtl = false
            Arrays.fill(g.REC_StockCnt, 0.toByte())
            c.PrismCopyString(
                g.REC_StockCnt,
                g.iRecordId,
                g.sRecordId,
                "StockCnt4426"
            ) //DbFunction 6(35); Format 107; PhyInvHdr; PmWinDbm.app (3); Updates PiShtHdr.tps
            g.REC_StockCnt[g.iRec_StockCnt_Action] = 0x31 //'1'
            c.PrismCopyString(
                g.REC_StockCnt,
                g.iRec_StockCnt_CountId,
                g.sRec_StockCnt_CountId,
                _sSheet
            )
            when (_eCall) {
                scaAction.GET_SHEET, scaAction.PRINT -> if (g.REC_StockCnt.get(g.iRec_StockCnt_CountId) == 0x00.toByte()) {
                    c.PrismMsg(_hObj, _tvMsg, R.string.msg_sheet_must_be_entered)
                    exit()
                    return
                } else {
                    if (_eCall == scaAction.GET_SHEET) {
                        g.REC_StockCnt[g.iRec_StockCnt_AreaId] = 0x47 //'G'
                        iMsg = R.string.msg_getting_sheet
                    } else {
                        g.REC_StockCnt[g.iRec_StockCnt_AreaId] = 0x52 //'R'
                        iMsg = R.string.msg_sending_print_request
                    }
                }
                scaAction.ADD_SHEET -> {
                    g.REC_StockCnt[g.iRec_StockCnt_CountId] = 0x00
                    g.REC_StockCnt[g.iRec_StockCnt_AreaId] = 0x41 //'A'
                    System.arraycopy(
                        g.REC_Logon,
                        g.iRec_Logon_UserId,
                        g.REC_StockCnt,
                        g.iRec_StockCnt_UserId,
                        g.sRec_StockCnt_UserId
                    )
                    iMsg = R.string.msg_adding_sheet
                }
                scaAction.FIRST -> {
                    g.REC_StockCnt[g.iRec_StockCnt_AreaId] = 0x46 //'F'
                    iMsg = R.string.msg_getting_most_recent_open_sheet
                }
                scaAction.NEXT -> {
                    g.REC_StockCnt[g.iRec_StockCnt_AreaId] = 0x4E //'A'
                    iMsg = R.string.msg_getting_next_older_open_sheet
                }
                scaAction.SUBMIT -> {
                    g.REC_StockCnt[g.iRec_StockCnt_AreaId] = 0x53 //'S'
                    iMsg = R.string.msg_submitting_sheet
                }
            }
            c.PrismMsg(_hObj, _tvMsg, iMsg)
            if (s.ServerSend(g.REC_StockCnt, _hObj, _tvMsg, _pbAct)) {
                if (g.REC_StockCnt.get(g.iRec_StockCnt_ReadStatus) == 0x31.toByte()) {
                    _sSheet = c.PrismGetString(
                        g.REC_StockCnt,
                        g.iRec_StockCnt_CountId,
                        g.sRec_StockCnt_CountId
                    )
                    _sStockCountSheet = _sSheet
                    c.PrismMsg(_hObj, _etSheet, _sSheet)
                    val sbSku: StringBuilder = StringBuilder()
                    sMsg = c.PrismGetItemSku(
                        g.REC_StockCnt,
                        g.iRec_StockCnt_ErrorString,
                        g.sRec_StockCnt_ErrorString,
                        sbSku
                    )
                    c.PrismMsg(_hObj, _tvTotal, sMsg)
                    c.PrismMsg(_hObj, _tvCreated, sbSku.toString())
                    sMsg = c.PrismGetString(
                        g.REC_StockCnt,
                        g.iRec_StockCnt_UserId,
                        g.sRec_StockCnt_UserId
                    )
                    c.PrismMsg(_hObj, _tvCreatedBy, sMsg)
                    when (_eCall) {
                        scaAction.GET_SHEET -> iMsg = R.string.msg_got_sheet
                        scaAction.ADD_SHEET -> iMsg = R.string.msg_added_sheet
                        scaAction.FIRST -> if (g.REC_StockCnt.get(g.iRec_StockCnt_CountId) != 0x00.toByte()) {
                            iMsg = R.string.msg_got_most_recent_open_sheet
                        } else {
                            iMsg = R.string.msg_got_no_open_sheets
                        }
                        scaAction.NEXT -> if (g.REC_StockCnt.get(g.iRec_StockCnt_AreaId) == 0x4E.toByte()) { //'N'
                            iMsg = R.string.msg_got_next_older_open_sheet
                        } else {
                            iMsg = R.string.msg_got_last_older_open_sheet
                        }
                        scaAction.SUBMIT -> {
                            iMsg = R.string.msg_submitted_sheet
                            c.PrismMsg(_hObj, _tvMsg, iMsg)
                            exit()
                            return
                        }
                        scaAction.PRINT -> {
                            iMsg = R.string.msg_sent_print_request
                            c.PrismMsg(_hObj, _tvMsg, iMsg)
                            return
                        }
                    }
                    _bCtl = true
                    c.PrismMsg(_hObj, _tvMsg, iMsg)
                } else {
                    c.PrismMsg(_hObj, _tvTotal, "")
                    c.PrismMsg(_hObj, _tvCreated, "")
                    c.PrismMsg(_hObj, _tvCreatedBy, "")
                    sMsg = c.PrismGetString(
                        g.REC_StockCnt,
                        g.iRec_StockCnt_ErrorString,
                        g.sRec_StockCnt_ErrorString
                    )
                    c.PrismMsg(_hObj, _tvMsg, sMsg)
                }
            }
            exit()
        }

        private fun exit() {
            val bCtl2: Boolean
            bCtl2 =
                !_bCtl || (_eCall != scaAction.NEXT) || (g.REC_StockCnt.get(g.iRec_StockCnt_AreaId) == 0x4E.toByte()) //'N'
            c.PrismBtnEnabled(_hObj, _btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_COUNT, _bCtl)
            c.PrismBtnEnabled(_hObj, _btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_SUBMIT, _bCtl)
            c.PrismBtnEnabled(_hObj, _btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_NEXT, bCtl2)
            if (_bCtl) {
                c.PrismReqFocus(_hObj, _btnIDC_STOCK_STOCKCOUNT_PUSHBUTTON_COUNT)
            } else {
                val imm: InputMethodManager =
                    getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                c.PrismReqFocus(_hObj, _etSheet, imm)
            }
        }
    }

    companion object {
        private var _sStockCountSheet: String? = ""
    }
}